import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { switchMap, map, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { Action } from '@ngrx/store';
import * as dogsActions from '../actions/dogs.action';
import { DogsService } from 'src/app/dogs.service';

@Injectable()
export class DogsEffects {
    constructor(
        private actions$: Actions,
        private dogService: DogsService
        ) {}

    @Effect()
    public loadDogs$: Observable<Action> = this.actions$.pipe(
      ofType(dogsActions.LOAD_DOGS),
      switchMap(() => {
        return this.dogService.getDogs().pipe(
          map(response => new dogsActions.LoadDogsSuccess(response.dogs)),
          catchError(error => of(new dogsActions.LoadDogsFail(error)))
        );
      })
    );

    @Effect()
    public createDog$: Observable<Action> = this.actions$.pipe(
      ofType(dogsActions.CREATE_DOG),
      mergeMap((action: any) => {
        return this.dogService.addDog(action.payload).pipe(
          switchMap(() => {
            return this.dogService.getDogs().pipe(
              map(response => new dogsActions.CreateDogSuccess(response.dogs)),
              catchError(error => of(new dogsActions.LoadDogsFail(error)))
            );
          })
        );
      })
    );

    @Effect()
    public deleteDog$: Observable<Action> = this.actions$.pipe(
      ofType(dogsActions.DELETE_DOG),
      switchMap((action: any) => {
        return this.dogService.deleteDog(action.payload).pipe(
          map(response => new dogsActions.DeleteDogSuccess(response)),
          catchError(error => of(new dogsActions.LoadDogsFail(error)))
        );
      })
    );

    @Effect()
    public updateDog$: Observable<Action> = this.actions$.pipe(
      ofType(dogsActions.UPDATE_DOG),
      switchMap((dog: any) => {
        return this.dogService.editDog(dog.payload, dog.formData).pipe(
          map(response => new dogsActions.UpdateDogSuccess(dog.payload, dog.formData)),
          catchError(error => of(new dogsActions.LoadDogsFail(error)))
        );
      })
    );
}
