import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromDogs from './dogs.reducer';
import { state } from '@angular/animations';

export interface AnimalsState {
    dogs: fromDogs.DogsState;
}

export const reducers: ActionReducerMap<AnimalsState> = {
    dogs: fromDogs.reducer
};

export const getAnimalsState = createFeatureSelector<AnimalsState>('animals');

/// dogs state
export const getDogsState = createSelector(
    getAnimalsState,
    (state: AnimalsState) => state.dogs
);

export const getAllDogs = createSelector(getDogsState, fromDogs.getDogs);
export const getDogsLoaded = createSelector(getDogsState, fromDogs.getDogsLoaded);
export const getDogsLoading = createSelector(getDogsState, fromDogs.getDogsLoading);
