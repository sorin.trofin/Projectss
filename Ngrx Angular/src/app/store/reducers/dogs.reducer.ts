import { Dog } from 'src/app/dog';
import * as fromDogs from '../actions/dogs.action';

export interface DogsState {
    data: Dog[];
    loaded: boolean;
    loading: boolean;
}

export const initialState: DogsState = {
    data: [],
    loaded: false,
    loading: false
};

export function reducer(
    state = initialState,
    action: fromDogs.DogsAction
): DogsState {

    switch (action.type) {
      case fromDogs.LOAD_DOGS: {
        return {
          ...state,
          loading: true
        };
      }

      case fromDogs.LOAD_DOGS_SUCCESS: {
        const data = action.payload;

        return {
          ...state,
          loading: false,
          loaded: true,
          data
        };
      }

      case fromDogs.LOAD_DOGS_FAIL: {
        return {
          ...state,
          loading: false,
          loaded: false
        };
      }

      case fromDogs.CREATE_DOG: {
        return {
          ...state,
          loaded: false,
          loading: true
        };
      }

      case fromDogs.CREATE_DOG_SUCCESS: {
        const data = action.payload;

        return {
          ...state,
          loading: false,
          loaded: true,
          data
        };
      }

      case fromDogs.DELETE_DOG: {
        return {
          ...state,
          loaded: false,
          loading: true
        };
      }

      case fromDogs.DELETE_DOG_SUCCESS: {
        const dataToFilter = Object.assign([], state.data);
        const data = dataToFilter.filter(dog => {
          return dog.id !== action.payload.id;
        });

        return {
          ...state,
          loading: false,
          loaded: true,
          data
        };
      }

      case fromDogs.UPDATE_DOG: {
        return {
          ...state,
          loaded: false,
          loading: true
        };
      }

      case fromDogs.UPDATE_DOG_SUCCESS: {
        const dataToFilter = Object.assign([], state.data);
        const data = dataToFilter.map((dog) => {
          if (dog.id === action.payload.id) {
            action.formData.id = action.payload.id;

            return action.formData;
          }

          return dog;
        });

        return {
          ...state,
          loading: false,
          loaded: true,
          data
        };
      }
    }

    return state;
}

export const getDogsLoading = (state: DogsState) => state.loading;
export const getDogsLoaded = (state: DogsState) => state.loaded;
export const getDogs = (state: DogsState) => state.data;
