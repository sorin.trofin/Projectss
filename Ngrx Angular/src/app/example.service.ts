import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  public behaviorExample: BehaviorSubject<any> = new BehaviorSubject<any>({});

  constructor() { }


  public setBehavior(obj): void {
    this.behaviorExample.next(obj);
  }

  public getBehavior(): BehaviorSubject<any> {
    return this.behaviorExample;
  }
}
