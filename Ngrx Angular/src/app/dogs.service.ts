import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dog } from 'src/app/dog';
import { map } from 'rxjs/operators';
import { Post } from 'src/app/post';

@Injectable({
  providedIn: 'root'
})
export class DogsService {
  constructor(private http: HttpClient) {}
  public dogsUrl = 'http://localhost:4600/dogs';
  public dogsUrlPut = 'http://localhost:4600/dogs';

  public postsUrl = 'http://localhost:4600/posts';
  public postsUrlPut = 'http://localhost:4600/post';


  /// DOGS METHODS

  public getDogs() {
    return this.http.get<any>(this.dogsUrl);
  }

  public getDogsById(id) {
    return this.http.get<Dog[]>(this.dogsUrl + '/' + id);
  }

  public addDog(dog: Dog): Observable<Dog> {
    return this.http.post<Dog>(this.dogsUrl, dog);
  }

  public deleteDog(dog: Dog): Observable<Dog[]> {
    const id = typeof dog === 'number' ? dog : dog.id;
    const deleteUrl = `${this.dogsUrl}/${id}`;
    return this.http.delete<Dog[]>(deleteUrl);
  }

  public editDog(dog: Dog, logData): Observable<Dog> {
    const editUrl = `${this.dogsUrl}/${dog.id}`;
    return this.http.put<Dog>(editUrl, logData);
  }

  public patchDog(dog, logData): Observable<any> {
    const editUrl = `${this.dogsUrl}/${dog.id}`;
    return this.http.patch<any>(editUrl, logData);
  }
  // POST METHODS
  public getPosts() {
    return this.http.get<any>(this.postsUrl);
  }

  public addPosts(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl + '/post', post);
  }

  public editPost(post: Post, logData): Observable<Post> {
    const editUrl = `${this.postsUrlPut}/${post.id}`;
    return this.http.put<Post>(editUrl , logData);
  }

  public deletePost(post: Post): Observable<Post[]> {
    const id = typeof post === 'number' ? post : post.id;
    const deleteUrl = `${this.postsUrlPut}/${id}`;
    return this.http.delete<Post[]>(deleteUrl);
  }


}
