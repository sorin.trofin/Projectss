import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule  } from '@angular/common/http';
import { MessageService } from './message.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared-module/shared.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { JwtModule } from '@auth0/angular-jwt';
import { UserService } from './user.service';
import { AuthenticationService } from './authentication.service';
import { AppSnackBarService } from './app-snackbar-service';
import { SecurityService } from './security-service';
import { CookieService } from 'ngx-cookie-service';
import { NgrxComponent } from './ngrx/ngrx.component';
import { ExampleService } from './example.service';

import { StoreModule } from '@ngrx/store';
import { reducers, effects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StopPropagationDirective } from './directives/stop-propagation.directive';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NgrxComponent,
    StopPropagationDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4000'],
        blacklistedRoutes: ['localhost:4000/api/auth']
      }
    }),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature(effects),
    StoreModule.forRoot({}),
    StoreModule.forFeature('animals', reducers)
  ],
  providers: [
    MessageService,
    AuthGuard,
    UserService,
    AuthenticationService,
    AppSnackBarService,
    SecurityService,
    CookieService,
    ExampleService
  ],
  bootstrap: [AppComponent],
  exports: [
    NgrxComponent,
    StopPropagationDirective
  ],
})
export class AppModule { }
