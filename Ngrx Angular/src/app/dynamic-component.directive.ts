import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[dynamic-comp]'
})
export class DynamicComponentDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
