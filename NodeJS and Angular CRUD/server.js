const express = require('express');
const path =  require('path');
const app = express();
const pg = require('pg');
const dotenv = require('dotenv');
var cors = require('cors');
const uuidV4 = require('uuid/v4');
var bodyParser = require('body-parser');
// config for your database
const config = {
 // insert database config
};

app.use(cors());
app.use(bodyParser.json());
var pool = new pg.Pool(config);

pool.connect(function(err, client, done) {
  if(err) {
    console.log('Server Error');
  } else {
    console.log('Connected to Database');
  }
  client.query('SELECT * FROM post', function(err, result) {
    if(err) {
      return console.error('erorr running query', err);
    }
    // console.log(result);
    // res.render('index', {posts: result.rows});
    done();
  });
});


app.get('/post', (req, res) => {
  pool.query('SELECT * FROM post', function(err, result, done) {
    console.log(result.rows);
    res.status(200).json({posts: result.rows});
  });
});

app.get('/post/:id', (req, res) => {
  const id = parseInt(req.params.id);
  console.log(id);
  pool.query('SELECT * FROM post WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
    // res.status(200).json(results.rows);
    res.status(200).json({posts: results.rows});
  });
});

app.post('/post', (req, res) => {
    pool.query('INSERT INTO post(id,posts,type) VALUES($1, $2, $3)',
    [parseInt(uuidV4()), req.body.posts, req.body.type]).then(function () {
      res.status(200).json({status: 'success', message: 'inserted post'});
    });
});

app.put('/post/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const { posts, type } = req.body;
  console.log('id', id);
  pool.query(
    'UPDATE post SET posts = $1, type = $2 WHERE id = $3',
    [posts, type, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json({status: 'success', message: `inserted post with ${id}`});
    }
  );
});

app.delete('/post/:id', (req, res)=> {
  const id = parseInt(req.params.id);

  pool.query('DELETE FROM post WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json({status: 'success', message: `deleted post with ${id}`});
  });
});

const posts = require('./server/routes/posts');

app.use('/posts', posts);

const port = process.env.PORT || 4600;

app.listen(port, (req, res) => {
 console.log(`RUNNING on port ${port}`);
});