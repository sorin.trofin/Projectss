import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = 'ngrx';
  public dataReceived: {}[] = [];

  constructor(private messageService: MessageService) {
  }

  public ngOnInit(): void {
    this.messageService.popupSource.subscribe((result) => {
      console.log(result);
    });

    this.messageService.dataToSend.subscribe((result) => {
      this.dataReceived = result;
      console.log(result);
    });
  }
}


