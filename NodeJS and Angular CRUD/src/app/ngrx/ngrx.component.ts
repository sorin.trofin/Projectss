import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-ngrx',
  templateUrl: './ngrx.component.html',
  styleUrls: ['./ngrx.component.css']
})
export class NgrxComponent implements OnInit {
  public hideElement: boolean;
  public dataSend: any = [
    {name: 'sorin' },
    {name: 'edi'},
    {name: 'stefan'}
  ];

  public dataSendTwo: any = [
    {name: 'AlteDate' },
    {name: 'AlteDate2'},
    {name: 'AlteDate3'}
  ];
  
  constructor(public messageService:MessageService) { }

  popupClick() {
    this.messageService.popup(true);
    this.messageService.popupSource.subscribe((result) => {
      this.hideElement = result;
   });    
  }

  hideElem() {
    this.messageService.popup(false);
  }

  sendObj() {
    this.messageService.dataToSend.next(this.dataSend);
  }

  sendObjTwo() {
    this.messageService.dataToSend.next(this.dataSendTwo);
  }

  ngOnInit() {
 
  }
}
