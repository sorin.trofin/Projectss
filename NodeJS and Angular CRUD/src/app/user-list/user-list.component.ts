import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: [ './user-list.component.css' ]
})
export class UserListComponent implements OnInit {
	names: string[];
	apartments: object[];
	constructor() {
		this.names = [ 'Puffy', 'Elsa', 'OtherDog' ];
		this.apartments = [
			{ name: 'sorin', age: 'notold', gender: 'male' },
			{ name: 'stefan', age: 'notold', gender: 'male' },
			{ name: 'edi', age: 'notold', gender: 'male' },
			{ name: 'tudor', age: 'notold', gender: 'male' },
			{ name: 'alex', age: 'notold', gender: 'male' }
		];
	}

	ngOnInit() {}
}
