import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Tutorial } from './../models/tutorial.model';
import { AppState } from './../app.state';
import * as TutorialActions from '../actions/tutorial.actions';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {
  public tutorials: Observable<Tutorial[]>;

  constructor(private store: Store<AppState>) {
    this.tutorials = store.select('tutorial');
  }

  public delTutorial(index) {
    this.store.dispatch(new TutorialActions.RemoveTutorial(index));
  }

  public ngOnInit() {
  }

  public fromReadComponent() {
    console.log('DE LA COMPONENTA READ');
  }
  public fromReadComponent2() {
    console.log('DE LA COMPONENTA READ DOI WAAAA');
  }
}
