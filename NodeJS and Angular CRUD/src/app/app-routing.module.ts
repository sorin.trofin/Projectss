import { TryesComponent } from './tryes/tryes.component';
import { UserListComponent } from './user-list/user-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserItemComponent } from './user-item/user-item.component';
import { NgrxComponent } from './ngrx/ngrx.component';
import { ParentComponentComponent } from './parent-component/parent-component.component';
import { ChildComponentComponent } from './child-component/child-component.component';

const routes: Routes = [
  { path: '', redirectTo: '/ngrx', pathMatch: 'full' },
  { path: 'acasa', component: ParentComponentComponent },
  { path: 'ngrx', component: NgrxComponent },
  { path: 'tryes', component: TryesComponent }
];

@NgModule({
  imports: [
    [RouterModule.forRoot(routes)],
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule {

}
