import { DogsService } from './../dogs.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Dog } from 'src/app/dog';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Post } from '../post';

@Component({
  selector: 'app-parent-component',
  templateUrl: './parent-component.component.html',
  styleUrls: ['./parent-component.component.css']
})
export class ParentComponentComponent implements OnInit {
  public msg: string;
  public dogArray: Dog[] = [];
  public receivedModifiedDogs = [];
  public firstApiData: any = [];
  public formdata: FormGroup;
  public editState: boolean;

  constructor(private dogService: DogsService, private formBuilder: FormBuilder) {
    this.msg = 'PARENTCOMPONENT';
  }

  public ngOnInit() {
    this.formdata = this.formBuilder.group({
      // id: ['', Validators.required],
      posts: ['', Validators.required],
      type: ['', Validators.required],
    });

    // this.getDogs();
    this.firstApiCall();
  }

  public addPost(): void {
    const postData = this.formdata.value;
    this.dogService.addPosts(postData).subscribe((response) => {
      if (response) {
        console.log(response);
        this.firstApiCall();
        this.formdata.reset();
      }
    });
  }

  public editPost(post) {
    const editPostData = this.formdata.value;

    this.editState = false;
    this.dogService.editPost(post, editPostData).subscribe((response) => {
    if (response) {
      this.firstApiCall();
    }
    this.formdata.reset();
    });
  }

  public deletePost(post: Post): void {
    // this.appdogs = this.appdogs.filter((h) => h !== dog);
    this.dogService.deletePost(post).subscribe(response => {
      if (response) {
        this.firstApiCall();
      }
    });
  }

  public firstApiCall(): void {
    this.dogService.firstApiCall().subscribe(response => {
      this.firstApiData = response;
      console.log('firstApiData', this.firstApiData);
    });
  }

  public getDogs(): void {
    this.dogService.getDogs().subscribe(response => {
      this.dogArray = response;
    });
  }

  public receiveModifiedDogs(event) {
    this.receiveModifiedDogs = event;
    console.log('receiveModifiedDogsInsideAppComp', event);
  }
}
