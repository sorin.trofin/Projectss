import { Component, OnInit, Input, AfterViewInit, OnChanges, EventEmitter, Output } from '@angular/core';
import { Dog } from 'src/app/dog';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})

export class ChildComponentComponent implements OnInit {
  @Input() public dogs: Dog[];
  @Output() public modifiedDogsOutput = new EventEmitter();
  public modifiedDogs = [];

  constructor() { }

  public ngOnInit() {
    this.modifiedDogs.push(this.dogs);
    this.modifyDog();
  }

  public modifyDog() {
    this.modifiedDogs.push(this.modifiedDogs);
  }

  public sendDogs() {
    this.modifiedDogsOutput.emit(this.modifiedDogs);
  }

  // public ngOnChanges(changes) {
  //   // only run when property "data" changed

  //   if (changes['dogs']) {
  //     console.log(changes);
  //   }
  // }
}
