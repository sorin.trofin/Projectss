﻿(function () {

    var app = angular.module("thunderWorm");

    var _directiveBaseFolder = "app/directives/slider/";
    var _imgFolderBase = "./Images/partnerSlider/";

    app.directive("partnersSlide", function () {
        return {
            scope: true,
            controller: partnersSlide,
            controllerAs: 'vm',
            bindToController: {
                partnersSlideData: "="
            },
            templateUrl: _directiveBaseFolder + "partnersSlide.directive.html"
        };
    });

    app.controller("partnersSlide", partnersSlide);

    partnersSlide.$inject = ["$translate"];

    function partnersSlide($translate) {
        var vm = this;
        vm.init();
    }

    partnersSlide.prototype.init = function () {
        var vm = this;

        vm.displayImages = [
           
            { src: _imgFolderBase + "partnerSlider1.png",  href: "http://www.gipest.ro/",  alt: "Gip Est" },
            { src: _imgFolderBase + "partnerSlider2.png",  href: "https://www.wetranslate.ro/?gclid=CjwKCAjwsJ3ZBRBJEiwAtuvtlCTmJb7BYG01qc1EgKFr1mWq0TmB5BSpw-8DesVk0KO-DhBPBYWwyBoC3acQAvD_BwE",  alt: "We Translate" },
            { src: _imgFolderBase + "partnerSlider3.png",  href: "http://www.dandinu.net/", alt: "Dan Dinu" },
            { src: _imgFolderBase + "partnerSlider4.png",  href: "http://www.cultura.ro/culturaro",  alt: "Ministerul Culturii" },
            { src: _imgFolderBase + "partnerSlider5.png",  href: "https://www.leaseplan.ro/?lang=ro",  alt: "Lease Plan" },
        ];
    }

})();