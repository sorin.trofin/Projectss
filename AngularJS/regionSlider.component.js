﻿(function () {

    var app = angular.module("thunderWorm");

    var _directiveBaseFolder = "app/components/regionSlider/";
    var _imgFolderBase = "./Images/region/";

    app.component("regionSlider",{
        controller: regionSlider,
        controllerAs: 'vm',
        bindings: {
            regionVar: "@"
        },
        templateUrl: _directiveBaseFolder + "regionSlider.component.html"
    });

    function regionSlider() {
        var vm = this;
        
        vm.init();
    }

    regionSlider.prototype.init = function () {
        var vm = this;
       
        vm.displayImages = [
            { src: _imgFolderBase + regionVar + "/" + "banatSlider1.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider2.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider3.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider4.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider5.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider6.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider7.jpg" },
            { src: _imgFolderBase + regionVar + "/" + "banatSlider8.jpg" }
        ];
    }
})();