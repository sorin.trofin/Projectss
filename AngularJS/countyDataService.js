﻿(function () {
    "use strict";

    angular.module("thunderWorm").factory("countyDataService", countyDataService);

    countyDataService.$inject = ["$q", "$http", "$location"];

    function countyDataService($q, $http, $location) {

        var headerAppJson = { headers: { 'Content-Type': "application/json" } };

        return {
            getCountyData: function (countyCode) {

                return $http.get("/api/CountyData/GetCountyByCode", { params: { countyCode: countyCode } });
            }
        };
    }

})();