﻿(function () {

    var app = angular.module("thunderWorm");

    var _directiveBaseFolder = "app/components/homeLangSwitch/";
    var _imgFolderBase = "./Images/imgFlags/";

    app.component("homeLangSwitch", {
        controller: homeLangSwitch,
        controllerAs: 'vm',
        bindings: {},
        templateUrl: _directiveBaseFolder + "homeLangSwitch.component.html"
    });

    homeLangSwitch.$inject = ["$translate", "$rootScope", "localStorageService"];

    function homeLangSwitch($translate, $rootScope, localStorageService) {
        var vm = this;

        vm.$translate = $translate;
        vm.$rootScope = $rootScope;

        vm.localStorageService = localStorageService;

        vm.$onInit = vm.init;
    }

    homeLangSwitch.prototype.changeLang = function (lang) {
        var vm = this;

        vm.appLang = lang;
        vm.$translate.use(lang);
        vm.$rootScope.$broadcast("language-changed");
        vm.localStorageService.set("app-lang", lang);
    }

    homeLangSwitch.prototype.init = function () {
        var vm = this;

        vm.appLang = vm.localStorageService.get("app-lang");

        if (vm.appLang) {
            vm.changeLang(vm.appLang);
        }

        vm.langFlags = [
            { src: _imgFolderBase + "ro.png", langCode: "ro" },
            { src: _imgFolderBase + "en.png", langCode: "en" },
            { src: _imgFolderBase + "ar.png", langCode: "ar" },
            { src: _imgFolderBase + "fr.png", langCode: "fr" },
            { src: _imgFolderBase + "de.png", langCode: "de" },
            { src: _imgFolderBase + "hu.png", langCode: "hu" },
            { src: _imgFolderBase + "ru.png", langCode: "ru" },
            { src: _imgFolderBase + "ir.png", langCode: "ir" },
            { src: _imgFolderBase + "es.png", langCode: "es" },
            { src: _imgFolderBase + "tr.png", langCode: "tr" },
            { src: _imgFolderBase + "ch.png", langCode: "ch" }
        ];
    }
})()