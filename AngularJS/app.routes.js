﻿(function () {

    var app = angular.module("thunderWorm");

    app.config(myAppRoutesProvider);

    myAppRoutesProvider.$inject = ["$routeProvider", "$locationProvider"];

    function myAppRoutesProvider($routeProvider, $locationProvider) {

        var _viewBaseFolder = "app/views/";
        var _staticViewFolder = _viewBaseFolder + "static/";
        var _regionViewFolder = _viewBaseFolder + "region/"

        $routeProvider
            .when("/", {
                templateUrl: _viewBaseFolder + "main-map.html",
                controller: "mainMapCtrl",
                controllerAs: "vm"
            });

        var _navMapCtrl = {
            templateUrl: _viewBaseFolder + "nav-map.html",
            controller: "navMapCtrl",
            controllerAs: "vm"
        }

        $routeProvider
            .when("/map", _navMapCtrl)
            .when("/map/:id", _navMapCtrl);

        var _locationCtrl = {
            templateUrl: _viewBaseFolder + "location.html",
            controller: "locationCtrl",
            controllerAs: "vm"
        };

        $routeProvider
            .when("/location", _locationCtrl)
            .when("/location/:param", _locationCtrl);

        $routeProvider
            .when("/register", {
                templateUrl: _viewBaseFolder + "auth-register.html",
                controller: "authCtrl",
                controllerAs: "vm"
            }).when("/login", {
                templateUrl: _viewBaseFolder + "auth-login.html",
                controller: "authCtrl",
                controllerAs: "vm"
            }).when("/user", {
                templateUrl: _viewBaseFolder + "auth-user.html",
                controller: "authCtrl",
                controllerAs: "vm"
            });

        $routeProvider
            .when("/test", {
                templateUrl: _staticViewFolder + "test.html",
            })
            .when("/about", {
                templateUrl: _staticViewFolder + "about.html",
            })
            .when("/contact", {
                templateUrl: _staticViewFolder + "contact.html",
            })
            .when("/blog", {
                templateUrl: _staticViewFolder + "blog.html",
            })
            .when("/whyinvest", {
                templateUrl: _staticViewFolder + "whyinvest.html",
            })
            .when("/latestUAT", {
                templateUrl: _staticViewFolder + "UAT.html",
            })
            .when("/anunturi", {
                templateUrl: _staticViewFolder + "anunturi.html",
            })
            .when("/politici", {
                templateUrl: _staticViewFolder + "politici.html",
            })
            .when("/termeniConditii", {
                templateUrl: _staticViewFolder + "termeniConditii.html",
            });

        $routeProvider
            .when("/Banat", {
                templateUrl: _regionViewFolder + "Banat.html"
            }).when("/Crisana", {
                templateUrl: _regionViewFolder + "Crisana.html"
            }).when("/Oltenia", {
                templateUrl: _regionViewFolder + "Oltenia.html"
            }).when("/Dobrogea", {
                templateUrl: _regionViewFolder + "Dobrogea.html"
            }).when("/Bucovina", {
                templateUrl: _regionViewFolder + "Bucovina.html"
            }).when("/Moldova", {
                templateUrl: _regionViewFolder + "Moldova.html"
            }).when("/Maramures", {
                templateUrl: _regionViewFolder + "Maramures.html"
            }).when("/Transilvania", {
                templateUrl: _regionViewFolder + "Transilvania.html"
            }).when("/Muntenia", {
                templateUrl: _regionViewFolder + "Muntenia.html"
            });

        var _formCtrl = {
            templateUrl: _viewBaseFolder + "forms.html",
            controller: "formsCtrl",
            controllerAs: "vm"
        };

        $routeProvider
            .when("/forms", _formCtrl)
            .when("/forms/:param", _formCtrl);

        // IF ROUTE DOES NOT EXIST OR BAD ROUTE -> GO TO HOME
        $routeProvider.otherwise({
            redirectTo: "/"
        });

        $locationProvider.hashPrefix("");
    }

})();