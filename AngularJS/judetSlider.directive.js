﻿(function () {

    var app = angular.module("thunderWorm");

    var _directiveBaseFolder = "app/directives/slider/";
    var _imgFolderBase = "./Images/";

    app.directive("judetSlider", function () {
        return {
            scope: {},
            controller: judetSlider,
            controllerAs: 'vm',
            bindToController: true, //required in 1.3+ with controllerAs
            templateUrl: _directiveBaseFolder + "judetSlider.directive.html"
        };

    });

    app.controller("judetSlider", judetSlider);

    function judetSlider() {
        var vm = this;

        vm.init();
    }

    judetSlider.prototype.init = function () {
        var vm = this;

        vm.displayImages = [
            { src: _imgFolderBase + "1.jpg" },
            { src: _imgFolderBase + "2.jpg" },
            { src: _imgFolderBase + "3.jpg" },
            { src: _imgFolderBase + "4.jpg" },
            { src: _imgFolderBase + "5.jpg" },
            { src: _imgFolderBase + "6.jpg" },
            { src: _imgFolderBase + "7.jpg" },
            { src: _imgFolderBase + "8.jpg" }
        ];
    }
})()