export interface Example {
  name: string;
  url: string;
  other: string;
}