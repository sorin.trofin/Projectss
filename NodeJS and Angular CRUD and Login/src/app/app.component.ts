import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';
import { AuthenticationService } from 'src/app/authentication.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'LoginApp NodeJS';
  public dataReceived: {}[] = [];

  constructor(
    private messageService: MessageService,
    private authenticationService: AuthenticationService,
    private router: Router
    ) {
  }

  public ngOnInit(): void {
    this.messageService.popupSource.subscribe((result) => {
      console.log(result);
    });

    this.messageService.dataToSend.subscribe((result) => {
      this.dataReceived = result;
      console.log(result);
    });
  }

  public logout(): any {
    if (this.isLogged()) {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }
  }

  public isLogged(): boolean {
    return this.authenticationService.loggedIn();
  }
}


