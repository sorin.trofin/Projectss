// ----------SLIDER----------
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1 ) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        slides[i].className = slides[i].className.replace(" fadeIn animated","");
    }

    for  (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active","");
        dots[i].className = dots[i].className.replace(" fadeIn animated","");
    }

    slides[slideIndex-1].style.display = "block";
    slides[slideIndex-1].className += " fadeIn animated";

    dots[slideIndex-1].className += " fadeIn animated";

    dots[slideIndex-1].className += " active";
    
    
}

var myIndex = 0;
carousel();

function carousel() {
    var i;
    var imgDuration = 3000;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";

        slides[i].className = slides[i].className.replace(" fadeIn animated","");
    }
    for  (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active","");
        dots[i].className = dots[i].className.replace(" fadeIn animated","");
    }
    myIndex++;
    if (myIndex > slides.length) {myIndex = 1}
    slides[myIndex - 1].style.display = "block";
    slides[myIndex - 1].className += " fadeIn animated";
    dots[myIndex - 1].className += " active";
    dots[myIndex - 1].className += " fadeIn animated";
    setTimeout(carousel, 3000);
    
}




// ----------SLIDER----------


//----------- ANIMATIONS-------------


jQuery(function($) {
  
    // Function which adds the 'animated' class to any '.animatable' in view
    var doAnimations = function() {
      
      // Calc current offset and get all animatables
      var offset = $(window).scrollTop() + $(window).height(),
          $animatables = $('.animatable');
      
      // Unbind scroll handler if we have no animatables
      if ($animatables.length == 0) {
        $(window).off('scroll', doAnimations);
      }
      
      // Check all animatables and animate them if necessary
          $animatables.each(function(i) {
         var $animatable = $(this);
              if (($animatable.offset().top + $animatable.height() - 50) < offset) {
          $animatable.removeClass('animatable').addClass('animated');
              }
      });
  
      };
    
    // Hook doAnimations on scroll, and trigger a scroll
      $(window).on('scroll', doAnimations);
    $(window).trigger('scroll');
  
  });




  $(".burger").click(function() {
    $(this).toggleClass('active');
    $("ul.menus li").slideToggle('fast');
})

$(window).resize(function() {
    if ($(window).width() > 768) {
        $('ul.menus li').removeAttr('style');
    }
})