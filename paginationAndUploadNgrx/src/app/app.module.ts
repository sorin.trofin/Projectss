import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule  } from '@angular/common/http';
import { MessageService } from './message.service';
import { TryesComponent } from './tryes/tryes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared-module/shared.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { JwtModule } from '@auth0/angular-jwt';
import { UserService } from './user.service';
import { AuthenticationService } from './authentication.service';
import { AppSnackBarService } from './app-snackbar-service';
import { SecurityService } from './security-service';
import { CookieService } from 'ngx-cookie-service';
import { TriesComponent } from './tries/tries.component';
import { CarComponent } from './car/car.component';
import { DynamicComponentDirective } from './dynamic-component.directive';
import { Dyn1Component } from './dyn1/dyn1.component';
import { Dyn2Component } from './dyn2/dyn2.component';
import { NgrxComponent } from './ngrx/ngrx.component';
import { counterReducer } from './ngrx/reducer/counter.reducer';
import { ExampleService } from './example.service';
// import { dogReducer } from './ngrx/reducer/dog.reducer';
import { HttpEffectsService } from './ngrx/effects/http-effects.service';

import { StoreModule } from '@ngrx/store';
import { reducers, effects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { FileUploadComponent } from './upload/file-upload/file-upload.component';
import { UploadContainerComponent } from './upload/upload-container.component';
import { UploadComponent } from './upload/uploads/upload.component';
import { DragDirective } from './directives/drag.directive';
import { RxjsComponent } from './rxjs/rxjs.component';
import { OperatorsComponent } from './rxjs/operators/operators.component';
import { RxJSHomeComponent } from './rxjs/home/home.component';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}
@NgModule({
  declarations: [
    AppComponent,
    TryesComponent,
    LoginComponent,
    RegisterComponent,
    TriesComponent,
    CarComponent,
    DynamicComponentDirective,
    Dyn1Component,
    Dyn2Component,
    NgrxComponent,
    UploadComponent,
    FileUploadComponent,
    UploadContainerComponent,
    DragDirective,
    RxjsComponent,
    OperatorsComponent,
    RxJSHomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4000'],
        blacklistedRoutes: ['localhost:4000/api/auth']
      }
    }),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature(effects),
    StoreModule.forRoot({}),
    StoreModule.forFeature('animals', reducers)
  ],
  providers: [
    MessageService,
    AuthGuard,
    UserService,
    AuthenticationService,
    AppSnackBarService,
    SecurityService,
    CookieService,
    ExampleService
  ],
  bootstrap: [AppComponent],
  exports: [
    CarComponent,
    DynamicComponentDirective,
    Dyn1Component,
    Dyn2Component,
    NgrxComponent,
    UploadComponent,
    FileUploadComponent,
    UploadContainerComponent,
    DragDirective
  ],
  entryComponents: [Dyn1Component, Dyn2Component]
})
export class AppModule { }
