import { Directive, HostListener, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appDrag]'
})
export class DragDirective {
  private filesChangeEmiter: EventEmitter<FileList> = new EventEmitter();
  
  constructor() { }
  
  @HostListener('dragover', ['$event']) public onDrop(evt): void {
    evt.preventDefault();
    evt.stopPropagation();

    const files = evt.dataTransfer.files;
    if (files.length > 0){
      this.filesChangeEmiter.emit(files);
    }
    console.log('evt', evt);
    
  }
}
