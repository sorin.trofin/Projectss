import { Component, OnInit, OnChanges } from '@angular/core';
import { DogsService } from '../dogs.service';

@Component({
  selector: 'app-upload-container',
  templateUrl: './upload-container.component.html',
  styleUrls: ['./upload-container.component.scss']
})
export class UploadContainerComponent implements OnInit {
  public uploadsReceived: {}[] = [];
  public fileType: string;

  constructor(private dogsService: DogsService) {
    this.uploadsReceived = [];
   }

  public ngOnInit(): void {
    this.getAttachments();
  }

  public getAttachments(): void {
    this.dogsService.getAttachments().subscribe(response => {
      if (response) {
        this.uploadsReceived = response.uploads;
        this.fileType = response.fileType;
      }
    });
  }

  public refresh(event): void {
    if (event) {
      console.log('event', event);
      this.getAttachments();
    }
  }
}
