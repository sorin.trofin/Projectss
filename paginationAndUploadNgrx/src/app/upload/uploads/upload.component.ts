import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DogsService } from '../../dogs.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Output() refresh = new EventEmitter<boolean>(true);
  
  public uploadedFiles: any = [];
  public uploadForm: FormGroup;
  public uploadsReceived: {}[] = [];
  public filePlace: {} = {};
  
  constructor(
    private formBuilder: FormBuilder,
    private dogsService: DogsService
    ) {
    this.uploadedFiles = [];
    this.uploadsReceived = [];
   }

  public ngOnInit(): void {
    this.uploadForm = this.formBuilder.group({
      attachments: [''],
    });

    if (this.data) {
      this.uploadedFiles = this.data;
      this.uploadedFiles.forEach(attachment => {
        this.filePlace = {
          id: attachment.id,
          fileName: attachment.originalname,
          fileUrl: attachment.path
        };
      });
    }
  }

  ngOnChanges(results): void {
    let data;
    data = results.data.currentValue;
    
    data.forEach(item => {
      console.log('item', item);
      this.filePlace = {
        id: item.id,
        fileName: item.originalname,
        fileUrl: item.path
      };
    });
  }

  public fileChangeHandler(file, fieldName = ''): void {
    if (Object.keys(file).length > 0) {
      file.attachmentType = fieldName;
      this.uploadedFiles[fieldName] = file;
      this.refresh.emit(true);

    } else {
      delete this.uploadedFiles[fieldName];
    }
  }

  public onSubmitEditSyncHandler(): void {
    const attachmentData = this.uploadForm.value;

    let attachments;

    attachments = Object.assign({}, this.uploadedFiles[0]);
    // for (const key of Object.keys(this.uploadedFiles)) {
    //   attachments.push(this.uploadedFiles[key]);
    // }

    // attachments.forEach(element => {
    //   attachmentData.id = element.id;
    //   attachmentData.attachments = element.attachments;
    // });

    // attachmentData.attachments = attachments.slice();

    this.dogsService.updateAttachment(attachments).subscribe(response => {
      if (response) {
      
      }
    });
  }

}
