import { Component, OnInit, ViewChild, Input, ViewEncapsulation, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { of } from 'rxjs';
import { DogsService } from 'src/app/dogs.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FileUploadComponent implements OnInit, OnChanges {
  @ViewChild('file') file;
  @Input() placeholder: string;
  @Input('showNumberPercentage') showNumberPercentage;
  @Input('uploadType') uploadType;
  @Input('filePlace') filePlace;
  @Input('trackId') trackId;
  @Input('isDisabled') isDisabled = false;
  @Output() changeFile = new EventEmitter<{}>();

  private files: Set<File> = new Set();
  public percentage = 0;
  public fileData = {};
  public localFileName = '';
  public uploading = false;

  constructor(
    private dialog: MatDialog,
    private dogsService: DogsService) {
    this.placeholder = this.placeholder || 'Select file';
  }

  public ngOnInit(): void {
    this.updateFilePlace();
  }

  public ngOnChanges(): void {
    this.updateFilePlace();
  }

  private updateFilePlace(): void {
    if (this.filePlace) {
      this.fileData = this.filePlace;
      this.localFileName = this.filePlace['fileName'] || this.placeholder;
    }
  }

  private reset(): void {
    this.uploading = false;
    this.localFileName = '';
    this.fileData = {};
    this.percentage = 0;
  }

  private getUploadSettings(settings): any {
    const response = Object.assign({}, settings);

    if (this.trackId) {
      response.trackId = this.trackId;
    }

    return response;
  }

  public fileChangeHandler(event): void {
    // Reset percentage when error occurres.
    this.percentage = 0;
    event.stopPropagation();
    // Uploading started...
    this.uploading = true;

    // Clear all files.
    this.files.clear();

    const files: { [key: string]: File } = this.file.nativeElement.files;

    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.files.add(files[key]);
      }
    }

    // For the moment, only use first file (the only one, actually).
    this.localFileName = files[0].name;

    let settings = {
      files: this.files,
      type: this.uploadType
    };

    settings = this.getUploadSettings(settings);
    console.log('fileData', files[0]);
   
    this
    .dogsService
    .upload(settings)[files[0].name]
    .pipe(
      // catchError(err => {
      //   this.reset();
  
      //   // Really?!
      //   if (err && err.error && err.error.errors && err.error.errors.userMessage && err.error.errors.userMessage.media) {
      //     // Show info message.
      //   }/*  else {
      //     this.appSnackBarService.show('An error occurred, please try again.', 'error');
      //   } */
  
      //   return of({});
      // })
      ).subscribe(
      event => {
        this.percentage = event['percentDone'];
        // this.percentage = event.percentDone;

        if (this.percentage === 100 && event['data']) {
          this.fileData = event['data'];
          this.fileData['fileName'] = event['data'].filename;

          this.changeFile.emit(this.fileData);
          // Show info message.
          // this.appSnackBarService.show('File successfully uploaded: ' + this.localFileName, 'success');

          this.uploading = false;
        }
      });
  }

  public cancelClickHandler(): void {
    console.log('fileData', this.fileData);
    
    this.dogsService.deleteUpload(this.fileData).subscribe(response => {
    });

    this.localFileName = '';
    this.file.nativeElement.value = '';
    this.reset();
    this.changeFile.emit(this.fileData);
  }

  public download(file): void {
    console.log('file', file);
    
    this.dogsService.download(file).subscribe(response => {
        const fileResponse = response['upload'][0];
        const anchor = document.createElement('a');
    
        anchor.style.display = 'none';
        (<any>document.body).append(anchor);
    
        const click = new MouseEvent('click', {
          'view': window,
          'bubbles': true,
          'cancelable': true
        });
       
        anchor.setAttribute('href', fileResponse.fileUrl);
        anchor.setAttribute('target', '_self');
        anchor.setAttribute('download', fileResponse.filename);
  
        anchor.dispatchEvent(click);
        anchor.remove();
  
        window.addEventListener('focus', e => URL.revokeObjectURL(anchor.href), { once: true });
      });
  }
}
