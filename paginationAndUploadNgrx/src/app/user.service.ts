import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/user.model';

@Injectable()
export class UserService {
    public postsUrl = 'http://localhost:4600';
    constructor(private http: HttpClient) { }

    public getAll() {
        return this.http.get<User[]>(this.postsUrl + `/users`);
    }

    public getById(id: number) {
        return this.http.get(this.postsUrl + `/users/` + id);
    }

    public register(user: User) {
        return this.http.post(this.postsUrl + `/users`, user);
    }

    public update(user: User) {
        return this.http.put(this.postsUrl + `/users/` + user.id, user);
    }

    public delete(id: number) {
        return this.http.delete(this.postsUrl + `/users/` + id);
    }
}