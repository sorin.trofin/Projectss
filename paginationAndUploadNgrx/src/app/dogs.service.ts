import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable, ObservableLike, Subject } from 'rxjs';
import { Dog } from 'src/app/dog';
import { map } from 'rxjs/operators';
import { Post } from 'src/app/post';

@Injectable({
  providedIn: 'root'
})
export class DogsService {
  constructor(private http: HttpClient) {}
  public dogsUrl = 'http://localhost:4600/dogs';
  public dogsUrlPut = 'http://localhost:4600/dogs';

  public postsUrl = 'http://localhost:4600/posts';
  public postsUrlPut = 'http://localhost:4600/post';
  public uploadUrl = 'http://localhost:4600/uploads';
  public downloadUrl = 'http://localhost:4600/download';

  /// DOGS METHODS
  public getDogs(limit = 5, offset = 1, sort= 'fullname'): Observable<Dog[]> {
    const params = new HttpParams()
    .set('offset', offset.toString())
    .set('limit', limit.toString())
    .set('sort', sort);

    return this.http.get<Dog[]>(this.dogsUrl, {
      params: params,
    });
  }

  public getDogsById(id): Observable<Dog[]> {
    return this.http.get<Dog[]>(this.dogsUrl + '/' + id);
  }

  public addDog(dog: Dog): Observable<Dog> {
    return this.http.post<Dog>(this.dogsUrl, dog);
  }

  public deleteDog(dog: Dog): Observable<Dog[]> {
    const id = typeof dog === 'number' ? dog : dog.id;
    const deleteUrl = `${this.dogsUrl}/${id}`;
    return this.http.delete<Dog[]>(deleteUrl);
  }

  public editDog(dog: Dog, logData): Observable<Dog> {
    const editUrl = `${this.dogsUrl}/${dog.id}`;
    return this.http.put<Dog>(editUrl, logData);
  }

  public patchDog(dog, logData): Observable<any> {
    const editUrl = `${this.dogsUrl}/${dog.id}`;
    return this.http.patch<any>(editUrl, logData);
  }

  public addPosts(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl + '/post', post);
  }

  public editPost(post: Post, logData): Observable<Post> {
    const editUrl = `${this.postsUrlPut}/${post.id}`;
    return this.http.put<Post>(editUrl , logData);
  }

  public deletePost(post: Post): Observable<Post[]> {
    const id = typeof post === 'number' ? post : post.id;
    const deleteUrl = `${this.postsUrlPut}/${id}`;
    return this.http.delete<Post[]>(deleteUrl);
  }

  public download(file): Observable<{}> {
    return this.http.get(`${this.uploadUrl}/${file.id}`);
  }

  public upload(settings): {[key: string]: Observable<{}>} {
    const status = {};
    const url = 'http://localhost:4600/uploads';
    console.log('UPLOADSETTINGS', settings);
    settings.files.forEach(file => {
      const body: FormData = new FormData();

      body.append('file', file, file.name);
      body.append('type', settings.type);

      if (settings.dogId) {
        body.append('dogId', settings.dogId);
      }

      const req = new HttpRequest('POST',
                                  url,
                                  body,
                                  {
                                    reportProgress: true
                                  });

      const progress = new Subject<{}>();

      this.http.request(req).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          progress.next({ percentDone: Math.round(100 * event.loaded / event.total) });
        } else if (event.type === HttpEventType.Response) {
          progress.next({ percentDone: 100, data: event.body });
          progress.complete();
        }
      }, err => {
        progress.error(err);
      });

      status[file.name] = progress.asObservable();
    });

    return status;
  }
  
  public updateAttachment(attachmentData): Observable<any> {
    console.log('attachmentData', attachmentData);
    return this.http.put<any>(`${this.uploadUrl}/` + attachmentData.id, attachmentData);
  }

  public getAttachments(): Observable<any> {
    return this.http.get<any>(this.uploadUrl);
  }

  public deleteUpload(attachment): Observable<any> {
    const id = typeof attachment === 'number' ? attachment : attachment.id;
    const deleteUrl = `${this.uploadUrl}/${id}`;
    return this.http.delete<any[]>(deleteUrl);
  }
}
