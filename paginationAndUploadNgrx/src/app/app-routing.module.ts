import { TryesComponent } from './tryes/tryes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { CarComponent } from './car/car.component';
import { NgrxComponent } from './ngrx/ngrx.component';
import { UploadContainerComponent } from './upload/upload-container.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { OperatorsComponent } from './rxjs/operators/operators.component';
import { RxJSHomeComponent } from './rxjs/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/tryes', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent  },
  { path: 'tryes', component: TryesComponent },
  { path: 'cars', component: CarComponent },
  { path: 'ngrx', component: NgrxComponent },
  { path: 'upload', component: UploadContainerComponent },
  { 
    path: 'rxjs', component: RxjsComponent,
    children: [
      {
        path: 'home',
        component: RxJSHomeComponent
      },
      {
        path: 'operators',
        component: OperatorsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    [RouterModule.forRoot(routes)],
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [
    AuthGuard
  ],
})

export class AppRoutingModule {

}
