import { Action } from '@ngrx/store';
import { Dog } from 'src/app/dog';

/// load DOGS
export const LOAD_DOGS = '[Dogs] Load Dogs';
export const LOAD_DOGS_FAIL = '[Dogs] Load Dogs Fail';
export const LOAD_DOGS_SUCCESS = '[Dogs] Load Dogs Success';
export const CREATE_DOG = '[Dogs] Create Dog';
export const CREATE_DOG_SUCCESS = '[Dogs] Create Dog Success';
export const DELETE_DOG = '[Dogs] Delete Dog';
export const DELETE_DOG_SUCCESS = '[Dogs] Delete Dog Success';
export const UPDATE_DOG = '[Dogs] Update Dog';
export const UPDATE_DOG_SUCCESS = '[Dogs] Update Dog Success';

export class LoadDogs implements Action {
    public readonly type = LOAD_DOGS;
}

export class LoadDogsFail implements Action {
    public readonly type = LOAD_DOGS_FAIL;
    constructor(public payload: any) {}
}

export class LoadDogsSuccess implements Action {
    public readonly type = LOAD_DOGS_SUCCESS;
    constructor(public payload: Dog[]) {}
}

export class CreateDog implements Action {
    public readonly type = CREATE_DOG;
    constructor(public payload) {}
}

export class CreateDogSuccess implements Action {
    public readonly type = CREATE_DOG_SUCCESS;
    constructor(public payload) {}
}

export class DeleteDog implements Action {
    public readonly type = DELETE_DOG;
    constructor(public payload) {}
}

export class DeleteDogSuccess implements Action {
    public readonly type = DELETE_DOG_SUCCESS;
    constructor(public payload) {}
}

export class UpdateDog implements Action {
    public readonly type = UPDATE_DOG;
    constructor(public payload, public formData) {}
}

export class UpdateDogSuccess implements Action {
    public readonly type = UPDATE_DOG_SUCCESS;
    constructor(public payload, public formData) {}
}

export type DogsAction =
  LoadDogs |
  LoadDogsFail |
  LoadDogsSuccess |
  CreateDog |
  CreateDogSuccess |
  DeleteDog |
  DeleteDogSuccess |
  UpdateDog |
  UpdateDogSuccess;
