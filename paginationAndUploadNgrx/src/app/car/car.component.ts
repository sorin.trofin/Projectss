import { Component, ComponentFactoryResolver, ViewChild, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Car } from 'src/app/car.interface';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppSnackBarService } from '../app-snackbar-service';
import { DynamicComponentDirective } from '../dynamic-component.directive';
import { Dyn1Component } from '../dyn1/dyn1.component';
import { Dyn2Component } from '../dyn2/dyn2.component';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DynamicComponentDirective) public dynamicHost: DynamicComponentDirective;
  public multiComponents: any = [];
  public interval: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private appSnackBarService: AppSnackBarService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.multiComponents = [Dyn1Component, Dyn2Component];
  }

  public ngOnInit() {
    this.dynamicHost.viewContainerRef.clear();
  }

  public ngAfterViewInit() {
    this.loadComponent();
  }

  public ngOnDestroy() {
    this.dynamicHost.viewContainerRef.clear();
  }

  public loadComponent() {
    this.multiComponents.forEach(element => {
      this.dynamicHost.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(element));
    });
  }
}
