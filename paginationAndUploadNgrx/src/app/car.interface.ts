export interface Car {
    name: string;
    year: number;
    model: string;
}
