import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, catchError, map, tap, mergeMap, startWith, exhaustMap} from 'rxjs/operators';
import { DogsService } from 'src/app/dogs.service';
import { ActionTypes } from '../actions/dog.actions';
// import { JsOrder, Order } from '../models/order';
// import { OrderService } from '../../services/order.service';
import { Dog } from 'src/app/dog';
import * as dogActions from '../actions/dog.actions';


@Injectable({
  providedIn: 'root'
})
export class HttpEffectsService {

  constructor(
    private actions$: Actions,
    private dogService: DogsService,
) {
  console.log('ActionTypes.GetDogs', ActionTypes.GetDogs);
}

// @Effect()
// public loadDogs$ = this.actions$
//   .pipe(
//     ofType(ActionTypes.GetDogs),
//     switchMap(() => this.dogService.getDogs()
//       .pipe(
//         map(dogs => ({ type: ActionTypes.GetDogs, payload: dogs })),
//       ))
//     );
//   }

// @Effect()
// public loadDogs$: Observable<Action> = this.actions$.pipe(
//   ofType<dogActions.GetDogsAction>(
//     dogActions.ActionTypes.GetDogs
//   ),
//   startWith(new dogActions.GetDogsAction()),
//   switchMap(action =>
//     this.dogService
//       .getDogs()
//       .pipe(
//         map(
//           items =>
//             new dogActions.GetDogsAction()
//           ),
//           catchError(error =>
//             observableOf(new dogActions.GetDogsAction())
//           )
//       )
//    )
// )

@Effect()
public loadDogs$ = this.actions$.pipe(
    ofType(ActionTypes.GetDogs),
    switchMap(() => {
      return this.dogService.getDogs().pipe(
        map(dogs => new dogActions.GetDogsSuccess())
      );
    })
);
}
