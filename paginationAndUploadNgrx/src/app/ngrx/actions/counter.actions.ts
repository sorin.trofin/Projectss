import { Action } from '@ngrx/store';

export enum ActionTypes {
  Increment = '[Counter Component] Increment',
  Decrement = '[Counter Component] Decrement',
  Reset = '[Counter Component] Reset',
  GetDogs = '[Get Dogs] Get Dogs'
}

export class Increment implements Action {
  public readonly type = ActionTypes.Increment;
}

export class Decrement implements Action {
  public readonly type = ActionTypes.Decrement;
}

export class Reset implements Action {
  public readonly type = ActionTypes.Reset;
}

export class GetDogs implements Action {
  public readonly type = ActionTypes.GetDogs;
}
