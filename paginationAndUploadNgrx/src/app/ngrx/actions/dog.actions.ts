import { Action } from '@ngrx/store';

export enum ActionTypes {
  GetDogs = '[Get Dogs] Get Dogs',
  GetDogsSuccess = '[Get Dogs Success] Get Dogs'
}

export class GetDogsAction implements Action {
  public readonly type = ActionTypes.GetDogs;
}

export class GetDogsSuccess implements Action {
  public readonly type = ActionTypes.GetDogsSuccess;
}

export type Actions
  = GetDogsAction | GetDogsSuccess;
