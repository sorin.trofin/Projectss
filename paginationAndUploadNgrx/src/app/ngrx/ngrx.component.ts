import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from '@angular/forms';
import { Observable } from 'rxjs';
import { Increment, Decrement, Reset, ActionTypes } from './actions/counter.actions';
import { mergeMap, map, delay, switchMap, concatMap, filter, tap, merge} from 'rxjs/operators';
import { of, concat, forkJoin, from } from 'rxjs';
import { ExampleService } from '../example.service';
import * as fromStore from '../store';
import { DogsService } from '../dogs.service';
import { Dog } from 'src/app/dog';
// import { dogReducer } from 'src/app/ngrx/reducer/dog.reducer';


@Component({
  selector: 'app-ngrx',
  templateUrl: './ngrx.component.html',
  styleUrls: ['./ngrx.component.scss']
})
export class NgrxComponent implements OnInit {
  public count$: Observable<number>;
  public id: number;
  public subscriber;
  public formdata: FormGroup;
  public editDogGroup: FormGroup;
  public dogs$: Observable<Dog[]>;
  public editForm: FormArray;
  public getLoading: Observable<boolean>;
  public loading: boolean;
  public getLoaded: Observable<boolean>;
  public loaded: boolean;

  constructor(
    private store: Store<fromStore.AnimalsState>,
    private formBuilder: FormBuilder,
    private dogService: DogsService,
    ) {
   }

  public ngOnInit() {
    this.formdata = this.formBuilder.group({
      fullname: ['', Validators.required],
      rasa: ['', Validators.required],
    });

    this.editDogGroup = this.formBuilder.group({
      editForm: this.formBuilder.array([])
    });

    this.dogs$ = this.store.select(fromStore.getAllDogs);
    this.getLoading = this.store.select(fromStore.getDogsLoading);

    this.getLoading.subscribe(loading => {
      this.loading = loading;
      console.log('this.loading', this.loading);
    });

    this.getLoaded = this.store.select(fromStore.getDogsLoaded);
    this.getLoaded.subscribe(loaded => {
      this.loaded = loaded;
      console.log('this.loaded', this.loaded);
    });

    this.store.dispatch(new fromStore.LoadDogs());

    this.dogs$.subscribe(dogs => {
      dogs.forEach(dog => {
        this.editForm = this.editDogGroup.get('editForm') as FormArray;
        this.editForm.push(this.formBuilder.group({
          fullname: ['', Validators.required],
          rasa: ['', Validators.required],
        }));
      });
    });
  }

  public addDog(): void {
    const dogData = this.formdata.value;
    if (this.formdata.invalid) {
      return;
    }

    this.formdata.reset();
    this.store.dispatch(new fromStore.CreateDog(dogData));
    this.editForm = this.editDogGroup.get('editForm') as FormArray;
    this.editForm.push(this.formBuilder.group({
      fullname: ['', Validators.required],
      rasa: ['', Validators.required],
    }));
  }

  public deleteDog(dog): void {
    const dogData = dog;
    this.store.dispatch(new fromStore.DeleteDog(dogData));
  }

  public editDog(dog, i) {
    const editDogData = this.editDogGroup.value.editForm[i];

    // @ts-ignore
    (this.editDogGroup.get('editForm') as FormArray).controls[i].reset();
    this.store.dispatch(new fromStore.UpdateDog(dog, editDogData));
  }
}
