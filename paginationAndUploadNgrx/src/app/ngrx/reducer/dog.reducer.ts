import { Action } from '@ngrx/store';
import { ActionTypes } from '../actions/dog.actions';
import { Dog } from 'src/app/dog';

const initialState = [];

export function dogReducer(state: Dog[] = initialState, action) {
  switch (action.type) {
    case ActionTypes.GetDogs: {
      return {
        ...state,
        loading: true
      };
    }

    case ActionTypes.GetDogsSuccess: {
      return {
        ...state,
        loading: false,
        loaded: true
      };
    }

    default:
      return state;
  }
}
