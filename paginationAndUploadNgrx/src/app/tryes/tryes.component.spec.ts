import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryesComponent } from './tryes.component';

describe('TryesComponent', () => {
  let component: TryesComponent;
  let fixture: ComponentFixture<TryesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
