
import { Component, OnInit, ViewChild, QueryList, ViewChildren, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from '@angular/forms';
import { DogsService } from 'src/app/dogs.service';
import { Dog } from 'src/app/dog';
import { filter, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { from } from 'rxjs';
import { Observable } from 'rxjs';
import { interval, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { takeWhile } from 'rxjs/operators';
import { toArray, take } from 'rxjs/operators';
import { Subject, merge } from 'rxjs';
// import { ReadComponent } from 'src/app/read/read.component';
import { log } from 'util';
import { AuthenticationService } from '../authentication.service';
import { MatPaginator, PageEvent } from '@angular/material';
import { tap } from 'rxjs/internal/operators/tap';

@Component({
  selector: 'app-tryes',
  templateUrl: './tryes.component.html',
  styleUrls: ['./tryes.component.scss']
})
export class TryesComponent implements OnInit, AfterViewInit {
  @ViewChildren('dogCheckbox') public dogCheckbox: QueryList<any>;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  public months = ['January', 'Feburary', 'March', 'April',
           'May', 'June', 'July', 'August', 'September',
           'October', 'November', 'December'];
  public title = 'Angular 4 Project!';
  public isavailable = false;
  public option: string;
  public editState: boolean;
  public dogs: any = [];
  public formdata: FormGroup;
  public hideme: {} = {};
  public fromObservable = [];
  public sub = new Subject();
  public length = 20;
  public pageSize = 5;
  public pageSizeOptions = [5, 10, 15, 20];
  public pageIndex: number;
  // @ViewChild('readComponent') public readComponent: ReadComponent;
  public pageSizePaginator: number;

  constructor(
    private dogService: DogsService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {
      this.pageIndex = 0;
   }

  public ngOnInit(): void {
    this.formdata = this.formBuilder.group({
      fullname: ['', Validators.required],
      rasa: ['', Validators.required],
    });
    
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = this.pageSize;

    this.getDogs();
  }

  public ngAfterViewInit(): void {
    merge()
      .pipe(
        tap(() => {
          this.paginator.pageIndex = 0;
          this.paginator.page.next(new PageEvent());
        })
      )
      .subscribe();

    // Listen for MatPaginator component.
    // When a pagination event is triggered, loadProjects with new filters.
    this
      .paginator
      .page
      .subscribe(() => {
        this.getDogs();
        console.log('thisPAgeSize', this.paginator.pageSize);
        console.log('this.paginator.pageIndex', this.paginator.pageIndex);
      });
  }

  public getDogs(): void {
    const offSet = this.paginator.pageIndex * this.paginator.pageSize;
    console.log('this.paginator.pageIndex,this.paginator.pageSize', this.paginator.pageIndex, this.paginator.pageSize);
    console.log('offSet', typeof offSet, offSet);

    this.dogService.getDogs(this.paginator.pageSize, offSet).subscribe((response) => {
      this.dogs = response;
      this.editState = false;
      this.hideme = {};
      console.log('thisdogs', this.dogs);
    });
  }

  public getDogsForObservable(): any {
    this.dogService.getDogs().subscribe((response) => {
      this.dogs = response;
      this.editState = false;
      this.hideme = {};
    });

    return this.dogs;
  }

  public addDog(formData, formDirective: FormGroupDirective): void {
    const dogData = this.formdata.value;

    if (this.formdata.invalid) {
      return;
    }
    console.log('formDirective', formDirective);
    this.dogService.addDog(dogData).subscribe((response) => {
      if (response) {
        console.log(response);
        this.getDogs();
        formDirective.resetForm();
        this.formdata.reset();
      }
    });
  }

  public delete(dog: Dog): void {
    // this.appdogs = this.appdogs.filter((h) => h !== dog);
    this.dogService.deleteDog(dog).subscribe(response => {
      if (response) {
        this.getDogs();
      }
    });
  }

  public editDog(dog): void {
    const editDogData = this.formdata.value;

    this.editState = false;
    this.dogService.editDog(dog, editDogData).subscribe((response) => {
    if (response) {
      this.getDogs();
    }
    this.formdata.reset();
    });
  }

  public patchDog(dog): void {
    const editDogData = {
      fullname: this.formdata.value.fullname
    };

    console.log('dog', editDogData);
    this.editState = false;
    this.dogService.patchDog(dog, editDogData).subscribe((response) => {

    if (response) {
      this.getDogs();
    }
    this.formdata.reset();
    });
  }

  public changeCheckbox(event): void {
    this.dogCheckbox.toArray().forEach(all => {
      if (all._checked) {
        console.log('all', all);
      }
    });
  }

  public isLoggedIn(): boolean {
    return this.authenticationService.loggedIn();
  }

  public function(): void {
    //   const squareOf2 = of(1, 2, 3, 4, 5, 6)
    //   .pipe(
    //   filter(num => num % 2 === 0),
    //   map(num => num * num)
    // );
    //   squareOf2.subscribe( (num) => console.log('waaa', num));
    // const arraySource = from([1, 2, 3, 4, 5]);
    // const subscribe = arraySource.subscribe(val => console.log('vall', val));
    // const hello = Observable.create(function(observer) {
    //   observer.next('Hello');
    //   observer.next('World');
    //   });

    // hello.subscribe(val => console.log(val));
    // const source = of(this.getDogsForObservable());
    // source.subscribe(val => this.fromObservable = val);
    // console.log('from', this.fromObservable);

    // const source = interval(1000);
    // // after 5 seconds, emit value
    // const timer$ = timer(5000);
    // // when timer emits after 5s, complete source
    // const example = source.pipe(takeUntil(timer$));
    // // output: 0,1,2,3
    // const subscribe = example.subscribe(val => console.log(val));

    // const source = of(1, 2, 3, 4, 5);
    // // allow values until value from source is greater than 4, then complete
    // const example = source.pipe(takeWhile(val => val <= 3));
    // // output: 1,2,3,4
    // const subscribe = example.subscribe(val => console.log(val));
    // this.sub.next(1);
    // this.sub.subscribe(console.log);
    // this.sub.next(5); // OUTPUT => 2
    // this.sub.next(8); // OUTPUT => 3,3 (logged from both subscribers)
    // this.readComponent.fromReadComponent();
    // this.readComponent.fromReadComponent2();
    }
  // public changemonths(event) {
  //   console.log('Changed month from the Dropdown');
  //   this.option = event.target.value;
  //   if (event.target.value === 'Feburary') {
  //     this.isavailable = true;
  //   } else {
  //     this.isavailable = false;
  //   }
  //   console.log(this.option);
  //   console.log(this.isavailable);
  // }

  // public optionClick(opt): void {
  //   console.log('optionvalue', opt);
  // }

  // public myClickFunction(event) {
  //    // just added console.log which will display the event details in browser on click of the button.
  //    alert(this.option);
  //    console.log(event.target.value);
  // }
}
