import { Component, OnInit, ViewChild, OnDestroy, Input, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatMenuTrigger } from '@angular/material';
import { FilterService } from 'app/_shared/services/filter-service/filter.service';
import { FilterComponent } from 'app/_shared/components/filter/filter.component';
import { SearchComponent } from 'app/_shared/components/search/search.component';
import { SearchService } from 'app/_shared/services/search-service/search.service';
import { ProjectsAPI } from 'app/main/projects/projects.api';

@Component({
  selector: 'side-buttons',
  templateUrl: './side-buttons.component.html',
  styleUrls: ['./side-buttons.component.scss']
})
export class SideButtonsComponent implements OnInit, OnDestroy {
  @ViewChildren(MatMenuTrigger) trigger: QueryList<MatMenuTrigger>;
  @ViewChild(FilterComponent) filter: FilterComponent;
  @Input('isCompletedProject') isCompletedProject = false;  
  @ViewChild(SearchComponent) search: SearchComponent;

  private subscriber;
  private filterSubscriber;
  private searchFilterSubscriber;
  private readonly projectId: number;
  public isFilterActive = false;
  public isSearchActive = false;

  constructor(
    private route: ActivatedRoute,
    public filterService: FilterService,
    public searchService: SearchService,
    public projectsApi: ProjectsAPI
  ) {
    this.projectId = this.route.parent.snapshot.params.projectId;
  }

  ngOnInit(): void {
    this.getFilterActive();
    this.getSearchFilterActive();
  }
  
  public closeSearchMenu(): void {
    ///// First Mat-Menu from ViewChildren (Search Component)
    this.trigger.toArray()[0].closeMenu();
  }

  public closeMenu(): void {
    ///// Secound Mat-Menu from ViewChildren (Filter Component)
    this.trigger.toArray()[1].closeMenu();
  }
  
  public getFilterActive(): void {
    this.filterSubscriber = this.filterService.getFilterDetails().subscribe(item => setTimeout(() => {
     
      // @ts-ignore
      this.isFilterActive = Object.values(item).reduce(function(reducer, value): any { return Number(Boolean(value.length)) + reducer; }, 0) !== 0;
    }));
  }

  public getSearchFilterActive(): void {
    this.searchFilterSubscriber = this.searchService.getSearchDetails().subscribe(item => setTimeout(() => {
   
      this.isSearchActive = Object.values(item).reduce(function(reducer, value): any {
          if (value) {
           
              // @ts-ignore
              return Number(Boolean(value.length)) + reducer; 
          }
        }, 0) !== 0;
    }));
  }

  public ngOnDestroy(): void {
    this.filterSubscriber.unsubscribe();
    this.searchFilterSubscriber.unsubscribe();
  }

  public resetFilters(): void {
    this.filter.hardResetFilters();
  }

  public resetSearchFilter(): void {
    this.search.resetSearchFilter();
  }

  public downloadPdfHandler(event, category, printOpt): void {
    const payload = {
      projectId: this.projectId,
      printOption: printOpt,
      view: category.indexOf('view') > -1 ? 1 : null
    };

    this.projectsApi.downloadPreviewPdf(payload).subscribe(response => {
      if (response) {
        console.log('response', response);
        window.open(response.fileUrl);
      }
    });
  }
}
