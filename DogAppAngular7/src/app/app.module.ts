import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule } from '@angular/forms';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';
import { HelloComponent } from './hello/hello.component';
import { HttpClientModule } from '@angular/common/http';
import { DogsComponent } from './dogs/dogs.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HelloChildComponent } from './hello-child/hello-child.component';
@NgModule({
	declarations: [AppComponent, ServerComponent, ServersComponent, HelloComponent, DogsComponent, HelloChildComponent],
	imports: [
		BrowserModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatCheckboxModule,
		MatSidenavModule,
		MatButtonModule,
		MatExpansionModule,
		FormsModule,
		ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' })
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
