export class PostData {
	fullname: string;
	rasa: string;
	adopted: string;

	constructor(fullname, rasa, adopted) {
		this.fullname = fullname;
		this.rasa = rasa;
		this.adopted = adopted;
	}
}
