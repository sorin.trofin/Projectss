import { Component, OnInit } from '@angular/core';
import { Dog } from '../dog';
import { PostData } from '../post-data';
import { DogsService } from '../dogs.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { log } from 'util';

@Component({
	selector: 'app-dogs',
	templateUrl: './dogs.component.html',
	styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {
	appdogs: Dog[];
	editState: boolean;
	public dogForm: FormGroup;
	public hideme: Object = {};
	public options = [
		{ optionName: 'Adopted' },
		{ optionName: 'Not Adopted' },
	]

	constructor(private dogService: DogsService,
		private formBuilder: FormBuilder) {
		this.dogService.getDogs().subscribe((dogs: Dog[]) => {
			this.appdogs = dogs;
			this.editState = false;
			this.hideme = {};
		});
	}

	ngOnInit() {

		this.dogForm = this.formBuilder.group({
			fullname: ['', Validators.required],
			rasa: ['', Validators.required],
			adopted: ['', Validators.required],
			checked: ['']
		})
	}

	public getDogs() {
		this.dogService.getDogs().subscribe((dogs: Dog[]) => {
			this.appdogs = dogs;
		});
	}

	public delete(dog: Dog): void {
		this.appdogs = this.appdogs.filter((h) => h !== dog);
		this.dogService.deleteDog(dog).subscribe();
	}

	public add() {
		if (this.dogForm.get('checked').value == '') {
			this.dogForm.get('checked').setValue(false);
		}
		const dogData = this.dogForm.value;


		this.dogService.addDog(dogData).subscribe((response) => {
			this.appdogs.push(response);
			if (response) {
				this.getDogs();
				this.dogForm.reset();
			}
		});
	}

	public editDog(dog) {
		const editDogData = this.dogForm.value;

		this.editState = false;
		this.dogService.editDog(dog, editDogData).subscribe((response) => {
			if (response) {
				this.getDogs();
			}
			this.dogForm.reset();
		})
	}
}
