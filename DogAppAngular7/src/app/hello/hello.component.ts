import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-hello',
	templateUrl: './hello.component.html',
	styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
	public eu: string;
	public deLaCopil: string;
	public array: Object[];

	constructor() {
		this.eu = 'EU PARINTE';
		this.array = [];
	}

	ngOnInit() { }

	public receiveArray($event) {
		this.array = $event;
	}

	public receiveValue($event) {
		this.deLaCopil = $event;
	}
}
