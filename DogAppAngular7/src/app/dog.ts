export class Dog {
	id: number;
	fullname: string;
	rasa: string;
	adopted: string;
	healthy: boolean;
}
