import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hello-child',
  templateUrl: './hello-child.component.html',
  styleUrls: ['./hello-child.component.css']
})
export class HelloChildComponent implements OnInit {
  @Input() parinte
  @Output() msgEvent = new EventEmitter<string>();
  @Output() arraySent = new EventEmitter<Array<any>>();

  public deLaCopil: string;

  public array: Array<any>;

  constructor() {
    this.deLaCopil = 'deLaCopil';
    this.array = [
      { name: 'sorin' },
      { name: 'sorin2' },
      { name: 'sorin3' },
      { name: 'sorin4' },
      { name: 'sorin5' },
      { name: 'sorin6' },
    ]
  }

  ngOnInit() {
    this.deLaCopil = 'deLaCopil';

  }

  public emitArray() {
    let plm1 = { name: 'dinobjectassignsssss' };
    const plm = Object.assign({}, plm1);

    // this.array.forEach(item => {

    //   if (item.name === 'dinobjectassign') {
    //     console.log(item);
    //     return;
    //   } else {
    //     this.arraySent.emit(this.array);
    //   }
    // });

    this.array.push(plm);

    this.array.filter((item, index, array) => {
      if (array.map((mapItem) => mapItem['name']).indexOf(item['name']) === index) {
        this.arraySent.emit(this.array);
      } else {
        this.array.pop();
        console.log('wa')
      }
    })

  }

  public emitChild() {
    this.parinte = 'PARINTE MODIFICAT';
    this.msgEvent.emit(this.parinte);
  }

}
