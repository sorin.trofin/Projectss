import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Dog } from './dog';
import { PostData } from './post-data';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
	providedIn: 'root'
})
export class DogsService {
	constructor(private http: HttpClient) { }

	dogsUrl = 'http://localhost:3000/dogs';

	getDogs() {
		return this.http.get<Dog[]>(this.dogsUrl);
	}

	deleteDog(dog: Dog): Observable<Dog[]> {
		const id = typeof dog === 'number' ? dog : dog.id;
		const deleteUrl = `${this.dogsUrl}/${id}`;
		return this.http.delete<Dog[]>(deleteUrl);
	}

	addDog(dog: Dog): Observable<Dog> {
		return this.http.post<Dog>(this.dogsUrl, dog);
	}

	editDog(dog: Dog, logData): Observable<Dog> {
		const editUrl = `${this.dogsUrl}/${dog.id}`;
		return this.http.put<Dog>(editUrl, logData);
	}
}
