/*------------------------------------------------------------------
Gulp file
------------------------------------------------------------------*/
var gulp = require('gulp'),
	/**
	 * Gulpfile Plugins
	 */
	del = require('del');
path = require('path'),
	autoprefixer = require('autoprefixer'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	plumber = require('gulp-plumber'),
	using = require('gulp-using'),
	wiredep = require('wiredep').stream,
	inject = require('gulp-inject'),
	mainBowerFiles = require('main-bower-files'),
	cssnano = require('cssnano'),
	atImport = require('postcss-import'),
	debug = require('gulp-debug'),
	postcss = require('gulp-postcss'),
	sort = require('gulp-sort');
/*
 * Variables
 */
paths = {
	root: 'src/',
	bower: 'src/bower_components/',
	js: 'src/js/',
	sass: 'src/sass/'
};

/*------------------------------------------------------------------
 TASK: sass - used for deployment
 ------------------------------------------------------------------*/

gulp.task('sass', function() {
	var injectStyles = gulp.src([
			path.join(paths.sass, '_core/*.scss'),
			path.join(paths.sass, 'partials/*.scss'),
			path.join(paths.sass, 'pages/**/*.scss'),
			path.join('!' + paths.sass, 'main.scss')
		], {
			read: false
		}),
		injectStylesOptions = {
			transform: function(filePath) {
				filePath = filePath.replace(paths.sass, '');
				return '@import "' + filePath + '";';
			},
			starttag: '// injector',
			endtag: '// endinjector',
			addRootSlash: false
		},
		processors = [
			atImport(),
			autoprefixer('last 8 version'),
			cssnano({
				discardComments: {
					removeAll: true
				}
			})
		];
	
	return gulp.src(paths.sass + 'main.scss')
		.pipe(wiredep({
			directory: 'src/bower_components',
			devDependencies: true,
			onError: function(error) {
				console.log('error', error);
			},
			onPathInjected: function(fileObject) {
				console.log('injected: ', fileObject.path);
			},
			onMainNotFound: function(pkg) {
				console.log('this package has no main: ', pkg);
			},
		}))
		.pipe(inject(injectStyles, injectStylesOptions))
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss(processors))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.root + 'dist'))
		.pipe(notify({ message: 'sass task completed' }));
});

/*------------------------------------------------------------------
 TASK: sass-dev - used in dev mode
 ------------------------------------------------------------------*/

gulp.task('sass-dev', function() {
	var injectStyles = gulp.src([
			path.join(paths.sass, '_core/*.scss'),
			path.join(paths.sass, 'partials/*.scss'),
			path.join(paths.sass, 'pages/**/*.scss'),
			path.join('!' + paths.sass, 'main.scss')
		], {
			read: false
		}),
		injectStylesOptions = {
			transform: function(filePath) {
				filePath = filePath.replace(paths.sass, '');
				return '@import "' + filePath + '";';
			},
			starttag: '// injector',
			endtag: '// endinjector',
			addRootSlash: false
		},
		processors = [
			atImport(),
			autoprefixer('last 8 version')
		];
	
	return gulp.src(paths.sass + 'main.scss')
		.pipe(wiredep({
			directory: 'src/bower_components',
			devDependencies: true,
			onError: function(error) {
				console.log('error', error);
			},
			onPathInjected: function(fileObject) {
				console.log('injected: ', fileObject.path);
			},
			onMainNotFound: function(pkg) {
				console.log('this package has no main: ', pkg);
			},
		}))
		.pipe(inject(injectStyles, injectStylesOptions))
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss(processors))
		.pipe(sourcemaps.write())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.root + 'dist'))
		.pipe(notify({ message: 'sass-dev task completed' }));
});

/*------------------------------------------------------------------
 TASK: vendorjs - used for vendors
 ------------------------------------------------------------------*/

gulp.task('vendorjs', function() {
	return gulp.src(
		mainBowerFiles({
			filter: '**/*.js',
			includeDev: true
		}),
		{
			base: paths.bower
		})
		.pipe(concat('vendor.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest(paths.root + 'dist'))
		.pipe(notify({ message: 'vendorjs task completed' }));
});

/*------------------------------------------------------------------
 TASK: appjs - used for deployment
 ------------------------------------------------------------------*/

gulp.task('appjs', function() {
	return gulp.src(path.join(paths.js, '**/*.js'))
		.pipe(sort())
		.pipe(debug({ 'showFilles': true }))
		.pipe(plumber({
			handleError: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(concat('app.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest(paths.root + 'dist'))
		.pipe(notify({ message: 'appjs task completed' }));
});

/*------------------------------------------------------------------
 TASK: appjs-dev - used in dev mode
 ------------------------------------------------------------------*/

gulp.task('appjs-dev', function () {
	return gulp.src(path.join(paths.js, '**/*.js'))
		.pipe(sort())
		.pipe(debug({ 'showFilles': true }))
		.pipe(plumber({
			handleError: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(concat('app.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.root + 'dist'))
		.pipe(notify({ message: 'appjs-dev task completed' }));
});

/*------------------------------------------------------------------
 TASK: watch - used in dev mode
 ------------------------------------------------------------------*/

gulp.task('watch', function() {
	gulp.watch(paths.js + '**/*.js', ['appjs-dev']);
	
	gulp.watch([
		paths.sass + '**/*.scss',
		path.join('!' + paths.sass, 'main.scss')
	], ['sass-dev']);
});

/*------------------------------------------------------------------
 TASK: clean
 ------------------------------------------------------------------*/

gulp.task('clean', function() {
	return del(paths.root + 'dist/*');
});

/*------------------------------------------------------------------
 TASK: default - used for build - deployment
 ------------------------------------------------------------------*/

gulp.task('default', ['clean'], function() {
	gulp.start('sass', 'vendorjs', 'appjs');
});