import { IApartments } from './../interfaces/iapartments';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApartmentsService {

  constructor() { }
  getApartments() {
    const apartments: IApartments[] = [
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
      new IApartments(
        1,
        'Ap 2 camere etaj 1',
        'Strada Moldovei nr 30',
        'reper',
        2,
        [
          {
            image: 'src/media/apartamente/apartament1/1.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/2.jpg',
          },
          {
            image: 'src/media/apartamente/apartament1/3.jpg',
          }
        ],
        '200 lei',
        'Apartment 2 camere etaj 1 , 200 lei'
      ),
    ]
    return apartments;
  }
}
