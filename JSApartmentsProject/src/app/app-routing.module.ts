import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './site-container/content/pages/Home/Home.component';
import { AboutComponent } from './site-container/content/pages/about/about.component';
import { ContactComponent } from './site-container/content/pages/contact/contact.component';
import { ApartamenteComponent } from './site-container/content/pages/apartamente/apartamente.component';

const routes: Routes = [
	{ path: '', redirectTo: '/acasa', pathMatch: 'full' },
	{ path: 'acasa', component: HomeComponent },
	{ path: 'apartamente', component: ApartamenteComponent },
	{ path: 'despre-noi', component: AboutComponent },
	{ path: 'contact', component: ContactComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
