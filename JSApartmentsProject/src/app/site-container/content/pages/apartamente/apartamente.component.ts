import { ApartmentsService } from './../../../../services/apartments.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apartamente',
  templateUrl: './apartamente.component.html',
  styleUrls: ['./apartamente.component.scss']
})
export class ApartamenteComponent implements OnInit {
  public allApartments = [];
  constructor(private apartmentService: ApartmentsService) { }

  ngOnInit() {
    this.allApartments = this.apartmentService.getApartments();
  }

}
