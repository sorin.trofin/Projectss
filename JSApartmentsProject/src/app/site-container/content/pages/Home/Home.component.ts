import { Component, OnInit } from '@angular/core';
import { ApartmentsService } from '../../../../services/apartments.service';
@Component({
	selector: 'app-Home',
	templateUrl: './Home.component.html',
	styleUrls: ['./Home.component.scss']
})
export class HomeComponent implements OnInit {
	constructor(private apartmentService: ApartmentsService) { }
	public apartments = []
	ngOnInit() {
		this.apartments = this.apartmentService.getApartments();

	}
}
