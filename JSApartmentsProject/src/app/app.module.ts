import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SiteContainerComponent } from './site-container/site-container.component';
import { HeaderComponent } from './site-container/header/header.component';
import { ContentComponent } from './site-container/content/content.component';
import { FooterComponent } from './site-container/footer/footer.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './site-container/content/pages/Home/Home.component';
import { AboutComponent } from './site-container/content/pages/about/about.component';
import { ContactComponent } from './site-container/content/pages/contact/contact.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ApartamenteComponent } from './site-container/content/pages/apartamente/apartamente.component';

@NgModule({
	declarations: [
		AppComponent,
		SiteContainerComponent,
		HeaderComponent,
		ContentComponent,
		FooterComponent,
		HomeComponent,
		AboutComponent,
		ContactComponent,
		ApartamenteComponent
	],
	imports: [BrowserModule, AppRoutingModule, MDBBootstrapModule.forRoot()],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
