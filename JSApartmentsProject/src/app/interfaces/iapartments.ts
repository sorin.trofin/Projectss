export class IApartments {
  id: number;
  name: string;
  location: string;
  reper: string;
  rooms: number;
  images: Object[];
  price: string;
  description: string;
  constructor(id: number, name: string, location: string, reper: string, rooms: number, images: Object[], price: string, description: string) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.reper = reper;
    this.rooms = rooms;
    this.images = images;
    this.price = price;
    this.description = description;
  }
}
