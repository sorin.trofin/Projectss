import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { DogsComponent } from './dogs/dogs.component';
@NgModule({
	declarations: [ AppComponent, DogsComponent ],
	imports: [
		BrowserModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatCheckboxModule,
		MatSidenavModule,
		MatButtonModule,
		MatExpansionModule,
		FormsModule
	],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
