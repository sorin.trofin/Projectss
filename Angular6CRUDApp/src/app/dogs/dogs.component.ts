import { Component, OnInit } from '@angular/core';
import { Dog } from '../dog';

import { DogsService } from '../dogs.service';
import { DogInterface } from '../doginterface';
@Component({
	selector: 'app-dogs',
	templateUrl: './dogs.component.html',
	styleUrls: [ './dogs.component.css' ]
})
export class DogsComponent implements OnInit {
	appdogs: any;
	constructor(private dogService: DogsService) {
		this.dogService.getDogs().subscribe((dogs: Dog[]) => {
			this.appdogs = dogs;
		});
	}

	delete(dog: Dog): void {
		this.appdogs = this.appdogs.filter((h) => h !== dog);
		this.dogService.deleteDog(dog).subscribe();
	}

	onSubmit(dog: DogInterface) {
		this.dogService.onSubmit(dog).subscribe((dog) => this.appdogs.push(dog));
	}

	editDog(dog: Dog): void {
		this.dogService.onEditDog(dog).subscribe((dog) => this.appdogs.push(dog));
	}
	ngOnInit() {}
}
