import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dog } from './dog';
// import { PostData } from '../post-data';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { DogInterface } from './doginterface';

@Injectable({
	providedIn: 'root'
})
export class DogsService {
	constructor(private http: HttpClient) {}

	dogsUrl = 'http://localhost:3000/dogs';

	getDogs() {
		return this.http.get<Dog[]>(this.dogsUrl);
	}

	deleteDog(dog: Dog): Observable<Dog[]> {
		const id = typeof dog === 'number' ? dog : dog.id;
		const deleteUrl = `${this.dogsUrl}/${id}`;
		return this.http.delete<Dog[]>(deleteUrl);
	}

	onSubmit(dog: DogInterface) {
		return this.http.post('http://localhost:3000/dogs', dog);
	}

	onEditDog(dog: Dog): Observable<Dog[]> {
		const id = typeof dog === 'number' ? dog : dog.id;
		const updateUrl = `${this.dogsUrl}/${id}`;
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};
		return this.http.put<Dog[]>(updateUrl, dog, httpOptions).map((response: Response) => response.json());
	}
}
