import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ]
})
export class AppComponent {
	inputText: string = 'Initial Value';
	values: string = '';
	public title = 'Mikey';
	heroes = [ 'Windstorm', 'Bombasto', 'Magneta', 'Tornado' ];
	months = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];
	isavailable = false;
	onKey(value: string) {
		this.values = value;
	}

	addHero(newHero: string) {
		if (newHero) {
			this.heroes.push(newHero);
		}
	}

	doBark(name: string) {
		console.log(`${name} has barked`);
	}
	dog1 = { name: 'Puffy', color: 'brown' };
	dog2 = { name: 'Elsa', color: 'brown' };
	changeDog1(name) {
		this.dog1.name = name;
	}
	visible: boolean = true;

	toggleIf() {
		this.visible = !this.visible;
	}

	myFavLang = [
		{ name: 'html', type: 'frontend' },
		{ name: 'css', type: 'frontend' },
		{ name: 'js', type: 'frontend' },
		{ name: 'ruby', type: 'backend' }
	];
}
