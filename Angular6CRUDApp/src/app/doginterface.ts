export interface DogInterface {
	id: number;
	fullname: string;
	rasa: string;
	adopted: string;
}
