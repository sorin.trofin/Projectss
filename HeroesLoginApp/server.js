const express = require('express');
const path =  require('path');
const app = express();
const pg = require('pg');
const dotenv = require('dotenv');
var cors = require('cors');
const uuidV4 = require('uuid/v4');
var bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const  bcrypt  =  require('bcryptjs');
// config for your database

const config = {
  //// database config before start
  user: 
  database: 
  password: 
  port: 

};

app.use(cors());
app.use(bodyParser.json());
var pool = new pg.Pool(config);

pool.connect(function(err, client, done) {
  if(err) {
    console.log('Not Connected');
  } else {
    console.log('Connected to Server');
  }
});

//////////  DOGS  
app.get('/dogs', (req, res) => {
  pool.query('SELECT * FROM dogs', function(err, result, done) {
    res.status(200).json({dogs: result.rows});
  
  });
});

app.get('/dogs/:id', (req, res) => {
  const id = parseInt(req.params.id);
  pool.query('SELECT * FROM dogs WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    // res.status(200).json(results.rows);
    res.status(200).json({dogs: results.rows});
  });
});

app.post('/dogs', (req, res) => {
  // const id = parseFloat(uuidV4(), 10);
  // console.log('id', id);

  pool.query('INSERT INTO dogs(fullname,rasa) VALUES($1, $2)',
  [req.body.fullname, req.body.rasa]).then(function () {
    res.status(200).json({status: 'success', message: 'inserted dog'});
  })
  .catch((error) => {
    console.log('error', error);
    res.status(400).json({error: error});
  });
});

app.put('/dogs/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const { fullname, rasa } = req.body;
  console.log('id', id);
  pool.query(
    'UPDATE dogs SET fullname = $1, rasa = $2 WHERE id = $3',
    [fullname, rasa, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json({status: 'success', message: `inserted dog with ${id}`});
    }
  );
});

app.patch('/dogs/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const { fullname } = req.body;

  pool.query(
    'UPDATE dogs SET fullname = $1 WHERE id = $2',
    [fullname, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json({status: 'success', message: `inserted dog with ${id}`});
    }
  );
});

app.delete('/dogs/:id', (req, res)=> {
  const id = parseInt(req.params.id);

  pool.query('DELETE FROM dogs WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json({status: 'success', message: `deleted post with ${id}`});
  });
});

/////////////////////// USERS AUTH /////////////
app.get('/users', (req, res) => {
  pool.query('SELECT * FROM users', function(err, result, done) {
    console.log(result.rows);
    res.status(200).json({users : result.rows});
  });
});

app.get('/users/:id', (req, res) => {
  const id = parseInt(req.params.id);

  pool.query('SELECT * FROM users WHERE id = $1', [id], (err, result, done) => {
    console.log(result.rows);
    res.status(200).json({users : result.rows});
  });
});

app.post('/users', function(req, res) {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const username = req.body.username;
  const password = bcrypt.hashSync(req.body.password);
  const confirmpassword = req.body.confirmpassword;
  const userType = req.body.userType;

  console.log('userType', userType);
  pool.query('INSERT INTO users(id,firstName,lastName,username,password, confirmpassword, userType) VALUES($1, $2, $3, $4, $5, $6, $7)',
  [parseInt(uuidV4()), firstName, lastName, username, password, confirmpassword, userType ]).then(function () {

    if (req.body.password !== confirmpassword) {
      res.status(401).send({message: 'Confirm Password Incorect. Please try again'});
      return;
    }

    res.status(200).json({status: 'User Created', message: 'User Created'});
  })
  .catch(function(err) {
    console.log('catch', err);
  });
});

app.post('/login', function(req, res, next) {
  const body = req.body;
  const SECRET_KEY = 'secretkey23456';
  pool.query('SELECT * FROM users WHERE username = $1', [body.username], (error, results) => {
    if (error) {
      res.status(500).send('Server error!'); 
    }

    const user = results.rows.find(user => user.username == body.username);
    const password = user ? bcrypt.compareSync(body.password, user.password) : null;
    console.log('user', user);
    if(!user) {
      res.status(401).send({message: 'Authentification Failed. User not found'});
      return;
    }

    if (!password) {
      res.status(401).send({message: 'Password Incorect. Please try again'});
      return;
    }
  
    const  expiresIn  =  24  *  60  *  60;
    const  accessToken  =  jwt.sign({ id:  req.body.id }, SECRET_KEY, {
        expiresIn:  expiresIn
    });
    console.log('user', user);
    res.status(200).send({ 
      'username':  body.username,
      'access_token':  accessToken,
      'expires_in':  expiresIn,
      'firstName': user.firstname,
      'lastName': user.lastname,
      'id': user.id,
      'userType': user.usertype
     }); 
  });
});

/////////////////////// USERS AUTH /////////////


//////////////// HEROES //////////////////

app.get('/heroes', (req, res) => {
  pool.query('SELECT * from heroes').then(function(heroesResult) {
    return heroesResult;
  }).then(function(heroesResult) {

    let queries = heroesResult.rows.map(function(item) {
      return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM allies WHERE index = $1', [item.id], (error, results) => {
          if (error) {
            throw error;
          }
          
          item.allies = []
  
          results.rows.forEach(ally => {
            if (ally.index === item.id) {
              item.allies.push(ally);
            }
          })

          resolve(item);
        })
      })
    }, this)

    Promise.all(queries).then(results => {
      res.status(200).json(results);
    })
  }).catch(err => {
    throw err;
  })
});

app.get('/heroes/:id', (req, res) => {
  const id = parseInt(req.params.id);
  pool.query('SELECT * FROM heroes WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    // res.status(200).json(results.rows);
    res.status(200).json(results.rows);
  });
});

app.put('/heroes/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const { name } = req.body;
  console.log('req', name, id);
  pool.query(
    'UPDATE heroes SET name = $1 WHERE id = $2',
    [name, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json({status: 'success', message: `updated hero with ${id}`});
    }
  );
});

app.post('/heroes', (req, res) => {
  const request = {
    name: req.body.name === '' ? null : req.body.name,
    level: req.body.level === '' ? null : req.body.level,
    experience: req.body.experience === '' ? null : req.body.experience,
    rank: req.body.rank === '' ? null : req.body.rank,
    allies: req.body.allies === '' ? null : req.body.allies
  }

  const validation = Object.values(request).every(function(value) {
    if (!value) {
      return false;
    } else {
      return true;
    }
  });

  if (!validation) {
    res.status(500).send({message: 'Form Invalid'});
    return;
  }

  pool.query('INSERT INTO heroes(name, level, experience, rank) VALUES($1, $2, $3, $4) RETURNING *',
    [req.body.name, req.body.level, req.body.experience, req.body.rank]).then(function(result) {
      let heroId;

      result.rows.forEach(item => {
        heroId = item.id;
      })

      req.body.allies.forEach(item => {
        pool.query('INSERT INTO allies(name, index) VALUES($1, $2)',
        [item.allyname, heroId]).then(function() {
        })
      })
    }).then(function(result) {
      res.status(200).json({status: 'Success', message: 'Inserted Ally'});
    }).catch((error) => { 
      console.log('error', error);
      res.status(400).json({error: error});
    });
});

app.delete('/heroes/:id', (req, res)=> {
  const id = parseInt(req.params.id);

  pool.query('DELETE FROM heroes WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }

    res.status(200).json({status: 'success', message: `deleted hero with ${id}`});
  });
});

 //#endregion
// const posts = require('./server/routes/posts');
// app.use('/posts', posts);
const port = process.env.PORT || 4600;

app.listen(port, (req, res) => {
 console.log(`RUNNING on port ${port}`);
});


// const results  = [];
// await array.reduce(async (nextPromise, item) => {
//   await nextPromise;
//   const result = await queryToDB(item);
//   results.push(result);
// }, Promise.resolve());