import { SecurityService } from './../security-service';
import { AppSnackBarService } from './../app-snackbar-service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;
    public loading = false;
    public submitted = false;
    public returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private appSnackBarService: AppSnackBarService,
        ) {}

    public ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    public onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.loginForm.get('username').value, this.loginForm.get('password').value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/heroes']);
                },
                err => {
                    // this.alertService.error(error);
                    console.log('error', err.error);
                    this.loading = false;
                    if (err.error.message) {
                        // Show info message.
                        this.appSnackBarService.show(err.error.message, 'error-snackbar');
                      } else {
                        this.appSnackBarService.show('An error occurred, please try again.', 'error');
                      }
                });
    }
}
