import { Component, ComponentFactoryResolver, ViewChild, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Car } from 'src/app/car.interface';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppSnackBarService } from '../app-snackbar-service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  public multiComponents: any = [];
  public interval: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private appSnackBarService: AppSnackBarService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
  }

  public ngOnInit() {
  }


}
