import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hero } from 'src/app/class/hero';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  public heroesUrl = 'http://localhost:4600/heroes';

  constructor(private http: HttpClient) { }

  public getHeroes() {
    return this.http.get<Hero[]>(this.heroesUrl);
  }

  public getHero(id): Observable<Hero> {
    return this.http.get<Hero>(`${this.heroesUrl}/${id}`)
  }

  public addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.heroesUrl, hero);
  }

  public deleteHero(hero: Hero): Observable<Hero> {
    return this.http.delete<Hero>(`${this.heroesUrl}/${hero.id}`);
  }

  public updateHero(hero: Hero): Observable<Hero> {
    return this.http.put<Hero>(`${this.heroesUrl}/${hero.id}`, hero);
  }
}
