export interface Dog {
  id: number;
  fullname: string;
  rasa: string;
}
