import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { ArrayType } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})

export class MessageService {
  public popupSource = new BehaviorSubject<boolean>(false);
  public dataToSend = new BehaviorSubject<any>([]);

  constructor() { }

  public popup(component) {
    this.popupSource.next(component);
 }
}
