import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/app/models/user.model';

// import { USER_TYPES } from 'app/main/security/user.types';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private user: User = new User();
  private isUserReady: Subject<boolean> = new Subject<boolean>();

  constructor(private cookieService: CookieService) {}

  public getUser(): User {
    return this.user;
  }

  public checkUserIsReady(): Subject<boolean> {
    return this.isUserReady;
  }

  public init(data?, rememberMe?): void {
    console.log('data', data);
    if (data) {
      this.cookieService.set('username', data.username, rememberMe ? 30 : 1, '/');
      this.cookieService.set('access_token', data.access_token, rememberMe ? 30 : 1, '/');
      this.cookieService.set('id', data.id, rememberMe ? 30 : 1, '/');
      this.cookieService.set('firstName', data.firstName, rememberMe ? 30 : 1, '/');
      this.cookieService.set('lastName', data.lastName, rememberMe ? 30 : 1, '/');
      this.cookieService.set('userType', data.userType, rememberMe ? 30 : 1, '/');
      // TODO: lastUsed
      if (data.companies && data.companies.length) {
        this.cookieService.set('companyKey', data.companies[0].key, rememberMe ? 30 : 1, '/');
        this.cookieService.set('companies', JSON.stringify(data.companies), rememberMe ? 30 : 1, '/');
      }
    }

    this.user.username = this.cookieService.get('username');
    this.user.access_token = this.cookieService.get('access_token');
    this.user.id = +this.cookieService.get('id');
    this.user.firstName = this.cookieService.get('firstName');
    this.user.lastName = this.cookieService.get('lastName');
    this.user.userType = this.cookieService.get('userType');
  }

  public reset(): void {
    this.cookieService.delete('username', '/');
    this.cookieService.delete('access_token', '/');
    this.cookieService.delete('id', '/');
    this.cookieService.delete('firstName', '/');
    this.cookieService.delete('lastName', '/');
    this.cookieService.delete('userType', '/');
  }

  public isUserLoggedIn(): Boolean {
    return !!(this.user.username && this.user.password && this.user.id && this.user.firstName);
  }


  public update(data): void {
    if (!data) {
      return;
    }

    this.user.username = data.username;
    this.user.access_token = data.access_token;
    this.user.id = +data.id;
    this.user.firstName = data.firstName;
    this.user.lastName = data.lastName;
    this.user.userType = data.userType;

    // if (data.userType) {
    //   this.user.userType = data.userType;

    //   // Concat 'editablePermissions' and 'readOnlyPermissions' into a single array 'permissions'.
    //   const firstArray = this.user.userType.editablePermissions || [];
    //   const secondArray = this.user.userType.readOnlyPermissions || [];
    //   const tempArray = firstArray.concat(secondArray);
      
    //   this.user.userType.permissions = tempArray;
    //   this.user.userType.permissionsMap = tempArray.reduce((obj, item) => {
    //     obj[item.code] = item.value;

    //     return obj;
    //   }, {});
    // }

    this.isUserReady.next(true);
  }
}
