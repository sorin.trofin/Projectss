import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ControlContainer, FormGroupDirective, FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-emitinputs',
  templateUrl: './emitinputs.component.html',
  styleUrls: ['./emitinputs.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: FormGroupDirective }]
})
export class EmitinputsComponent implements OnInit {
  @Output() heroFormEmit = new EventEmitter<{}>();

  public heroesForm;
  public heroData: {} = {};

  constructor(private parentHeroesForm: FormGroupDirective, private fb: FormBuilder) { 
    this.heroData = {
      hero: {level: '', experience: '', rank: ''}
    }
  }

  ngOnInit() {
    this.heroesForm = this.parentHeroesForm.form;
    this.heroesForm.addControl('level', this.fb.control('', Validators.required));
    this.heroesForm.addControl('experience', this.fb.control('', Validators.required));
    this.heroesForm.addControl('rank', this.fb.control('', Validators.required));
    this.heroesForm.addControl('allies', this.fb.array([
      this.fb.group({
        allyname: ['']
      })
    ]))
  }

  public alliesToAdd(): FormGroup {
    return this.fb.group({
      allyname: ['']
    });
  }

  get allyArr() {
    return this.heroesForm.get('allies') as FormArray;
  }

  public addAlly(): void {
    const allies = this.heroesForm.get('allies') as FormArray;
    
    allies.push(this.alliesToAdd());
  }

  public removeAt(index): void {
    const allies = this.heroesForm.get('allies') as FormArray;

    allies.removeAt(index);
  }
}
