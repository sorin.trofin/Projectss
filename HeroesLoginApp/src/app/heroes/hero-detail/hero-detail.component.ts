import { Component, OnInit, Input } from '@angular/core';
import { Hero } from 'src/app/class/hero';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from 'src/app/services/heroes.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;

  constructor(private route: ActivatedRoute,private heroesService: HeroesService, private location: Location) { 
    
  }

  public ngOnInit() {
    this.getHero();
  }

  public getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log('thisroute', this.route, id);
    this.heroesService.getHero(id).subscribe(hero => {
      this.hero = hero[0];
    })
  }

  public goBack(): void {
    this.location.back();
  }

  public save(): void {
    this.heroesService.updateHero(this.hero)
      .subscribe(() => this.goBack());
  }

}
