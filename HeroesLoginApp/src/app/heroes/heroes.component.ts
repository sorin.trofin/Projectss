import { Component, OnInit, ViewChild } from '@angular/core';
import { Hero } from '../class/hero';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HeroesService } from '../services/heroes.service';
import { AppSnackBarService } from '../app-snackbar-service';
import { SecurityService } from '../security-service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  @ViewChild('heroAttributes') public heroAttributes;

  public heroes: Hero[];
  public heroesForm: FormGroup;
  public selectedHero: Hero;
  public user: User;

  constructor(
    private formBuilder: FormBuilder,
    private heroesService: HeroesService,
    private appSnackBarService: AppSnackBarService,
    private securityService: SecurityService) { 
    }

  public ngOnInit() {
    this.heroesForm = this.formBuilder.group({
      name: ['', Validators.required],
    });

    this.getHeroes();

    console.log('this.user', this.securityService.getUser());
    this.user = this.securityService.getUser();
  }

  public getHeroes(): void {
    this.heroesService.getHeroes().subscribe(response => {
      if (response) {
        console.log('response', response);
        this.heroes = response;
      }
    });
  }

  // public heroesChangeHandler(event): void {
  //   this.heroesForm.value.level = event.hero.level;
  //   this.heroesForm.value.experience = event.hero.experience;
  //   this.heroesForm.value.rank = event.hero.rank;
  // }
  
  public addHero(formData): void {
    const heroData = Object.assign({}, this.heroesForm.value); 

    this.heroesService.addHero(heroData).subscribe((response) => {
      if (response) {
        this.getHeroes();
        this.heroesForm.reset();
        this.heroesForm.clearValidators()
        for( let i in this.heroesForm.controls ) {
          this.heroesForm.controls[i].setErrors(null);
        }
        this.heroAttributes.heroData.hero = {};
      }
    }, err => {
      console.log('error', err.error);
      if (err.error.message) {
          // Show info message.
          this.appSnackBarService.show(err.error.message, 'error');
        } else {
          this.appSnackBarService.show('An error occurred, please try again.', 'error');
        }
     });
  }

  public onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  public deleteHero(hero: Hero): void {
    this.heroesService.deleteHero(hero).subscribe(response => {
      if (response) {
        this.getHeroes();
      }
    });
  }
}
