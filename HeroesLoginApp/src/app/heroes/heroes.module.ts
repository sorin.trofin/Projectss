import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeroesRoutingModule } from './heroes-routing.module';
import { HeroesComponent } from './heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared-module/shared.module';
import { EmitinputsComponent } from '../shared-components/emitinputs/emitinputs.component';

@NgModule({
  imports: [
    CommonModule,
    HeroesRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    HeroesComponent,
    HeroDetailComponent,
    DashboardComponent,
    EmitinputsComponent
  ],
  exports: [
    HeroesComponent,
    HeroDetailComponent,
    DashboardComponent,
    EmitinputsComponent
  ]
})
export class HeroesModule { }
