import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/heroes.service';
import { Hero } from 'src/app/class/hero';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public heroes: Hero[] = [];

  constructor(private heroesService: HeroesService) { }

  public ngOnInit() {
    this.getHeroes();
  }

  public getHeroes(): void {
    this.heroesService.getHeroes().subscribe(response => {
      this.heroes = response.slice(0, 5);
    })
  }

}
