import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';
import { AppSnackBarService } from '../app-snackbar-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    public registerForm: FormGroup;
    public loading = false;
    public submitted = false;
    public usersType: {}[] = [];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private appSnackBarService: AppSnackBarService
        ) {
          this.usersType = [
            { name: 'Admin', value: 'ADMIN'},
            { name: 'Manager', value: 'MANAGER'},
            { name: 'User', value: 'USER'}
          ]
         }

    public ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmpassword: ['', [Validators.required, Validators.minLength(6)]],
            userType: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    public onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        console.log('registerForm', this.registerForm);
        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    if (data) {
                        // Show info message.
                        console.log('data', data);
                        this.appSnackBarService.show(data['status'], 'error-snackbar');
                      } else {
                        this.appSnackBarService.show('An error occurred, please try again.', 'error');
                      }
                    this.router.navigate(['/login']);
                },
                err => {
                    if (err.error.message) {
                        // Show info message.
                        this.appSnackBarService.show(err.error.message, 'error-snackbar');
                      } else {
                        this.appSnackBarService.show('An error occurred, please try again.', 'error');
                      }
                    this.loading = false;
                });

    }
}

