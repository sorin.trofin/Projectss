import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule  } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { MessageService } from './message.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared-module/shared.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { JwtModule } from '@auth0/angular-jwt';
import { UserService } from './user.service';
import { AuthenticationService } from './authentication.service';
import { AppSnackBarService } from './app-snackbar-service';
import { SecurityService } from './security-service';
import { CookieService } from 'ngx-cookie-service';
import { CarComponent } from './car/car.component';
import { DynamicComponentDirective } from './dynamic-component.directive';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroesService } from './services/heroes.service';
import { HeroDetailComponent } from './heroes/hero-detail/hero-detail.component';
import { DashboardComponent } from './heroes/dashboard/dashboard.component';
import { HeroesModule } from './heroes/heroes.module';
// ...

export function initializeApp(usersApi: UserService, securityService: SecurityService): any {
  securityService.init();
  console.log('INIT');
  
  if (securityService.isUserLoggedIn()) {
    return () => usersApi.loadProfilePromise(securityService.getUser().id).then(response => {
      securityService.update(response);
    });
  }

  return () => true;
}

export function tokenGetter() {
  return localStorage.getItem('access_token');
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    CarComponent,
    DynamicComponentDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4000'],
        blacklistedRoutes: ['localhost:4000/api/auth']
      }
    }),
    HeroesModule
  ],
  providers: [
    MessageService,
    AuthGuard,
    UserService,
    AuthenticationService,
    AppSnackBarService,
    SecurityService,
    CookieService,
    HeroesService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [UserService, SecurityService],
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  exports: [
    CarComponent,
    DynamicComponentDirective,
    SharedModule,
    HeroesModule,
  ],
})
export class AppModule { }
