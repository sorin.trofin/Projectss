(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = ['$rootScope','DogEvents'];

  function run($rootScope, DogEvents) {
    /*------------------------------------------------------------------
    Properties
    ------------------------------------------------------------------*/
    var events = [
      DogEvents
    ];
    /*------------------------------------------------------------------
    Methods
    ------------------------------------------------------------------*/
    function wireEvents() {
      for (var i = 0; i < events.length; i++) {
        events[i].listenAll();
      }
    }

    /*------------------------------------------------------------------
    Init
    ------------------------------------------------------------------*/
    wireEvents();
  }
})();

'use strict';
var app = angular.module('app');

app.directive('autocomplete', function(dogService) {
    return {
        restrict: 'A',
        require : 'ngModel',
        scope: {
          details: '='
        },
        controller: function($scope, $element, $resource) {
    
        },
        link : function (scope, element, attrs, ngModelCtrl) {
          var availableTags = [];
          element.bind('keydown', function (event) {
            console.log('event', event);
            if(event) {
              dogService.getDogs().get().$promise.then(function(response) {
                if (response) {
                  scope.apiRequest = response;
                  availableTags = scope.apiRequest;
                }
              });
            }
          });
          scope.$watch(function() {
            return scope.apiRequest;
          },function() {
              var newElements = [];
              availableTags.forEach(item => {
                newElements.push(item.fullname); 
                console.log('waaaa');
              });
              element.autocomplete({
                source: newElements,
                select:function (event,ui) {
                  console.log(ui);
                    ngModelCtrl.$setViewValue(ui.item);
                    scope.$apply();
                }
              });
          });
        }
    };
});



(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('CheckboxCtrl', CheckboxCtrl);

	CheckboxCtrl.inject = [ '$location', '$routeParams', '$rootScope', 'dogService' ];

	function CheckboxCtrl($location, $routeParams, dogService) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
		vm.apiRequest = [];

		vm.dogService.getDogs().get().$promise.then(function(response) {
			vm.apiRequest = response;
		});
	}
})();

(function() {
  'use strict';

  angular
    .module('app')
    .factory('DogEntity', DogEntity);

  DogEntity.$inject = ['EventManager'];

  function DogEntity(EventManager) {
    return {
      isDog: false,
    };
  }
})();

(function () {
  'use strict';

  angular
    .module('app')
    .factory('DogEvents', DogEvents);

  // MyMaacoAppointmentEvents.$inject = ['EventManager', 'MyMaacoAppointmentEntity', 'SendYodleAppointmentCommand', 'GenerateCenterOpeningHoursCommand', 'GenerateCenterDatesCommand', 'GenerateAppointmentDatesCommand'];

  function DogEvents(EventManager,DogEntity) {
    return {
      listenAll: function() {
        EventManager
          .subscribe('createddog.success', function() {
          
            DogEntity.isDog = true;
            console.log('BINE WA', DogEntity.isDog);
          });
      }
    };
  }
})();

(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('DogCtrl', DogCtrl);

	DogCtrl.inject = [ '$location', 'EventManager', '$routeParams', '$rootScope', 'dogService', '$compile', '$scope'];

	function DogCtrl($location, EventManager, $routeParams, $rootScope, dogService, $compile, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.$rootScope = $rootScope;
		vm.model = {};
		vm.apiRequest = [];
		vm.fullname = null;
		vm.$rootScope.otherdog = vm.name;
		vm.fromMain = vm.$rootScope.fromMain;

		///// init
		vm.dogService.getDogs().get().$promise.then(function(response) {
			if (response) {
				console.log('response', response);
				vm.apiRequest = response;
			}
		});
		////

		vm.createDogs = function() {
			vm.dogService
				.CreateDog()
				.save({ fullname: vm.fullname, rasa: vm.rasa, adopted: vm.adopted })
				.$promise.then(function(response) {
					vm.dogService.getDogs().get().$promise.then(function(response) {
						vm.apiRequest = response;
					});
				});
		};

		vm.DeleteDogs = function(dog) {
			vm.dogService.DeleteDog().delete({ id: dog.id }).$promise.then(function(response) {
				vm.dogService.getDogs().get().$promise.then(function(response) {
					vm.apiRequest = response;
				});
			});
		};

		vm.UpdateDogs = function(id, fullname, rasa, adopted) {
			vm.dogService
				.UpdateDog()
				.update({
					id: id,
					fullname: fullname,
					rasa: rasa,
					adopted: adopted
				})
				.$promise.then(function(response) {
					vm.dogService.getDogs().get().$promise.then(function(response) {
						vm.apiRequest = response;
					});
				});
		};
	}
})();

(function () {
  'use strict';

  angular
    .module('app')
    .factory('EventManager', EventManager);

  function EventManager() {
    var _channel = angular.element({});

    return {
      // Binds to a specific 'event'
      subscribe: function(event, callback) {
        _channel.on(event, function(e, data) {
          if (angular.isFunction(callback)) {
            callback(e, data);
          }

          _channel.off(event, callback);
        });

        return this;
      },

      unsubscribe: function() {
        _channel.off(event);

        return this;
      },

      // Triggers a specific 'event'
      publish: function(event, data) {
        var parts = event.split(' ');

        for (var i = 0; i < parts.length; i++) {
          _channel.trigger(parts[i], data);
        }

        return this;
      }
    };
  }
})();

(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('homeController2', homeController2);

	homeController2.inject = [ '$location', '$routeParams', '$rootScope', 'dogService', '$rootScope', '$scope)' ];

	function homeController2($location, $routeParams, dogService, $rootScope, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
    vm.apiRequest = [];
    vm.fromParent = [];
    vm.test = [
      {name: 'sorin'},
      {name: 'wtf'},
      {name: 'wtf2'}
    ];

    $scope.$on('greeting', function(event,data) {
      console.log('received Data', data);
      vm.fromParent = data;
    });

    vm.sendDataToParent = function() {
      $scope.$emit('childrenData', vm.test);
    };
	}
})();

(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('homeController', homeController);

	homeController.inject = [ '$location', '$routeParams', '$rootScope', 'dogService', '$rootScope', '$scope)' ];

	function homeController($location, $routeParams, dogService, $rootScope, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
    vm.apiRequest = [];
    vm.test = [
      {name: 'sorin222'},
      {name: 'wtf222'},
      {name: 'wtf2222222'}
    ];

    vm.fromChild = [];

    $scope.$on('childrenData', function(event, data) {
      vm.fromChild = data;
      console.log('fromChild', vm.fromChild);
    });

    vm.sayHi = function() {
      $scope.$broadcast('greeting' , vm.test);
    };

	}
})();

'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanAppApp
 */
var app = angular.module('app');
app.controller('MainCtrl', function($rootScope, EventManager) {
	var vm = this;
	vm.awesomeThings = [ 'HTML5 Boilerplate', 'AngularJS', 'Karma' ];
	vm.dogs = $rootScope.otherdog;
	$rootScope.fromMain = 'fromMain';
	vm.test = 'test';
});

(function() {
	'use strict';

	angular.module('app').factory('dogService', dogService);

	/** @ngInject */

	function dogService($resource) {
		var defaultParams = {
			id: '@id'
		};

		return {
			getDogs: function(headers) {
				return $resource('http://localhost:3000/dogs', defaultParams, {
					get: {
						method: 'GET',
						params: {},
						isArray: true
					}
				});
			},
			CreateDog: function(headers) {
				return $resource('http://localhost:3000/dogs', defaultParams, {
					save: {
						method: 'POST',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			},
			DeleteDog: function(headers) {
				return $resource('http://localhost:3000/dogs/:id', defaultParams, {
					delete: {
						method: 'DELETE',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			},
			UpdateDog: function(headers) {
				return $resource('http://localhost:3000/dogs/:id',  defaultParams, {
					update: {
						method: 'PUT',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			}
		};
	}
})();

(function() {
	'use strict';
	angular.module('app').factory('dogService2', function($resource) {
		return $resource('http://localhost:3000/dogs'); // Note the full endpoint address
	});
})();
