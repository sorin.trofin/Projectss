(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('homeController', homeController);

	homeController.inject = [ '$location', '$routeParams', '$rootScope', 'dogService', '$rootScope', '$scope)' ];

	function homeController($location, $routeParams, dogService, $rootScope, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
    vm.apiRequest = [];
    vm.test = [
      {name: 'sorin222'},
      {name: 'wtf222'},
      {name: 'wtf2222222'}
    ];

    vm.fromChild = [];

    $scope.$on('childrenData', function(event, data) {
      vm.fromChild = data;
      console.log('fromChild', vm.fromChild);
    });

    vm.sayHi = function() {
      $scope.$broadcast('greeting' , vm.test);
    };

	}
})();
