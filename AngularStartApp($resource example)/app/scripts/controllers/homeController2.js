(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('homeController2', homeController2);

	homeController2.inject = [ '$location', '$routeParams', '$rootScope', 'dogService', '$rootScope', '$scope)' ];

	function homeController2($location, $routeParams, dogService, $rootScope, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
    vm.apiRequest = [];
    vm.fromParent = [];
    vm.test = [
      {name: 'sorin'},
      {name: 'wtf'},
      {name: 'wtf2'}
    ];

    $scope.$on('greeting', function(event,data) {
      console.log('received Data', data);
      vm.fromParent = data;
    });

    vm.sendDataToParent = function() {
      $scope.$emit('childrenData', vm.test);
    };
	}
})();
