(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('CheckboxCtrl', CheckboxCtrl);

	CheckboxCtrl.inject = [ '$location', '$routeParams', '$rootScope', 'dogService' ];

	function CheckboxCtrl($location, $routeParams, dogService) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.model = {};
		vm.apiRequest = [];

		vm.dogService.getDogs().get().$promise.then(function(response) {
			vm.apiRequest = response;
		});
	}
})();
