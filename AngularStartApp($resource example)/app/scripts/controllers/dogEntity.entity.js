(function() {
  'use strict';

  angular
    .module('app')
    .factory('DogEntity', DogEntity);

  DogEntity.$inject = ['EventManager'];

  function DogEntity(EventManager) {
    return {
      isDog: false,
    };
  }
})();
