'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanAppApp
 */
var app = angular.module('app');
app.controller('MainCtrl', function($rootScope, EventManager) {
	var vm = this;
	vm.awesomeThings = [ 'HTML5 Boilerplate', 'AngularJS', 'Karma' ];
	vm.dogs = $rootScope.otherdog;
	$rootScope.fromMain = 'fromMain';
	vm.test = 'test';
});
