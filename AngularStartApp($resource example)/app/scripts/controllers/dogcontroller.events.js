(function () {
  'use strict';

  angular
    .module('app')
    .factory('DogEvents', DogEvents);

  // MyMaacoAppointmentEvents.$inject = ['EventManager', 'MyMaacoAppointmentEntity', 'SendYodleAppointmentCommand', 'GenerateCenterOpeningHoursCommand', 'GenerateCenterDatesCommand', 'GenerateAppointmentDatesCommand'];

  function DogEvents(EventManager,DogEntity) {
    return {
      listenAll: function() {
        EventManager
          .subscribe('createddog.success', function() {
          
            DogEntity.isDog = true;
            console.log('BINE WA', DogEntity.isDog);
          });
      }
    };
  }
})();
