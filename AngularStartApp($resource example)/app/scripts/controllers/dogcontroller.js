(function() {
	'use strict';
	var app = angular.module('app');
	app.controller('DogCtrl', DogCtrl);

	DogCtrl.inject = [ '$location', 'EventManager', '$routeParams', '$rootScope', 'dogService', '$compile', '$scope'];

	function DogCtrl($location, EventManager, $routeParams, $rootScope, dogService, $compile, $scope) {
		var vm = this;
		vm.$location = $location;
		vm.$routeParams = $routeParams;
		vm.dogService = dogService;
		vm.$rootScope = $rootScope;
		vm.model = {};
		vm.apiRequest = [];
		vm.fullname = null;
		vm.$rootScope.otherdog = vm.name;
		vm.fromMain = vm.$rootScope.fromMain;

		///// init
		vm.dogService.getDogs().get().$promise.then(function(response) {
			if (response) {
				console.log('response', response);
				vm.apiRequest = response;
			}
		});
		////

		vm.createDogs = function() {
			vm.dogService
				.CreateDog()
				.save({ fullname: vm.fullname, rasa: vm.rasa, adopted: vm.adopted })
				.$promise.then(function(response) {
					vm.dogService.getDogs().get().$promise.then(function(response) {
						vm.apiRequest = response;
					});
				});
		};

		vm.DeleteDogs = function(dog) {
			vm.dogService.DeleteDog().delete({ id: dog.id }).$promise.then(function(response) {
				vm.dogService.getDogs().get().$promise.then(function(response) {
					vm.apiRequest = response;
				});
			});
		};

		vm.UpdateDogs = function(id, fullname, rasa, adopted) {
			vm.dogService
				.UpdateDog()
				.update({
					id: id,
					fullname: fullname,
					rasa: rasa,
					adopted: adopted
				})
				.$promise.then(function(response) {
					vm.dogService.getDogs().get().$promise.then(function(response) {
						vm.apiRequest = response;
					});
				});
		};
	}
})();
