'use strict';
var app = angular.module('app');

app.directive('autocomplete', function(dogService) {
    return {
        restrict: 'A',
        require : 'ngModel',
        scope: {
          details: '='
        },
        controller: function($scope, $element, $resource) {
    
        },
        link : function (scope, element, attrs, ngModelCtrl) {
          var availableTags = [];
          element.bind('keydown', function (event) {
            console.log('event', event);
            if(event) {
              dogService.getDogs().get().$promise.then(function(response) {
                if (response) {
                  scope.apiRequest = response;
                  availableTags = scope.apiRequest;
                }
              });
            }
          });
          scope.$watch(function() {
            return scope.apiRequest;
          },function() {
              var newElements = [];
              availableTags.forEach(item => {
                newElements.push(item.fullname); 
                console.log('waaaa');
              });
              element.autocomplete({
                source: newElements,
                select:function (event,ui) {
                  console.log(ui);
                    ngModelCtrl.$setViewValue(ui.item);
                    scope.$apply();
                }
              });
          });
        }
    };
});


