'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # yeomanAppApp
 *
 * Main module of the application.
 */
var app = angular.module('app', [
	'ngAnimate',
	'ngCookies',
	'ngResource',
	'ngRoute',
	'ngSanitize',
	'ngTouch',
	'ngResource',
]);

app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/main.html',
			controller: 'MainCtrl',
			controllerAs: 'main'
		})
		.when('/about', {
			templateUrl: 'views/about.html',
			controller: 'MainCtrl',
			controllerAs: 'main'
		})
		.when('/contact', {
			templateUrl: 'views/contact.html',
			controller: 'MainCtrl',
			controllerAs: 'main'
		})
		.when('/menu', {
			templateUrl: 'views/menu.html',
			controller: 'MainCtrl',
			controllerAs: 'main'
		})
		.otherwise({
			redirectTo: '/'
		});
});
