(function() {
	'use strict';

	angular.module('app').factory('dogService', dogService);

	/** @ngInject */

	function dogService($resource) {
		var defaultParams = {
			id: '@id'
		};

		return {
			getDogs: function(headers) {
				return $resource('http://localhost:3000/dogs', defaultParams, {
					get: {
						method: 'GET',
						params: {},
						isArray: true
					}
				});
			},
			CreateDog: function(headers) {
				return $resource('http://localhost:3000/dogs', defaultParams, {
					save: {
						method: 'POST',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			},
			DeleteDog: function(headers) {
				return $resource('http://localhost:3000/dogs/:id', defaultParams, {
					delete: {
						method: 'DELETE',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			},
			UpdateDog: function(headers) {
				return $resource('http://localhost:3000/dogs/:id',  defaultParams, {
					update: {
						method: 'PUT',
						params: {},
						headers: { 'Content-Type': 'application/json; charset=UTF-8' }
					}
				});
			}
		};
	}
})();
