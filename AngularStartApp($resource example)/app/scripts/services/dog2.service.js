(function() {
	'use strict';
	angular.module('app').factory('dogService2', function($resource) {
		return $resource('http://localhost:3000/dogs'); // Note the full endpoint address
	});
})();
