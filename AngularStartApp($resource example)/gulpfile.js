// Include Gulp
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')({
	pattern: [ 'gulp-*', 'gulp.*', 'main-bower-files' ],
	replaceString: /\bgulp[\-.]/
});

// Define default destination folder
var dest = 'dist/';

var errorHandler = function(title) {
	return function(err) {
		console.log(plugins.color('[' + title + ']' + err.toString(), 'RED'));
		this.emit('end');
	};
};

/**
 * Default Tasks
 */
gulp.task('default', [ 'compile' ]);
gulp.task('clean', function() {
	return gulp.src(dest + '*', { read: false }).pipe(plugins.clean());
});

/**
 * Watch Tasks
 */

gulp.task('js', function() {
	return gulp
		.src('js/**/*.js')
		.on('error', errorHandler('js-src'))
		.pipe(plugins.filter([ '**/*.js' ], { restore: true }))
		.on('error', errorHandler('js-filter'))
		.pipe(
			plugins.order(
				[
					'js/app/**/*.js',
					'js/custom-js/youtube-api.js',
					'js/custom-js/screen-handler.js',
					'js/custom-js/*.js'
				],
				{ base: './' }
			)
		)
		.on('error', errorHandler('js-order'))
		.pipe(plugins.sourcemaps.init())
		.on('error', errorHandler('js-sourcemaps-init'))
		.pipe(plugins.concat('main.min.js'))
		.on('error', errorHandler('js-concat'))
		.pipe(plugins.ngAnnotate({ add: true }))
		.on('error', errorHandler('js-ngAnnotate'))
		.pipe(plugins.uglify())
		.on('error', errorHandler('js-uglify'))
		.pipe(plugins.sourcemaps.write('maps'))
		.on('error', errorHandler('js-sourcemaps-write'))
		.pipe(gulp.dest(dest));
});

gulp.task('css', [ 'sass' ], function() {
	var cssFiles = [ dest + 'css/front-main.css' ];

	return (gulp
			.src(plugins.mainBowerFiles().concat(cssFiles))
			.on('error', errorHandler('css-src'))
			.pipe(plugins.filter([ '**/*.css', '!**/font-awesome.css' ]))
			.on('error', errorHandler('css-filter'))
			.pipe(plugins.sourcemaps.init())
			.on('error', errorHandler('css-sourcemaps-init'))
			.pipe(plugins.concat('main.min.css'))
			.on('error', errorHandler('css-concat'))
			// .pipe(plugins.minifyCss()).on('error', errorHandler('css-minifyCss'))
			.pipe(plugins.sourcemaps.write('maps'))
			.on('error', errorHandler('css-sourcemaps-write'))
			.pipe(gulp.dest(dest)) );
});

gulp.task('sass', function() {
	var sassOptions = {
		outputStyle: 'compressed'
	};

	return gulp
		.src('sass/**/*.scss')
		.on('error', errorHandler('sass-src'))
		.pipe(plugins.sourcemaps.init())
		.on('error', errorHandler('sass-sourcemaps-init'))
		.pipe(plugins.sass(sassOptions))
		.on('error', errorHandler('sass-sass'))
		.pipe(plugins.autoprefixer())
		.on('error', errorHandler('sass-autoprefixer'))
		.pipe(plugins.cleanCss({ compatibility: 'ie10' }))
		.on('error', errorHandler('sass-cleanCss'))
		.pipe(plugins.sourcemaps.write('maps'))
		.on('error', errorHandler('sass-sourcemaps-write'))
		.pipe(gulp.dest(dest + 'css'));
});

gulp.task('watch', [ 'js-vendor-build', 'js-build', 'css-build' ], function() {
	gulp.watch('sass/**/*.scss', [ 'css' ]);
	gulp.watch('js/**/*.js', [ 'js' ]);
});

/**
 * Build Tasks
 */
gulp.task('js-vendor-build', [ 'clean' ], function() {
	return gulp
		.src(plugins.mainBowerFiles())
		.on('error', errorHandler('js-vendor-src'))
		.pipe(plugins.filter('**/*.js', { restore: true }))
		.on('error', errorHandler('js-vendor-filter'))
		.pipe(plugins.concat('vendor.min.js'))
		.on('error', errorHandler('js-vendor-concat'))
		.pipe(plugins.ngAnnotate({ add: true }))
		.on('error', errorHandler('js-vendor-ngAnnotate'))
		.pipe(plugins.uglify())
		.on('error', errorHandler('js-vendor-uglify'))
		.pipe(gulp.dest(dest));
});

gulp.task('js-build', [ 'clean' ], function() {
	return gulp
		.src('js/**/*.js')
		.on('error', errorHandler('js-src'))
		.pipe(plugins.filter([ '**/*.js', '!js/custom-js/pre-launch-checklist.js' ], { restore: true }))
		.on('error', errorHandler('js-filter'))
		.pipe(
			plugins.order(
				[
					'js/app/**/*.js',
					'js/custom-js/screen-handler.js',
					'js/custom-js/youtube-api.js',
					'js/custom-js/*.js'
				],
				{ base: './' }
			)
		)
		.on('error', errorHandler('js-order'))
		.pipe(plugins.concat('main.min.js'))
		.on('error', errorHandler('js-concat'))
		.pipe(plugins.ngAnnotate({ add: true }))
		.on('error', errorHandler('js-ngAnnotate'))
		.pipe(plugins.uglify())
		.on('error', errorHandler('js-uglify'))
		.pipe(gulp.dest(dest));
});

gulp.task('css-build', [ 'sass-build' ], function() {
	var cssFiles = [ dest + 'css/front-main.css' ];

	return (gulp
			.src(plugins.mainBowerFiles().concat(cssFiles))
			.on('error', errorHandler('css-src'))
			.pipe(plugins.filter([ '**/*.css', '!**/font-awesome.css' ]))
			.on('error', errorHandler('css-filter'))
			.pipe(plugins.concat('main.min.css'))
			.on('error', errorHandler('css-concat'))
			// .pipe(plugins.minifyCss()).on('error', errorHandler('css-minifyCss'))
			.pipe(gulp.dest(dest)) );
});

gulp.task('sass-build', [ 'clean' ], function() {
	var sassOptions = {
		outputStyle: 'compressed'
	};

	return gulp
		.src('sass/**/*.scss')
		.on('error', errorHandler('sass-src'))
		.pipe(plugins.sass(sassOptions))
		.on('error', errorHandler('sass-sass'))
		.pipe(plugins.autoprefixer())
		.on('error', errorHandler('sass-autoprefixer'))
		.pipe(plugins.cleanCss({ compatibility: 'ie10' }))
		.on('error', errorHandler('sass-cleanCss'))
		.pipe(gulp.dest(dest + 'css'));
});

gulp.task('build', [ 'js-vendor-build', 'js-build', 'css-build' ], function() {
	console.log(plugins.color('Build Complete', 'GREEN'));
});
