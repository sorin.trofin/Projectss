import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Home from './components/Home';
import './App.css';
import GradiList from './containers/GradiList';
import AddGroup from './containers/AddGroup';

const linkStyle = {
	textDecoration: 'none',
	color: '#fff',
	display: 'block',
	transition: '.3s background-color'
};

class App extends Component {
	render() {
		return (
			<Router>
				<div className="container">
					<ul className="mainmenu">
						<li>
							<Link style={linkStyle} to="/">
								Home
							</Link>
						</li>
						<li>
							<Link style={linkStyle} to="/view">
								Kindergarden
							</Link>
						</li>
						<li>
							<Link style={linkStyle} to="/addgroup">
								Add Group
							</Link>
						</li>
					</ul>

					<Switch>
						<Route exact path="/" component={Home} />
						<Route path="/view" component={GradiList} />
						<Route path="/addgroup" component={AddGroup} onAddGroup={this.props.addGroup} />
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
