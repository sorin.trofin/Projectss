import uuidv1 from 'uuid/v1';

export const getGroups = async () => {
	const groupsResponse = await fetch('/groups', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const groups = await groupsResponse.json();
	return groups;
};

export const getChildren = async () => {
	const childrenResponse = await fetch('/children', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const children = await childrenResponse.json();
	return children;
};

export const handleAddGroup = async (name) => {
	const group = { id: uuidv1(), name };
	const response = await fetch('/groups', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(group),
		method: 'POST'
	});
	const addedGroup = await response.json();
	return addedGroup;
};

export const handleAddChild = async ({ groupID, childName, circle }) => {
	const child = {
		id: uuidv1(),
		groupId: groupID,
		fullname: childName,
		circle: circle
	};

	const response = await fetch(`/children`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(child),
		method: 'POST'
	});
	const addedChild = await response.json();
	return addedChild;
};

export const handleRemoveChild = async (childToDelete) => {
	const response = await fetch(`/children/${childToDelete.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'DELETE'
	});
	return response.json();
};

export const handleEditGroup = async (groupToEdit, newGroupName) => {
	const editedGroup = { ...groupToEdit, name: newGroupName };
	const response = await fetch(`/groups/${groupToEdit.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(editedGroup),
		method: 'PUT'
	});
	return response.json();
};

export const handleDeleteGroup = async (groupToDelete) => {
	const response = await fetch(`/groups/${groupToDelete.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'DELETE'
	});

	return response.json();
};

export const handleUpdateChild = async (childToUpdate, newName) => {
	const updatedChild = { ...childToUpdate, fullname: newName };
	const response = await fetch(`/children/${childToUpdate.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(updatedChild),
		method: 'PUT'
	});
	return response.json();
};
