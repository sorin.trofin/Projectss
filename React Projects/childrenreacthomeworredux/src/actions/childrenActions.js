import * as actionTypes from '../constants/action-types';

import { getChildren, handleAddChild, handleUpdateChild, handleRemoveChild } from '../services/api';
export const getChild = () => async (dispatch) => {
	const children = await getChildren();

	dispatch({
		type: actionTypes.GET_CHILDREN,
		payload: {
			children
		}
	});
};

export const createChild = (newChild) => async (dispatch) => {
	const child = await handleAddChild(newChild);

	dispatch({
		type: actionTypes.CREATE_CHILDREN,
		payload: {
			child
		}
	});
};

export const updateChild = (childToUpdate, newChild) => async (dispatch) => {
	const updatedChild = await handleUpdateChild(childToUpdate, newChild);
	dispatch({
		type: actionTypes.UPDATE_CHILDREN,
		payload: {
			updatedChild
		}
	});
};

export const deleteChild = (childToDelete) => async (dispatch) => {
	const child = await handleRemoveChild(childToDelete);
	dispatch({
		type: actionTypes.DELETE_CHILDREN,
		payload: {
			child
		}
	});
};
