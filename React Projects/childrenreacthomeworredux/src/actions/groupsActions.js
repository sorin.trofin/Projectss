import * as actionTypes from '../constants/action-types';
import { getGroups, handleAddGroup, handleEditGroup, handleDeleteGroup } from '../services/api';

export const getGroup = () => async (dispatch) => {
	const groups = await getGroups();

	dispatch({
		type: actionTypes.GET_GROUPS,
		payload: {
			groups
		}
	});
};

export const addGroup = (newGroup) => async (dispatch) => {
	const group = await handleAddGroup(newGroup);
	console.log(group);
	dispatch({
		type: actionTypes.ADD_GROUP,
		payload: {
			group
		}
	});
};

export const editGroup = (groupToEdit, newGroupName) => async (dispatch) => {
	const newGroup = await handleEditGroup(groupToEdit, newGroupName);

	dispatch({
		type: actionTypes.EDIT_GROUP,
		payload: {
			newGroup
		}
	});
};

export const deleteGroup = (groupToDelete) => async (dispatch) => {
	const group = await handleDeleteGroup(groupToDelete);

	dispatch({
		type: actionTypes.DELETE_GROUP,
		payload: {
			group
		}
	});
};
