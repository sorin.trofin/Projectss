import * as actionTypes from '../constants/action-types';

// action creator
export const setFilter = (filter) => {
	return {
		// action
		type: actionTypes.SET_FILTER,
		payload: {
			filter
		}
	};
};
