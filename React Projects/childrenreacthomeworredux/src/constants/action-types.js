/**
 * filter actions
 */
export const SET_FILTER = 'SET_FILTER';

/**
 * product actions
 */
export const GET_GROUPS = 'GET_GROUPS';
export const GET_CHILDREN = 'GET_CHILDREN';
export const ADD_GROUP = 'ADD_GROUP';
export const EDIT_GROUP = 'EDIT_GROUP';
export const DELETE_GROUP = 'DELETE_GROUP';
export const CREATE_CHILDREN = 'CREATE_CHILDREN';
export const UPDATE_CHILDREN = 'UPDATE_CHILDREN';
export const DELETE_CHILDREN = 'DELETE_CHILDREN';
export const IS_EDITING = 'IS_EDITING';
