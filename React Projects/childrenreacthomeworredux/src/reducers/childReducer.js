import * as actionTypes from '../constants/action-types';

const initialState = [];

const childReducer = (state = initialState, action) => {
	const { payload } = action;

	switch (action.type) {
		case actionTypes.GET_CHILDREN:
			return payload.children;
		case actionTypes.CREATE_CHILDREN:
			return [ ...state, payload.child ];

		case actionTypes.UPDATE_CHILDREN:
			const newChildren = state.map((child) => {
				if (child.id === payload.updatedChild.id) {
					return payload.updatedChild;
				}
				return child;
			});
			return newChildren;

		case actionTypes.DELETE_CHILDREN:
			const newState = Object.assign([], state);
			const indexOfChildToDelete = state.findIndex((child) => {
				return child.id === payload.child.id;
			});
			newState.splice(indexOfChildToDelete, 1);
			return newState;
		default:
			return state;
	}
};

export default childReducer;
