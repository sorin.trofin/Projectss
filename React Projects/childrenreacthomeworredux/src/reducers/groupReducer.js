import * as actionTypes from '../constants/action-types';

const initialState = [];

const groupReducer = (state = initialState, action) => {
	const { payload } = action;

	switch (action.type) {
		case actionTypes.GET_GROUPS:
			return payload.groups;
		case actionTypes.ADD_GROUP:
			return [ ...state, payload.group ];

		case actionTypes.EDIT_GROUP:
			const newGroup = state.map((group) => {
				if (group.id === payload.newGroup.id) {
					return payload.newGroup;
				}
				return group;
			});
			return newGroup;

		case actionTypes.DELETE_GROUP:
			const newState = Object.assign([], state);
			const indexOfgroupToDelete = state.findIndex((group) => {
				return group.id === payload.group.id;
			});
			newState.splice(indexOfgroupToDelete, 1);
			return newState;
		default:
			return state;
	}
};

export default groupReducer;
