import { combineReducers } from 'redux';

import filterReducer from './filterReducer';
import childReducer from './childReducer';
import groupReducer from './groupReducer';

const rootReducer = {
	filter: filterReducer,
	children: childReducer,
	groups: groupReducer
};

export default combineReducers(rootReducer);
