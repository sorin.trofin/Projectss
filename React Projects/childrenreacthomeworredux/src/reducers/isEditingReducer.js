import * as actionTypes from '../constants/action-types';

const initialState = [];

const isEditingReducer = (state = initialState, action) => {
	const { payload } = action;

	switch (action.type) {
		case actionTypes.IS_EDITING:
			return {
				...state,
				isEditing: !state.isEditing
			};
		default:
			return state;
	}
};

export default isEditingReducer;
