import React from 'react';
import { Input } from 'antd';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as groupsActions from '../actions/groupsActions';
import * as filterActions from '../actions/filterActions';

class AddGroup extends React.Component {
	render() {
		const { actions } = this.props;
		return (
			<div className="add-group">
				<h2>Add Group</h2>
				<Input
					className="newinput"
					placeholder="Type a group name"
					type="text"
					ref={(input) => (this.addGroupInput = input)}
				/>
				<Button
					className="newbutton"
					variant="contained"
					color="primary"
					type="primary"
					onClick={() => {
						actions.addGroup(this.addGroupInput.input.value);
						this.addGroupInput.input.value = '';
					}}
				>
					Add Group
				</Button>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	groups: state.groups
});

const mapDispatchToProps = (dispatch) => ({
	actions: {
		addGroup: (name) => dispatch(groupsActions.addGroup(name))
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddGroup));
