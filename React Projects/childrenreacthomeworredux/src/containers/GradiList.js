import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { GroupList } from '../components/GroupList';
import FilterSelector from '../components/FilterSelector';
import * as childrenActions from '../actions/childrenActions';
import * as filterActions from '../actions/filterActions';
import * as groupsActions from '../actions/groupsActions';

class GradiList extends Component {
	async componentDidMount() {
		const { actions } = this.props;
		actions.getChild();
		actions.getGroup();
	}

	render() {
		const { actions, filter } = this.props;
		const { groups, children } = this.props;

		return children.length > 0 ? (
			<div className="children-list">
				<FilterSelector currentFilter={filter} onSelectFilter={actions.setFilter} />
				<GroupList
					groups={groups}
					children={children}
					currentFilter={filter}
					onAddChild={actions.createChild}
					onRemoveChild={actions.deleteChild}
					onEditGroup={actions.editGroup}
					onDeleteGroup={actions.deleteGroup}
					onUpdateChild={actions.updateChild}
					onClickEdit={this.onClickEdit}
					onSaveEdit={this.onSaveEdit}
				/>
			</div>
		) : (
			<p>Something its not working , please wait</p>
		);
	}
}

const mapStateToProps = (state) => ({
	filter: state.filter,
	children: state.children,
	groups: state.groups
});

const mapDispatchToProps = (dispatch) => ({
	actions: {
		setFilter: (filter) => dispatch(filterActions.setFilter(filter)),
		getGroup: () => dispatch(groupsActions.getGroup()),
		getChild: () => dispatch(childrenActions.getChild()),
		createChild: (groupID, childName, circle) =>
			dispatch(childrenActions.createChild({ groupID, childName, circle })),
		updateChild: (childToUpdate, newName) => dispatch(childrenActions.updateChild(childToUpdate, newName)),
		deleteChild: (child) => dispatch(childrenActions.deleteChild(child)),
		addGroup: (name) => dispatch(groupsActions.addGroup(name)),
		editGroup: (groupToEdit, newGroupName) => dispatch(groupsActions.editGroup(groupToEdit, newGroupName)),
		deleteGroup: (deleteGroup) => dispatch(groupsActions.deleteGroup(deleteGroup))
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GradiList));
