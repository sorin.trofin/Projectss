import React from 'react';
import ChildList from './ChildList';
import AddChild from './AddChild';
import DeleteGroup from './DeleteGroup';

export const GroupItem = (props) => (
	<div className="group-item">
		<div>
			<h1>Group: {props.item.name}</h1>
			<div>
				<DeleteGroup
					id={props.item.id}
					onEditGroup={(newGroupName) => props.onEditGroup(newGroupName)}
					onDeleteGroup={() => props.onDeleteGroup()}
				/>

				<AddChild
					onAddChild={(childName, circle) => props.onAddChild(props.item.id, childName, circle)}
					childList={props.childList}
				/>

				{props.childList.length > 0 ? (
					<ChildList
						childList={props.childList}
						onRemoveChild={(child) => props.onRemoveChild(child)}
						currentFilter={props.currentFilter}
						isEditing={props.isEditing}
						onClickEdit={props.onClickEdit}
						onSaveEdit={props.onSaveEdit}
						onUpdateChild={(child, newName) => props.onUpdateChild(child, newName)}
					/>
				) : (
					<div>No children found</div>
				)}
			</div>
		</div>
	</div>
);
