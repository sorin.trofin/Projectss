import React from 'react';
import { GroupItem } from './GroupItem';

export const GroupList = (props) => {
	return (
		<div className="group-list">
			{props.groups.map((group) => (
				<GroupItem
					item={group}
					key={group.id}
					childList={props.children.filter((child) => child.groupId === group.id)}
					onAddChild={(groupId, childName, circle) => props.onAddChild(groupId, childName, circle)}
					onRemoveChild={(child) => props.onRemoveChild(child)}
					onEditGroup={(newGroupName) => props.onEditGroup(group, newGroupName)}
					onDeleteGroup={() => props.onDeleteGroup(group)}
					onUpdateChild={(child, newName) => props.onUpdateChild(child, newName)}
					currentFilter={props.currentFilter}
					isEditing={props.isEditing}
					onClickEdit={props.onClickEdit}
					onSaveEdit={props.onSaveEdit}
				/>
			))}
		</div>
	);
};
