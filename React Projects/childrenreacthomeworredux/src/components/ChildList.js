import React from 'react';
import ChildItem from './ChildItem';
import * as filterTypes from '../constants/filter-types';




const childFilterTypes = {
	[filterTypes.ALL_CIRCLES]: () => true,
	[filterTypes.RED_CIRCLE]: (children) => children.circle === 'red',
	[filterTypes.GREEN_CIRCLE]: (children) => children.circle === 'green',
	[filterTypes.BLACK_CIRCLE]: (children) => children.circle === 'black'
};

const ChildList = ({ childList, onRemoveChild, currentFilter, isEditing, onClickEdit, onSaveEdit, onUpdateChild }) => {
	return (
		<div className="children-list">
			{childList
				.filter(childFilterTypes[currentFilter])
				.map((child) => (
					<ChildItem
						key={child.id}
						child={child}
						onRemoveChild={(child) => onRemoveChild(child)}
						isEditing={isEditing}
						onClickEdit={onClickEdit}
						onSaveEdit={onSaveEdit}
						onUpdateChild={(child, newName) => onUpdateChild(child, newName)}
					/>
				))}
		</div>
	);
};

export default ChildList;
