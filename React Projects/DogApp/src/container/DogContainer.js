import React, { Component } from 'react';
import DogList from '../components/DogList';
import { getDogs, handleAddDog } from '../services/api';
import AddDog from '../components/AddDog';
import 'antd/dist/antd.css';
import * as dogFilterTypes from '../constants/filter-types';
import FilterSelector from '../components/FilterSelector';
export default class DogContainer extends Component {
	state = {
		dogs: [],
		currentFilter: dogFilterTypes.ALL_ADOPTED
	};

	async componentDidMount() {
		const dogs = await getDogs();
		this.setState({
			dogs
		});
	}

	onSelectFilter = (value) => {
		this.setState({
			currentFilter: value
		});
	};

	handleRemoveDog = async (dogToDelete) => {
		const response = await fetch(`/dogs/${dogToDelete.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'DELETE'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					dogs: prevState.dogs.filter((dog) => dog.id !== dogToDelete.id)
				};
			});
		}
	};

	handleUpdateDog = async (dogToUpdate, newName) => {
		const updatedDog = { ...dogToUpdate, fullname: newName };
		const response = await fetch(`/dogs/${dogToUpdate.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(updatedDog),
			method: 'PUT'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					dogs: prevState.dogs.map((dog) => {
						if (dog.id === dogToUpdate.id) {
							return dogToUpdate;
						}
						return dog;
					})
				};
			});
		}
	};

	handleUpdateAdopted = async (dogToUpdate, newAdopted) => {
		const updatedDog = { ...dogToUpdate, adopted: newAdopted };
		const response = await fetch(`/dogs/${dogToUpdate.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(updatedDog),
			method: 'PUT'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					dogs: prevState.dogs.map((dog) => {
						if (dog.id === dogToUpdate.id) {
							return updatedDog;
						}
						return dog;
					})
				};
			});
		}
	};

	handleAddDog = async (dogName, rasa, adopted) => {
		const addedDog = await handleAddDog(dogName, rasa, adopted);
		this.setState((prevState) => ({
			dogs: [ ...prevState.dogs, addedDog ]
		}));
	};

	render() {
		const { dogs, currentFilter } = this.state;
		return (
			<div>
				<FilterSelector currentFilter={currentFilter} onSelectFilter={this.onSelectFilter} />
				<AddDog handleAddDog={this.handleAddDog} />
				<DogList
					dogs={dogs}
					handleRemoveDog={this.handleRemoveDog}
					handleUpdateDog={this.handleUpdateDog}
					currentFilter={currentFilter}
					handleUpdateAdopted={this.handleUpdateAdopted}
				/>
			</div>
		);
	}
}
