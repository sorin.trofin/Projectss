import React, { Component } from 'react';
import { Input } from 'antd';
import { Button } from 'antd';
import { Collapse } from 'antd';
const Panel = Collapse.Panel;
class DogItem extends Component {
	state = {
		fullname: this.props.dog.fullname,
		rasa: this.props.dog.rasa,
		isEditing: false,
		adopted: this.props.dog.adopted,
		isToggleOn: false
	};

	onClickEdit = () => {
		this.setState({ isEditing: !this.state.isEditing });
	};

	onSaveEdit = () => {
		this.setState({
			isEditing: false
		});
		this.props.handleUpdateDog(this.props.dog, this.state.fullname);
	};

	handleNameChange = (fullname) => (event) => {
		this.setState({
			[fullname]: event.target.value
		});
	};

	handleAdopted = () => {
		this.setState(
			function(prevState) {
				return {
					adopted: prevState.adopted === 'Adopted' ? 'Not Adopted' : 'Adopted',
					isToggleOn: !prevState.isToggleOn
				};
			},
			() => this.props.handleUpdateAdopted(this.props.dog, this.state.adopted)
		);
	};

	render() {
		const { dog } = this.props;
		return (
			<div className="dog-item container">
				<Collapse className="panel">
					<Panel header={this.state.fullname} className="panel">
						{this.state.isEditing ? '' : <h3>Name:{this.state.fullname}</h3>}
						{this.state.isEditing ? '' : <h3>Rasa:{this.state.rasa}</h3>}
						{this.state.isEditing ? '' : <h4>Adopted:{this.state.adopted}</h4>}
						{this.state.isEditing ? (
							<Input
								className="dogtochangeinput"
								value={this.state.fullname}
								type="text"
								onChange={this.handleNameChange('fullname')}
							/>
						) : (
							''
						)}
						{this.state.isEditing ? '' : <Button onClick={this.onClickEdit}>Edit</Button>}
						<Button className="newbutton" onClick={this.onSaveEdit}>
							Save
						</Button>
						<Button onClick={() => this.props.handleRemoveDog(dog)}>Delete</Button>
						<br />
						{/* <select className="selectAdopted" value={this.state.adopted} onChange={this.handleAdopted}>
							<option value="Adopted">Adopted</option>
							<option value="Not Adopted">Not Adopted</option>
						</select> */}

						<Button onClick={this.handleAdopted}>
							{this.state.isToggleOn ? 'Adopt it' : 'Not Adopted'}
						</Button>
					</Panel>
				</Collapse>
			</div>
		);
	}
}

export default DogItem;
