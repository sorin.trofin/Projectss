import React from 'react';
import { Input, Button } from 'antd';
import { Select } from 'antd';
const Option = Select.Option;
export default class AddDog extends React.Component {
	// handleAdopted = (event) => {
	// 	const adoptedValue = event.target.value;
	// 	this.props.handleAddDog(this.props.dog, adoptedValue);
	// };
	state = {
		addAdopted: ''
	};

	handleChange = (value) => {
		this.setState({ addAdopted: value });
	};
	render() {
		return (
			<div className="add-Dog container">
				<Input
					placeholder="Type a Dog name"
					type={Input.Text}
					disabled={false}
					className="input"
					ref={(input) => (this.addDogInput = input)}
				/>
				<Input
					placeholder="Type rasa"
					type={Input.Text}
					className="input"
					disabled={false}
					ref={(input) => (this.addRasaInput = input)}
				/>

				<h5 style={{ marginBottom: '1px' }}>Select if dog is adopted or not</h5>

				<Select style={{ width: 150 }} className="selectAdopted" onChange={this.handleChange}>
					<Option value="Adopted">Adopted</Option>
					<Option value="Not Adopted">Not Adopted</Option>
				</Select>

				<Button
					type="primary"
					className="button"
					onClick={() => {
						this.props.handleAddDog(
							this.addDogInput.input.value,
							this.addRasaInput.input.value,
							this.state.addAdopted
						);
						this.addDogInput.input.value = '';
						this.addRasaInput.input.value = '';
					}}
				>
					Add Dog
				</Button>
			</div>
		);
	}
}
