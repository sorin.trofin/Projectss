import React, { Component } from 'react';

import './App.css';
import DogContainer from './container/DogContainer';
class App extends Component {
	render() {
		return (
			<div className="App">
				<DogContainer />
			</div>
		);
	}
}

export default App;
