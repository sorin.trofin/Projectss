import uuidv1 from 'uuid/v1';
export const getDogs = async () => {
	const dogsResponse = await fetch('/dogs', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const dogs = await dogsResponse.json();
	return dogs;
};

export const handleAddDog = async (dogName, rasa, adopted) => {
	const dogs = {
		id: uuidv1(),
		fullname: dogName,
		rasa: rasa,
		adopted: adopted
	};
	const response = await fetch(`/dogs`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(dogs),
		method: 'POST'
	});
	const addedDog = await response.json();
	return addedDog;
};
