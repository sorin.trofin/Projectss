import React from 'react';
import { Input } from 'antd';
import { Button } from '@material-ui/core';

export default class AddChild extends React.Component {
	render() {
		return (
			<div className="addchild">
				<Input
					className="newinput"
					placeholder="Type a child fullname"
					type="text"
					ref={(input) => (this.addChildInput = input)}
				/>
				<Input
					className="newinput"
					placeholder="Choose: red, green or black"
					type="text"
					ref={(input) => (this.addCircleInput = input)}
				/>
				<Button
					className="newbutton"
					variant="contained"
					color="primary"
					type="primary"
					onClick={() => {
						this.props.onAddChild(this.addChildInput.input.value, this.addCircleInput.input.value);
						this.addChildInput.input.value = '';
						this.addCircleInput.input.value = '';
					}}
				>
					Add Child
				</Button>
			</div>
		);
	}
}
