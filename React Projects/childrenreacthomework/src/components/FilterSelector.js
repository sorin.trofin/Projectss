import React from 'react';

import * as filterTypes from '../constants/filter-types';

const FilterSelector = ({ selectedFilter, onSelectFilter }) => {
	return (
		<div id="product-filter">
			<h2>Filter the Children:</h2>
			<select value={selectedFilter} onChange={(event) => onSelectFilter(event.target.value)}>
				<option value={filterTypes.ALL_CIRCLES}>All</option>
				<option value={filterTypes.RED_CIRCLE}>RED</option>
				<option value={filterTypes.GREEN_CIRCLE}>GREEN</option>
				<option value={filterTypes.BLACK_CIRCLE}>BLACK</option>
			</select>
		</div>
	);
};

export default FilterSelector;
