import React from 'react';
import { Input } from 'antd';
import { Button } from '@material-ui/core';

export default class DeleteEditGroup extends React.Component {
	render() {
		return (
			<div className="deletegroup">
				<Input
					className="newinput"
					placeholder="Type a new group name"
					type="text"
					disabled={false}
					ref={(input) => (this.editGroupInput = input)}
				/>
				<Button
					className="newbutton"
					variant="contained"
					color="primary"
					type="primary"
					onClick={() => {
						this.props.onEditGroup(this.editGroupInput.input.value);
						this.editGroupInput.input.value = '';
					}}
				>
					Edit Group
				</Button>
				<Button
					className="newbutton"
					type="danger"
					variant="contained"
					color="primary"
					onClick={() => {
						this.props.onDeleteGroup();
					}}
				>
					Delete Group
				</Button>
			</div>
		);
	}
}
