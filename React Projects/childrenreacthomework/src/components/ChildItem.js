import React from 'react';
import { ListItem, TextField, Button } from '@material-ui/core';
import { Input } from 'antd';
export default class ChildItem extends React.Component {
	state = {
		fullname: this.props.child.fullname,
		circle: this.props.child.circle,
		isEditing: this.props.isEditing
	};

	onClickEdit = () => {
		this.setState({ isEditing: !this.state.isEditing });
	};

	onSaveEdit = () => {
		this.setState({
			isEditing: false
		});
		this.props.onUpdateChild(this.props.child, this.state.fullname);
	};

	handleNameChange = (fullname) => (event) => {
		this.setState({
			[fullname]: event.target.value
		});
	};

	render() {
		const { child } = this.props;
		return (
			<ListItem className="child">
				{this.state.isEditing ? '' : <TextField label="Childname" value={this.state.fullname} />}
				{this.state.isEditing ? (
					<span>
						<Input
							class="newinput"
							label="Childname"
							value={this.state.fullname}
							type="text"
							onChange={this.handleNameChange('fullname')}
						/>
					</span>
				) : (
					''
				)}
				<span className={this.state.circle} />

				{this.state.isEditing ? (
					''
				) : (
					<Button className="newbutton" variant="contained" color="primary" onClick={this.onClickEdit}>
						Edit
					</Button>
				)}
				<Button className="newbutton" variant="contained" color="primary" onClick={this.onSaveEdit}>
					Save
				</Button>
				<Button
					className="newbutton"
					variant="contained"
					color="primary"
					onClick={() => this.props.onRemoveChild(child)}
				>
					Delete
				</Button>
			</ListItem>
		);
	}
}
