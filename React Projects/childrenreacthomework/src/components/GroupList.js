import React from 'react';
import { GroupItem } from './GroupItem';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

// const {match : { params } } = this.props;

export const GroupList = (props) => (
	<div className="group-list">
		{props.groups.map((group) => {
			return (
				<Router>
					<div>
						<Link to={`/groups/${group.id}`}>
							<GroupItem
								item={group}
								key={group.id}
								childList={props.allChildren.filter((child) => child.groupId === group.id)}
								onAddChild={(groupID, childName, circle) =>
									props.onAddChild(groupID, childName, circle)}
								onRemoveChild={(child) => props.onRemoveChild(child)}
								onEditGroup={(newGroupName) => props.onEditGroup(group, newGroupName)}
								onDeleteGroup={() => props.onDeleteGroup(group)}
								onUpdateChild={(child, newName) => props.onUpdateChild(child, newName)}
								onUpdateChecked={(child, value) => props.onUpdateChecked(child, value)}
								currentFilter={props.currentFilter}
								isEditing={props.isEditing}
								onClickEdit={props.onClickEdit}
								onSaveEdit={props.onSaveEdit}
							/>
						</Link>
						<Switch>
							{props.groups.map((groups) => <Route path="/groups/groupId" component={GroupItem} />)}
						</Switch>
					</div>
				</Router>
			);
		})}
	</div>
);
