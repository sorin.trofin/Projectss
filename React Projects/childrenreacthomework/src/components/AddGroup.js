import React from 'react';
import { Input } from 'antd';
import { Button } from '@material-ui/core';

export default class AddGroup extends React.Component {
	render() {
		return (
			<div className="add-group">
				<h2>Add Group</h2>
				<Input
					className="newinput"
					placeholder="Type a group name"
					type="text"
					ref={(input) => (this.addGroupInput = input)}
				/>
				<Button
					className="newbutton"
					variant="contained"
					color="primary"
					type="primary"
					onClick={() => {
						this.props.onAddGroup(this.addGroupInput.input.value);
						this.addGroupInput.input.value = '';
					}}
				>
					Add Group
				</Button>
			</div>
		);
	}
}
