import React, { Component } from 'react';

import './App.css';
import GradiList from './containers/GradiList';

class App extends Component {
	render() {
		return (
			<div className="App">
				<h1>Children</h1>
				<GradiList />
			</div>
		);
	}
}

export default App;
