import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';
import 'antd/dist/antd.css';
import { GroupList } from '../components/GroupList';
import FilterSelector from '../components/FilterSelector';
import AddGroup from '../components/AddGroup';
import { getGroups, getChildren } from '../services/api';
import * as filterTypes from '../constants/filter-types';

class GradiList extends Component {
	state = {
		children: [],
		groups: [],
		currentFilter: filterTypes.ALL_CIRCLES,
		isEditing: false
	};

	async componentDidMount() {
		const groups = await getGroups();
		const children = await getChildren();
		this.setState({
			groups,
			children
		});
	}

	onSelectFilter = (value) => {
		this.setState({
			currentFilter: value
		});
	};

	handleAddGroup = async (name) => {
		const group = { id: uuidv1(), name };
		const response = await fetch('/groups', {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(group),
			method: 'POST'
		});
		const addedGroup = await response.json();
		this.setState((prevState) => ({ groups: [ ...prevState.groups, addedGroup ] }));
	};

	handleAddChild = async (groupID, childName, circle) => {
		const child = {
			id: uuidv1(),
			groupId: groupID,
			fullname: childName,
			circle: circle
		};
		const response = await fetch(`/children`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(child),
			method: 'POST'
		});
		const addedChild = await response.json();
		this.setState((prevState) => ({
			children: [ ...prevState.children, addedChild ]
		}));
	};

	handleRemoveChild = async (childToDelete) => {
		const response = await fetch(`/children/${childToDelete.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'DELETE'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					children: prevState.children.filter((child) => child.id !== childToDelete.id)
				};
			});
		}
	};

	handleEditGroup = async (groupToEdit, newGroupName) => {
		const editedGroup = { ...groupToEdit, name: newGroupName };
		const response = await fetch(`/groups/${groupToEdit.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(editedGroup),
			method: 'PUT'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					groups: prevState.groups.map((group) => {
						if (group.id === editedGroup.id) {
							return editedGroup;
						}
						return group;
					})
				};
			});
		}
	};

	handleDeleteGroup = async (groupToDelete) => {
		const response = await fetch(`/groups/${groupToDelete.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'DELETE'
		});

		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					groups: prevState.groups.filter((group) => group.id !== groupToDelete.id),
					children: prevState.children.filter((child) => child.groupId !== groupToDelete.id)
				};
			});
		}
	};

	handleUpdateChild = async (childToUpdate, newName) => {
		const updatedChild = { ...childToUpdate, fullname: newName };
		const response = await fetch(`/children/${childToUpdate.id}`, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(updatedChild),
			method: 'PUT'
		});
		if (response.status === 200) {
			this.setState((prevState) => {
				return {
					children: prevState.children.map((child) => {
						if (child.id === childToUpdate.id) {
							return childToUpdate;
						}
						return child;
					})
				};
			});
		}
	};

	render() {
		const { currentFilter, children } = this.state;
		const { groups, children: allChildren } = this.state;

		return children.length > 0 ? (
			<div className="container">
				<FilterSelector currentFilter={currentFilter} onSelectFilter={this.onSelectFilter} />
				<AddGroup onAddGroup={this.handleAddGroup} />
				<GroupList
					groups={groups}
					allChildren={allChildren}
					onAddChild={this.handleAddChild}
					onRemoveChild={this.handleRemoveChild}
					onEditGroup={this.handleEditGroup}
					onDeleteGroup={this.handleDeleteGroup}
					onUpdateChild={this.handleUpdateChild}
					currentFilter={currentFilter}
					handleAddCircle={this.handleAddCircle}
					isEditing={this.state.isEditing}
					onClickEdit={this.onClickEdit}
					onSaveEdit={this.onSaveEdit}
				/>
			</div>
		) : (
			<p>Something its not working , please wait</p>
		);
	}
}

export default GradiList;
