export const getGroups = async () => {
	const groupsResponse = await fetch('/groups', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const groups = await groupsResponse.json();
	return groups;
};

export const getChildren = async () => {
	const childrenResponse = await fetch('/children', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const children = await childrenResponse.json();
	return children;
};
