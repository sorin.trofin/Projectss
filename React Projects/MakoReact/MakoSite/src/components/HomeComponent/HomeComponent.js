import React, { Component } from 'react';
import { FooterComponent } from '../FooterComponent/FooterComponent';
import { SocialComponent } from '../SocialComponent/SocialComponent';
import 'simple-line-icons/css/simple-line-icons.css';
import { Link } from 'react-router-dom';
import './style.css';
import Order from './order-online.png';
import Shrimps from './shrimps.jpg';
import Shrimps2 from './shrimps2.jpg';
import { Helmet } from 'react-helmet';

class HomeComponent extends Component {
	constructor(props) {
		super(props);
		this.state = { reload: false };
	}
	componentWillReceiveProps(nextProps) {
		const { location } = this.props;
		const { location: nextLocation } = nextProps;

		if (
			nextLocation.pathname === location.pathname &&
			nextLocation.search === location.search &&
			nextLocation.hash === location.hash &&
			nextLocation.key !== location.key
		) {
			this.setState({ reload: true }, () => this.setState({ reload: false }));
		}
		console.log(this.state.reload);
	}
	render() {
		return this.state.reload ? null : (
			<div>
				<section className="features" id="features">
					<div className="container">
						<div className="row marginTop">
							<div className="col-lg-6 col-md-6 col-sm-6 text-justify order ">
								<h1 className="text-center story ">Our Story</h1>
								<p>
									The concept of international cuisine came from our chef, whose passion would be
									wasted if we had not decided to share it with you. This is how Mako's Table
									Restaurant was born. A restaurant with which we propose two things. First of all, to
									create a pleasant and relaxed atmosphere if you want to come to for a business lunch
									and for date night or for a Friday night with friends, but also for a quiet brunch
									with family in weekend. Secondly, we want to open the way for some special dishes
									adapted by our chef to suit everyone's tastes.
								</p>
								<div className="col-12 text-center">
									<Link className="story findmoreaboutus" to="/aboutus">
										Find More About Us
									</Link>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 col-sm-6 text-justify order">
								<div className="align-center">
									<h2 className="text-center story">Make a Booking</h2>
									<script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=167445&type=standard&theme=standard&iframe=true&overlay=false&domain=couk&lang=en-GB'></script>
									<Helmet>
										<script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=167445&type=standard&theme=standard&iframe=true&overlay=false&domain=couk&lang=en-GB'></script>
									</Helmet>
								</div>
							</div>
						</div>

						<div className="row marginTop">
							<div className="col-lg-6 col-md-12 col-sm-12">
								<h1 className="text-center story top">Perfect Taste</h1>
								<p>
									We promise an intimate setting and the experience of a relaxing dinner that offers
									something different from other restaurants.
								</p>
								<p>Enjoy a memorable culinary experience every time!</p>
							</div>

							<div className="col-lg-6 col-md-12 col-sm-12 perfecttastephotos">
								<div className="row">
									<div className="photos col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
										<img className="shrimps" src={Shrimps} alt="Shrimps" />
									</div>
									<div className="photos col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<img className="shrimps" src={Shrimps2} alt="Shrimps2" />
									</div>
								</div>
							</div>
						</div>

						<div className="row marginTop">
							<div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 offers">
								<h1 className="text-center story">Phone Takeaway Offers</h1>
								<p>10% discount on collection</p>
								<p>
									<span>FREE</span> "Starter" on orders over £40
								</p>
								<p>
									<span>FREE</span> "Pancakes" on orders over £30
								</p>
								<p>10% discount for students on orders over £20</p>
								<p>
									<span>FREE</span> Home Deliveries for orders over £20, minumum order £10
								</p>

								<div>
									<p>Delivery Post codes and charges:</p>
									<p>
										SO15 <span>FREE</span> DELIVERY
									</p>
									<p>SO14,SO16,SO17 - £1.50( Free Delivery on orders over £20 )</p>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 col-sm-6 text-justify order">
								<div className="align-center">
									<h2 className="text-center story">Order Online</h2>
									<div className="marginTop">
										<a href="https://www.just-eat.co.uk/restaurants-makos-table-millbrook/menu" target="_blank"><img src={Order}></img>></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<br />
				<SocialComponent />
				<FooterComponent />
			</div>
		);
	}
}
export default HomeComponent;
