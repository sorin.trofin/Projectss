import React from 'react';
import './style.css';

export const FooterComponent = (props) => {
	return (
		<footer>
			<div className="container">
				<p>&copy; 2018 Mako's Table Restaurant All Rights Reserved.</p>
				<a href="https://www.linkedin.com/in/trofin-sorin-67425a155/">Created by Trofin Sorin</a>
			</div>
		</footer>
	);
};
