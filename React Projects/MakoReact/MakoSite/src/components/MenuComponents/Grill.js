import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Chicken breast:',
		price: '£4.90'
	},
	{
		name: 'Boneless chicken thighs:',
		price: '£4.50'
	},
	{
		name: 'Homemade shish kebab (Mici):',
		price: '£0.80'
	},
	{
		name: 'Pork shoulder steak:',
		price: '£5.75'
	},
	{
		name: 'Beef steak:',
		price: '£8.50'
	}
];

export const Grill = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Grill</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
