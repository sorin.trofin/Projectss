import React from 'react';
import './css/maincourse.css';
const feluri = [
	{
		name: 'Creamy Mushroom Soup:',
		price: '£4.95',
		ingredients: ' (Champignons, wild mushrooms, onion, garlic, double cream, potatoes.)'
	},
	{
		name: 'Beef Soup:',
		price: '£5.50',
		ingredients:
			'(Beef, onion, bell pepper, carrot, celery, potatoes, lime juice, tomato sauce, parsley, snow peas.) '
	},
	{
		name: 'Turkey A la Grec Soup:',
		price: '£5.20',
		ingredients: '(Turkey, onion, bell pepper, carrot, celery, rice, lemon, double cream, parsley, dill.)'
	},
	{
		name: 'Tom Kha Gai:',
		price: '£6.30',
		ingredients:
			'(Champignons, chicken breast, coconut milk, lemongrass, lime, chilly, fish sauce, ginger, coriander.) '
	}
];

export const Soups = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Soups</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>

			<br />
		</div>
	);
};
