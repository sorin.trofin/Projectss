import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Olive Bruschetta:',
		price: '£4',
		ingredients: '(Baguette, olives, sun dried tomatoes, parsley, basil, olive oil.)'
	},
	{
		name: 'Deep Fried Calamari Rings:',
		price: '£4.75',
		ingredients: '(Flour, bread crumbs, egg, calamari. Served with 1000 island sauce.)'
	},
	{
		name: 'Deep Fried Tempura Vegetables:',
		price: '£3.95',
		ingredients: '(Carrot, aubergine, red onion, mushrooms, beer batter. Served with soy sauce.)'
	},
	{
		name: 'Polenta with Sour Cream and Feta Cheese:',
		price: '£3.95',
		ingredients: '(Corn flour, feta, sour cream.)'
	}
];

export const Starters = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Starters</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>

			<br />
		</div>
	);
};
