import React from 'react';
import './css/maincourse.css';
const feluri = [
	{
		name: 'Ciorba de vacuta:',
		price: '£5.50',
		ingredients: '(Vita, ceapa, ardei gras, morcov, telina, cartofi, bulion, patrunjel, mazare.) '
	},
	{
		name: 'Ciorba de curcan A la Grec:',
		price: '£5.20',
		ingredients:
			'(Piept de curcan, ceapa, ardei gras, morcov, telina, orez, lamaie, smantana, patrunjel, marar. Servita cu ardei iute si smantana.)'
	},
	{
		name: 'Mamaliguta cu Branza si Smantana:',
		price: '£3.95',
		ingredients: '(Faina de porumb, branza, smantana.)'
	},
	{
		name: 'Shaorma de pui',
		price: '£6.50',
		ingredients:
			'(Piept de pui, lipie, maioneza cu usturoi, ketchup, varza, cartofi prajiti, ceapa rosie, castraveti murati, boia dulce.)'
	},
	{
		name: 'Tochitura cu Mamaliguta si Branza:',
		price: '£8.50',
		ingredients:
			'(Carne de porc, carnati de porc, faina de porumb, margarina, ou, branza sarata, cimbru, boia afumata,vin.)'
	},
	{
		name: 'Snitel de pui cu cartofi zdrobiti: ',
		price: '£8.95',
		ingredients:
			'(Piept de pui, pesmet, ou, faina, cartofi zdrobiti, curcuma, praf de usturoi, margarina, ceapa verde.)'
	},
	{
		name: 'Ceafa de porc marinata cu cartof copt si mamaliguta: ',
		price: '£8.50',
		ingredients:
			'(Ceafa de porc, usturoi, boia afumata, cimbrisor, cartof copt, faina de porumb, margarina, smantana.)'
	},
	{
		name: 'Miel cu piure de cartofi:',
		price: '£12.50',
		ingredients: '(Miel, piure de cartofi, margarina, lapte, morcov, ceapa, rozmarin.) '
	},
	{
		name: 'Pulpa de rata in sos de vin rosu cu piure aromatizat si ulei de trufe:',
		price: '£11.95',
		ingredients:
			'(Pulpa de rata, usturoi, cimbru, boia de ardei, piure de cartofi, lapte, ulei de trufe, vin rosu, merisoare.)'
	},
	{
		name: 'Coaste de Porc cu cartof copt si smantana:',
		price: '£13.55',
		ingredients:
			'(Coaste de porc, cartof copt, smantana, boia de ardei, usturoi, zahar brun, ghimbir, cimbriu, sos barbecue.)'
	},
	{
		name: 'Omleta a la Mako ',
		price: '£4.50',
		ingredients: '(Ou, ceapa, ardei gras, bacon, ciuperci, patrunjel, parmezan. Servita cu cartof zdrobit.)   '
	},
	{
		name: 'Mushrooms Risotto:',
		price: '£7.20',
		ingredients: '(Arborio rice, champignon mushrooms, red onion, butter, double cream, parmesan.) '
	},
	{
		name: 'Piept de pui la gratar:',
		price: '£4.90'
	},
	{
		name: 'Pulpe de pui dezosate:',
		price: '£4.50'
	},
	{
		name: 'Mici:',
		price: '£0.80'
	},
	{
		name: 'Ceafa de porc la gratar:',
		price: '£5.75'
	},
	{
		name: 'Salata de varza:',
		price: '£3.00'
	},
	{
		name: 'Salata de rosii, castraveti si ceapa:',
		price: '£3.50'
	},
	{
		name: 'Castraveti murati:',
		price: '£3.00'
	},
	{
		name: 'Ardei iute:',
		price: '£0.50'
	},
	{
		name: 'Papanasi cu Gem / Nutella:',
		price: '£4.75',
		ingredients: '(Faina, branza de vaci, oua, zahar, bicarbonat,  smantana, gem sau nutella.)'
	},
	{
		name: 'Clatite:',
		price: '£3.95',
		ingredients: '(faina, lamaie, oua, zahar, esenta de vanilie. Servite cu gem sau Nutella.)'
	}
];
export const MeniuRomanesc = () => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story ">Meniu Romanesc</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
