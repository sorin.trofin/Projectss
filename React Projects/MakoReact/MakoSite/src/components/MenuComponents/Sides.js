import React from 'react';
import './css/maincourse.css';
const feluri = [
	{
		name: 'Mixed salad:',
		price: '£3.50',
		ingredients: '(Lettuce, cherry tomatoes, cucumber, green onion, parmesan)',
		id: 1
	},
	{
		name: 'Chips:',
		price: '£2.50',
		id: 2
	},
	{
		name: 'Cheesy Chips:',
		price: '£3.00',
		id: 3
	},
	{
		name: 'Mash potatoes:',
		price: '£3.50',
		id: 4
	},
	{
		name: 'Sautéed mushrooms:',
		price: '£3.75',
		id: 5
	},
	{
		name: 'Baked potato:',
		price: '£2.65',
		id: 6
	},
	{
		name: 'Asparagus sautéed:',
		price: '£4.35',
		id: 7
	},
	{
		name: 'Crushed potatoes:',
		price: '£3.50',
		id: 8
	},
	{
		name: 'Garlic bread:',
		price: '£2.50',
		id: 9
	},
	{
		name: 'Garlic bread with cheese:',
		price: '£3.20 ',
		id: 10
	},
	{
		name: 'Couscous:',
		price: '£2.80',
		id: 11
	},
	{
		name: 'Coleslaw:',
		price: '£1.70',
		id: 12
	}
];

export const Sides = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Sides</h1>

					{feluri.map(({ name, price, ingredients, id }) => (
						<div className="center" key={id}>
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>

			<br />
		</div>
	);
};
