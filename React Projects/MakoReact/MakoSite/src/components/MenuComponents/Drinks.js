import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Coca-Cola / Diet Coca-Cola / Sprite(zero) / Schweppes:',
		price: '£1.95'
	},
	{
		name: '7Up(zero):',
		price: '£1.85'
	},
	{
		name: 'J2O Apple and Raspberry / Orange and passion fruit:',
		price: '£2.00'
	},
	{
		name: 'Redbull:',
		price: '£2.10'
	},
	{
		name: 'Water:',
		price: '£1.00'
	}
];

const feluri2 = [
	{
		name: 'Budweiser  300 ml:',
		price: '£2.20'
	},
	{
		name: 'Corona  330 ml:',
		price: '£2.40'
	},
	{
		name: 'Desperados 330 ml:',
		price: '£2.50'
	},
	{
		name: 'San Miguel 330ml:',
		price: '£2.40'
	},
	{
		name: 'Stella Artois 330 ml:',
		price: '£2.60'
	},
	{
		name: 'Peroni  330 ml:',
		price: '£2.50'
	},
	{
		name: 'Kopparberg mixed fruit  500 ml:',
		price: '£3.30'
	}
];

export const Drinks = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Drinks</h1>
					<br />
					<h4 className="center felname story">Cold Drinks</h4>
					<br />
					{feluri.map(({ name, price }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
						</div>
					))}
					<br />
					<h4 className="center felname story">Alcoholic Drinks</h4>
					<br />
					{feluri2.map(({ name, price }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
