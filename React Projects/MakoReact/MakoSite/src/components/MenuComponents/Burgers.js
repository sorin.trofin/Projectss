import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Grilled vegetables CheeseBurger:',
		price: '£9.50',
		ingredients:
			'(Minced beef, aubergine, tomato, bell pepper, red onion, eisberg salad, cheddar, garlic mayo. Comes with cheesy chips)'
	},
	{
		name: 'BBQ Beef and Bacon Burger:',
		price: '£9.50',
		ingredients:
			'(Minced beef, emmentaler cheese, bacon, egg, eisberg, tomato, red onion, pickles. Comes with cheesy chips)'
	},
	{
		name: 'Brie Gourmet Burger:',
		price: '£8.50',
		ingredients:
			'(Minced beef, lettuce, brie, pickles, 1000 island sauce, boiled egg, tomato. Comes with cheesy chips)'
	},
	{
		name: 'Schnitzel Chicken Burger:',
		price: '£9.50',
		ingredients:
			'(Chicken breast, emmentaller cheese, egg, bread crumbs, pickles, red onion, tomato, lettuce, 1000 island sauce. Comes with cheesy chips)'
	}
];

export const Burgers = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Burgers</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
