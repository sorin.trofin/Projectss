import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Sea Food Pasta:',
		price: '£10.50',
		ingredients:
			'(Linguini, jumbo shrimps, mussels, calamari, tomato pure, cherry tomatoes, basil, red onion, garlic, parsley, olive oil.)'
	},
	{
		name: 'Carbonara:',
		price: '£7.95',
		ingredients: '(Linguini, bacon, double cream, parmesan, egg.)'
	},
	{
		name: 'Bolognese:',
		price: '£8.95',
		ingredients: '(Tagliatelle, minced beef, tomato, red wine, onion, celery, carrot, thyme, sugar)'
	},
	{
		name: 'A La Matriciana:',
		price: '£8.85',
		ingredients: '(Tagliatelle, bacon, tomato, basil, red onion, garlic, white wine, parmesan.)'
	},
	{
		name: 'Chicken Gorgonzola and Pesto(Mako’s pasta):',
		price: '£9.85',
		ingredients: '(Tagliatelle, chicken, gorgonzola, double cream, basil, olive oil, parmesan, pistachio.)'
	},
	{
		name: 'Primavera:',
		price: '£6.90',
		ingredients: '(Tagliatelle, bell pepper, red onion, mushroom, aubergine, tomato, basil, garlic.)'
	}
];

export const Pasta = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Pasta</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
