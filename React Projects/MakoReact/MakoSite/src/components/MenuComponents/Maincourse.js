import React from 'react';
import './css/maincourse.css';
const feluri = [
	{
		name: 'Sunday Roast Dinner:',
		price: '£11.50',
		ingredients: '(Beef, baked potatoes, peas, carrots, Yorkshire puddings, gravy.)'
	},
	{
		name: 'Surf and Turf:',
		price: '£12.50',
		ingredients: '(Beef, shrimps, butter, asparagus, couscous, citronette sauce.)'
	},
	{
		name: '9Oz Beef Steak with Mustard Sauce and Asparagus:',
		price: '£13.50',
		ingredients: '(Beef entrecote, mustard, double cream, garlic, onion, thyme, asparagus, white wine.)'
	},
	{
		name: 'Duck Leg Confit with Truffle Flavored Mash Potatoes and Red Wine Sauce: ',
		price: '£11.95',
		ingredients: '(Duck leg, garlic, thyme, smoked paprika, mash, milk, truffle oil, red wine, cranberries.) '
	},
	{
		name: 'Shank Lamb with Mash Potatoes:',
		price: '£12.50',
		ingredients: ' (Lamb, potatoes, butter, milk, carrot, onion, rosemary.) '
	},
	{
		name: 'Barbeque Spare Ribs with Baked Potatoes:',
		price: '£13.55',
		ingredients:
			'(Pork ribs, baked potato, sour cream, smoked paprika, garlic, brown sugar, ginger, thyme, barbeque sauce.)'
	},
	{
		name: 'Rustic Stew with Polenta and Feta Cheese:',
		price: '£8.50',
		ingredients: '(Pork belly, pork sausages, corn flour, butter, egg, cheese, thyme, smoked paprika, white wine.) '
	},
	{
		name: 'Thay Beef Noodles:',
		price: '£8.60',
		ingredients:
			'(Beef entrecote, red onion, ginger, lime, soy sauce, corn starch, coriander, brown sugar, noodles.)'
	},
	{
		name: 'Chicken Breast Schnitzel with Crushed Potatoes:',
		price: '£8.95',
		ingredients:
			' (Chicken breast,lemon, bread crumbs, egg, flour, crushed potatoes, curcuma, garlic, parsley, butter, spring onion.)'
	},
	{
		name: 'Crispy Strips with Chips:',
		price: '£7.50',
		ingredients: '(Chicken breast, flower, egg, ketchup, garlic, mayo.)'
	},
	{
		name: 'Marinated Pork Shoulder with Baked Potato and Grilled Polenta:',
		price: '£8.50',
		ingredients: '(Pork shoulder, garlic, smoked paprika, thyme, baked potato, corn flower, butter, sour cream.)'
	},
	{
		name: 'Mushrooms Risotto:',
		price: '£7.20',
		ingredients: '(Arborio rice, champignon mushrooms, red onion, butter, double cream, parmesan.) '
	},
	{
		name: 'Shrimps and Asparagus Risotto: ',
		price: '£11.95',
		ingredients: '(shrimps, arborio rice, asparagus, basil, cherry tomatoes, lime, double cream, butter, parmesan.)'
	},
	{
		name: 'Shrimps with Citronette Sauce & Flavored Mashed Potatoes with Basil:',
		price: '£10.50',
		ingredients: '(shrimps, potatoes, butter, milk, basil, lemon, orange, honey.)'
	}
];
export const Maincourse = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Main course</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
