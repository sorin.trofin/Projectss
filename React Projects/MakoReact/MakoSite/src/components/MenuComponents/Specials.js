import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'All Day English Breakfast:',
		price: ' £6.95',
		ingredients: '(2x Bacon, 1 toast,  ½ tomato, mushrooms , 1x hash brown, 2x eggs,  beans, 2x sausages.)'
	},
	{
		name: 'Mighty All Day English Breakfast:',
		price: '£8.95',
		ingredients: '(3x Bacon, 1 toast, 2x  ½ tomato, mushrooms , 3x hash brown, 3x eggs,  beans, 3x sausages, chips)'
	},
	{
		name: 'Mako’s Sandwich:',
		price: '£4.35',
		ingredients: '(Toast, bacon, butter, mayo, lettuce, tomato, cucumber, mushroom cheddar.)'
	},
	{
		name: 'Shawarma:',
		price: '£6.50',
		ingredients:
			'(Chicken breast, tortilla, mayo, ketchup, cabbage, potato, red onion, pickles, garlic, smoked paprika.)'
	},
	{
		name: 'Quessadilla:',
		price: '£7.00',
		ingredients:
			'(Chicken breast, tortilla, red onion, bell pepper, cheddar, salsa, ginger, garlic, smoked paprika. Comes with chips.)'
	},
	{
		name: 'Jacket Potato:',
		price: '£3.75',
		ingredients: 'with: 1.beans           2.cheddar and bacon           3.tuna salad. '
	},
	{
		name: 'Mako’s Omelette with Crushed Potatoes:',
		price: '£4.50',
		ingredients: '(Egg, red onion, bell pepper, bacon, mushroom, parsley, parmesan, crushed potatoes.)'
	},
	{
		name: 'Scrambled Egg on toast. ',
		price: '£2.50'
	}
];

export const Specials = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Specials</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
