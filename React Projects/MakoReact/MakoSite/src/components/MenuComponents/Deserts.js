import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'New York Cheesecake:',
		price: '£3.95',
		ingredients: '(Mascarpone,whipping cream, biscuits, butter, cranberries, forest fruits sauce, pistachios.)'
	},
	{
		name: 'Apple pie & ice-cream / custard:',
		price: '£3.95',
		ingredients: 'apples, cinnamon, cornstarch, flour, yoghurt, egg, yeast, butter '
	},
	{
		name: 'Cheese Doughnuts with Jam / Nutella (Papanasi cu branza):',
		price: '£4.75',
		ingredients: 'flour, cow cheese, egg, sugar, bicarbonate,  sour cream, jam or nutella.'
	},
	{
		name: 'Ben and Jerry’s Ice Cream Chocolate Fudge Brownie or Cookie Dough 500ml:',
		price: '£4.99'
	}
];

export const Deserts = () => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Deserts</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
