import React from 'react';
import './css/maincourse.css';

const feluri = [
	{
		name: 'Caesar Salad :',
		price: '£8.50',
		ingredients: '(Eisberg salad, chicken breast, tortilla, parmesan, cherry tomatoes, Caesar dressing, croutons.)'
	},
	{
		name: 'Greek Salad:',
		price: '£7.50',
		ingredients: '(Eisberg salad, egg, bell pepper, tomato, cucumber, feta, olives, olive oil.)'
	},
	{
		name: 'Mako’s Salad: ',
		price: '£8.95',
		ingredients:
			'(Lettuce, cherry tomato, cucumber, champignons, red onion, pistachios, carrot, sour cream, basil, lime, honey.)'
	}
];

export const Salads = (props) => {
	return (
		<div>
			<section className="features" id="features">
				<div className="container">
					<h1 className="center felname story">Salads</h1>

					{feluri.map(({ name, price, ingredients }) => (
						<div className="center">
							<h2 className="nameofproducts">
								{name} <span className="price">{price}</span>
							</h2>
							<p>{ingredients}</p>
						</div>
					))}
				</div>
			</section>
			<br />
		</div>
	);
};
