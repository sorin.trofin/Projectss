import React from 'react';
import './style.css';

export const SocialComponent = () => {
	return (
		<section className="contact bg-primary" id="contact">
			<div className="container">
				<br />
				<br />
				<h2 className="story">Contact Us</h2>
				<div className="row">
					<div className="col-lg-6 col-md-12 col-sm-12">
						<h3>Opening Times</h3>
						<p>
							<strong>Monday-Sunday:</strong> 14:30 - 00:00;
						</p>

						<p>
							<strong>Thursday: </strong>Closed;
						</p>
					</div>

					<div className="col-lg-6 col-md-12 col-sm-12 text-align">
						<p className="adress">
							<strong>Adress:</strong> 206 Shirley Road SO153FL Southampton
						</p>

						<p className="adress">
							<strong>Telephone number: </strong> <br />
							07487 321 049 <br />02380 360 120
						</p>
						<p className="adress">
							<strong>Email: </strong> makostablerestaurant@yahoo.com
						</p>
					</div>

					<div className="col-lg-6 col-md-12 col-sm-12">
						<ul className="list-inline list-social">
							<li className="list-inline-item social-instagram">
								<a href="https://www.instagram.com/makostablerestaurant/?hl=en">
									<i className="fa fa-instagram" />
								</a>
							</li>
							<li className="list-inline-item social-facebook">
								<a href="https://www.facebook.com/MakosTableRestaurant">
									<i className="fa fa-facebook" />
								</a>
							</li>

							<li className="list-inline-item social-twitter">
								<a href="https://twitter.com/MakosTable">
									<i className="fa fa-twitter twitter" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<br />
		</section>
	);
};
