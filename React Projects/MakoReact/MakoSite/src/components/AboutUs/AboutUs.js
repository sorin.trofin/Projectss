import React from 'react';
import './style.css';
import hero from './img/hero.jpg';
import { FooterComponent } from '../FooterComponent/FooterComponent';
import { SocialComponent } from '../SocialComponent/SocialComponent';
import lifecycle from 'react-pure-lifecycle';
const methods = {
	componentDidMount() {
		window.scrollTo(500, 400);
	}
};
const AboutUs = (props) => {
	return (
		<section className="download background-colors text-center" id="download">
			<div className="container">
				<div className="row">
					<div className="col-md-12 mx-auto">
						<br />
						<h2
							style={{
								textAlign: 'center',

								fontFamily: 'Herr Von Muellerhoff',
								color: '#212529',

								fontWeight: '500',
								fontSize: '60px'
							}}
							className="section-heading"
						>
							About Us
						</h2>

						<p className="aboutusparagraph">
							The concept of international cuisine came from our chef, whose passion would be wasted if we
							had not decided to share it with you.
						</p>
						<p className="aboutusparagraph">
							This is how Mako's Table Restaurant was born. A restaurant with which we propose two things.
							First of all, to create a pleasant and relaxed atmosphere if you want to come for a business
							lunch and for date night or for a Friday night with friends, but also for a quiet brunch
							with family in weekend. Secondly, we want to open the way for some special dishes adapted by
							our chef to suit everyone's tastes.
						</p>
						<p className="aboutusparagraph">
							And because we want our space to make everyone feel welcome, the menu is so constructed that
							you can eat regardless of dietary restrictions. All these elements, along with drinks as
							special as food, are sure to make you feel at home.
						</p>
						<p className="aboutusparagraph">
							So when we opened the restaurant we made it out of the desire to make people happy and to
							share with others the dishes and beverages we love. The fact that we now have the
							opportunity to do this every day is a bigger reward than we could have imagined.
						</p>
						<p className="aboutusparagraph">
							After all, you are the reason we are here. The way we treat our customers is the face of our
							restaurant and shows how much we appreciate that they have chosen us.
						</p>
						<div>
							<img className="heroimage" src={hero} alt="Mako's Table" />
						</div>
					</div>
				</div>
				<br />

				<br />
			</div>
			<SocialComponent />
			<FooterComponent />
		</section>
	);
};

export default lifecycle(methods)(AboutUs);
