import React, { Component } from 'react';
import './style.css';
import { Redirect } from 'react-router';
import { Events, animateScroll as scroll, scrollSpy } from 'react-scroll';
import { Link } from 'react-router-dom';
class NavBarComponent extends Component {
	constructor(props) {
		super(props);
		this.state = { mnuShow: false };
		this.closeMnu = this.closeMnu.bind(this);
	}

	componentDidMount() {
		Events.scrollEvent.register('begin', () => {
			console.log('begin', arguments);
			this.closeMnu();
		});

		Events.scrollEvent.register('end', function () {
			console.log('end', arguments);
		});
		scrollSpy.update();
	}

	componentWillUnmount() {
		Events.scrollEvent.remove('begin');
		Events.scrollEvent.remove('end');
	}

	toggleShow() {
		this.setState({ mnuShow: !this.state.mnuShow });
	}

	closeMnu() {
		if (this.state.mnuShow) {
			this.setState({ mnuShow: false });
		}
	}

	scrollToTop() {
		scroll.scrollToTop();
	}

	render() {
		const show = this.state.mnuShow ? 'show' : '';

		return (
			<nav className={`navbar navbar-expand-lg navbar-dark ${this.props.navBarShrink}`} id="mainNav">
				<div className="container menubuttons">
					<a
						onClick={this.scrollToTop.bind(this)}
						className="navbar-brand js-scroll-trigger"
						href="#page-top"
					>
						Mako's Table Restaurant
					</a>
					<button
						onClick={this.toggleShow.bind(this)}
						className="navbar-toggler navbar-toggler-right"
						type="button"
						data-toggle="collapse"
						data-target="#navbarResponsive"
						aria-controls="navbarResponsive"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						Menu
						<i className="fa fa-bars" />
					</button>
					<div className={`collapse navbar-collapse ${show}`} id="navbarResponsive">
						<ul className="navbar-nav ml-auto">
							<li className="nav-item">
								<Link
									className="nav-link js-scroll-trigger"
									to={{ pathname: '/', state: 'desiredState' }}
									smooth="easeInOutQuart"
									duration={1000}
								>
									Home
								</Link>
							</li>
							<li className="nav-item">
								<Link
									className="nav-link js-scroll-trigger"
									to="/aboutus"
									smooth="easeInOutQuart"
									duration={1000}
								>
									About Us
								</Link>
							</li>
							<li className="nav-item">
								<Link
									className="nav-link js-scroll-trigger"
									to="menu/starters"
									smooth="easeInOutQuart"
									duration={1000}
								>
									Menu
								</Link>
							</li>

							<li className="nav-item">
								<Link
									className="nav-link js-scroll-trigger"
									to="/gallery"
									smooth="easeInOutQuart"
									duration={1000}
								>
									Gallery
								</Link>
							</li>
							<li className="nav-item">
								<Link
									className="nav-link js-scroll-trigger"
									to="/contact"
									smooth="easeInOutQuart"
									duration={1000}
								>
									Contact
								</Link>
							</li>
							<li className="nav-item">
								<a
									className="nav-link js-scroll-trigger"
									href="https://www.just-eat.co.uk/restaurants-makos-table-millbrook/menu"
									smooth="easeInOutQuart"
									duration={1000}
								>
									Order Online
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}
export default NavBarComponent;
