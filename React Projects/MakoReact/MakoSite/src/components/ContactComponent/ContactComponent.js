import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import contact from './Contact.png';
import { FooterComponent } from '../FooterComponent/FooterComponent';
import './styles.css';
import pinpoint from './pinpoint.png';

const AnyReactComponent = ({ text }) => (
	<img
		src={pinpoint}
		style={{
			color: 'white',

			padding: '15px 10px',
			width: '80px',
			height: '80px',
			display: 'inline-flex',
			textAlign: 'center',
			alignItems: 'center',
			justifyContent: 'center',

			transform: 'translate(-80%, -100%)'
		}}
	/>
);

class ContactComponent extends Component {
	static defaultProps = {
		center: {
			lat: 50.914629,
			lng: -1.423833
		},
		zoom: 18
	};
	componentDidMount() {
		window.scrollTo(500, 400);
	}
	render() {
		return (
			// Important! Always set the container height explicitly
			<div style={{ height: '50vh', width: '100%', margin: '30px 0' }}>
				<div className="container">
					<div className="row">
						<div className="col-lg-6 col-md-12 col-sm-12">
							<h3>Opening Times</h3>
							<p>
								<strong>Monday-Sunday:</strong> 14:30 - 00:00;
							</p>

							<p>
								<strong>Thursday: </strong>Closed;
							</p>
							<p>
								<strong>Adress:</strong>206 Shirley Road SO153FL Southampton
							</p>

							<p>
								<strong>Telephone number: </strong> <br />
								07487 321 049 <br />
								02380 360 120
							</p>

							<p>
								<strong>Email: </strong> makostablerestaurant@yahoo.com
							</p>
						</div>

						<div className="col-lg-6 col-md-12 col-sm-12">
							<img className="contactphoto" src={contact} alt="contact" />
						</div>
					</div>
				</div>

				<br />
				<br />

				<GoogleMapReact
					className="googlemap"
					bootstrapURLKeys={{ key: 'AIzaSyAWwhireugU3MSmcGUMJID9auIFufA6hUo' }}
					defaultCenter={this.props.center}
					defaultZoom={this.props.zoom}
				>
					<AnyReactComponent lat={50.914449} lng={-1.4235713} text={'Mako"s Table Restaurant'} />
				</GoogleMapReact>

				<FooterComponent />
			</div>
		);
	}
}

export default ContactComponent;
