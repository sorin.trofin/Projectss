import React from 'react';
import './style.css';
import 'simple-line-icons/css/simple-line-icons.css';
import { SocialComponent } from '../SocialComponent/SocialComponent';
import { FooterComponent } from '../FooterComponent/FooterComponent';
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { Deserts } from '../MenuComponents/Deserts';
import { Specials } from '../MenuComponents/Specials';
import { Pasta } from '../MenuComponents/Pasta';
import { Maincourse } from '../MenuComponents/Maincourse';
import { Drinks } from '../MenuComponents/Drinks';
import { Starters } from '../MenuComponents/Starters';
import { Soups } from '../MenuComponents/Soups';
import { Salads } from '../MenuComponents/Salads';
import { Burgers } from '../MenuComponents/Burgers';
import { Grill } from '../MenuComponents/Grill';
import { Sides } from '../MenuComponents/Sides';
import { MeniuRomanesc } from '../MenuComponents/MeniuRomanesc';
import Starter from './img/Starter.jpg';
import Desert from './img/Desert.jpg';
import Soup from './img/Soup.jpg';
import Maincourses from './img/Maincourses.jpg';
import Pastas from './img/Pastas.jpg';
import Burger from './img/Burger.jpg';
import Salad from './img/Salad.jpg';
import Grills from './img/Grills.jpg';
import Special from './img/Special.jpg';
import Sidess from './img/Sides.png';
import ciorba from './img/ciorba.jpg';
import drinks from './img/drinks.jpg';
import lifecycle from 'react-pure-lifecycle';
const methods = {
	componentDidMount() {
		window.scrollTo(500, 400);
	}
};
const MenuComponent = () => {
	return (
		<Router>
			<div>
				<div className="menu">
					<h1
						style={{
							textAlign: 'center',
							backgroundColor: '#fdcc52',
							fontFamily: 'Herr Von Muellerhoff',
							color: '#212529',

							fontWeight: '500',
							fontSize: '60px'
						}}
					>
						Choose a Menu
					</h1>
					<aside className="menuitemsleft">
						<Link to="/menu/starters" className="subfels" id="starters">
							<div className="left">
								<img className="categoryphoto" src={Starter} alt="starter" />
								<h2 className="startername starters">Starters</h2>
							</div>
						</Link>

						<Link to="/menu/soups" className="subfels" id="soups">
							<div className="left">
								<img className="categoryphoto" src={Soup} alt="Soup" />
								<h2 className="startername">Soups</h2>
							</div>
						</Link>

						<Link to="/menu/maincourse" className="subfels" id="maincourse">
							<div className="left">
								<img className="categoryphoto" src={Maincourses} alt="Maincourse" />
								<h2 className="startername">Main Course</h2>
							</div>
						</Link>

						<Link to="/menu/pasta" className="subfels" id="pasta">
							<div className="left">
								<img className="categoryphoto" src={Pastas} alt="pasta" />
								<h2 className="startername">Pasta</h2>
							</div>
						</Link>

						<Link to="/menu/burgers" className="subfels" id="burgers" alt="burgers">
							<div className="left">
								<img className="categoryphoto" src={Burger} alt="burger" />
								<h2 className="startername burgers">Burgers</h2>
							</div>
						</Link>

						<Link to="/menu/drinks" className="subfels" id="burgers" alt="burgers">
							<div className="left">
								<img className="categoryphoto" src={drinks} alt="burger" />
								<h2 className="startername">Drinks</h2>
							</div>
						</Link>
					</aside>

					<div className="menuitems">
						<Switch>
							<Route exact path="/menu/starters" component={Starters} />
							<Route path="/menu/soups" component={Soups} />
							<Route path="/menu/maincourse" component={Maincourse} />
							<Route path="/menu/pasta" component={Pasta} />
							<Route path="/menu/burgers" component={Burgers} />
							<Route path="/menu/salads" component={Salads} />
							<Route path="/menu/grill" component={Grill} />
							<Route path="/menu/specials" component={Specials} />
							<Route path="/menu/sides" component={Sides} />
							<Route path="/menu/deserts" component={Deserts} />
							<Route path="/menu/drinks" component={Drinks} />
							<Route path="/menu/meniuromanesc" component={MeniuRomanesc} />
						</Switch>
					</div>

					<aside className="menuitemsright">
						<Link to="/menu/salads" className="subfels" id="salads">
							<div className="right">
								<img className="categoryphotoright" src={Salad} alt="Salads" />
								<h2 className="startername">Salads</h2>
							</div>
						</Link>
						<Link to="/menu/grill" className="subfels" id="grill">
							<div className="right">
								<img className="categoryphotoright" src={Grills} alt="Grills" />
								<h2 className="startername grill">Grill</h2>
							</div>
						</Link>

						<Link to="/menu/specials" className="subfels" id="specials">
							<div className="right">
								<img className="categoryphotoright " src={Special} alt="Specials" />
								<h2 className="startername specials">Specials</h2>
							</div>
						</Link>

						<Link to="/menu/sides" className="subfels" id="sides">
							<div className="right">
								<img className="categoryphotoright" src={Sidess} alt="Sides" />
								<h2 className="startername">Sides</h2>
							</div>
						</Link>

						<Link to="/menu/deserts" className="subfels" id="deserts">
							<div className="right">
								<img className="categoryphotoright" src={Desert} alt="Desert" />
								<h2 className="startername deserts">Deserts</h2>
							</div>
						</Link>

						<Link to="/menu/meniuromanesc" className="subfels" id="deserts">
							<div className="right">
								<img className="categoryphotoright" src={ciorba} alt="Desert" />
								<h2 className="startername meniuromanesc">Meniu Romanesc</h2>
							</div>
						</Link>
					</aside>
					<div className="clearfix" />

					{/* <section className="features" id="features">
					<div className="container">
						<h1 className="center">Menu</h1>
						<ul className="navbar-nav ml-auto">
							<li className="nav-item">
								{feluri.map(({ subfel, label, clase, id }) => (
									<Link to={`/${subfel}`} className={`${clase}`} id={`${id}`}>
										{label}
									</Link>
								))}
							</li>
						</ul>
					</div>
				</section> */}
				</div>
				<SocialComponent />
				<FooterComponent />
			</div>
		</Router>
	);
};
export default lifecycle(methods)(MenuComponent);
