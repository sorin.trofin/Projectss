import React from 'react';
import './style.css';
import one from './img/1.jpg';
import two from './img/2.jpg';
import three from './img/3.jpg';
import four from './img/4.jpg';
import five from './img/5.jpg';
import six from './img/6.jpg';
import seven from './img/7.jpg';
import eight from './img/8.jpg';
import ten from './img/10.jpg';
import eleven from './img/11.jpg';
import twelwe from './img/12.jpg';
import thirdteen from './img/13.jpg';
import fourteen from './img/14.jpg';
import fifthteen from './img/15.jpg';
import sixteen from './img/16.jpg';
import seventeen from './img/17.jpg';
import eightteen from './img/18.jpg';
import nineteen from './img/19.jpg';
import twenty from './img/20.jpg';
import twentyone from './img/21.jpg';
import twentytwo from './img/22.jpg';
import twentythree from './img/23.jpg';
import twentyfour from './img/24.jpg';
import twentyfive from './img/25.jpg';
import twentysix from './img/26.jpg';
import twentyseven from './img/27.jpg';
import twentyeight from './img/28.jpg';
import twentynine from './img/29.jpg';
import thirty from './img/30.jpg';
import Carousel from 'nuka-carousel';
import { SocialComponent } from '../SocialComponent/SocialComponent';
import { FooterComponent } from '../FooterComponent/FooterComponent';
class GalleryComponent extends React.Component {
	state = {
		slideIndex: 0
	};
	componentDidMount() {
		window.scrollTo(500, 400);
	}
	render() {
		return (
			<div>
				<div className="container">
					<h1
						style={{
							textAlign: 'center',

							fontFamily: 'Herr Von Muellerhoff',
							color: '#212529',

							fontWeight: '500',
							fontSize: '60px'
						}}
						className="gallerytitle"
					>
						Gallery
					</h1>

					<Carousel slideIndex={this.state.slideIndex} wrapAround={true} autoplay={true} className="carousel">
						<img className="imgslide" src={one} alt="1" />
						<img className="imgslide" src={two} alt="2" />
						<img className="imgslide" src={three} alt="3" />
						<img className="imgslide" src={four} alt="4" />
						<img className="imgslide" src={five} alt="5" />
						<img className="imgslide" src={six} alt="6" />
						<img className="imgslide" src={seven} alt="7" />
						<img className="imgslide" src={eight} alt="8" />
						<img className="imgslide" src={ten} alt="10" />
						<img className="imgslide" src={eleven} alt="11" />
						<img className="imgslide" src={twelwe} alt="12" />
						<img className="imgslide" src={thirdteen} alt="13" />
						<img className="imgslide" src={fourteen} alt="14" />
						<img className="imgslide" src={fifthteen} alt="15" />
						<img className="imgslide" src={sixteen} alt="16" />
						<img className="imgslide" src={seventeen} alt="17" />
						<img className="imgslide" src={eightteen} alt="18" />
						<img className="imgslide" src={nineteen} alt="19" />
						<img className="imgslide" src={twenty} alt="20" />
						<img className="imgslide" src={twentyone} alt="21" />
						<img className="imgslide" src={twentytwo} alt="22" />
						<img className="imgslide" src={twentythree} alt="23" />
						<img className="imgslide" src={twentyfour} alt="24" />
						<img className="imgslide" src={twentyfive} alt="25" />
						<img className="imgslide" src={twentysix} alt="26" />
						<img className="imgslide" src={twentyseven} alt="27" />
						<img className="imgslide" src={twentyeight} alt="28" />
						<img className="imgslide" src={twentynine} alt="29" />
						<img className="imgslide" src={thirty} alt="30" />
					</Carousel>
				</div>
				<br />
				<br />
				<SocialComponent />
				<FooterComponent />
			</div>
		);
	}
}
export default GalleryComponent;
