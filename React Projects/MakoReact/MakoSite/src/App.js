import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import 'font-awesome/css/font-awesome.css';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import AboutUs from './components/AboutUs/AboutUs';
import { HeroComponent } from './components/HeroComponent/HeroComponent';
import NavBarComponent from './components/NavBarComponent/NavBarComponent';
import MenuComponent from './components/MenuComponent/MenuComponent';
import GalleryComponent from './components/GalleryComponent/GalleryComponent';
import ContactComponent from './components/ContactComponent/ContactComponent';
import HomeComponent from './components/HomeComponent/HomeComponent';
import { BookComponent } from './components/BookTableComponent/BookComponent';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = { navBarShrink: '' };
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	// handleScroll(event) {
	// 	const domNode = ReactDOM.findDOMNode(this.navEl);
	// 	const nbs = window.pageYOffset > 100 ? 'navbar-shrink' : '';
	// 	this.setState({ navBarShrink: nbs });
	// }

	render() {
		const nbs = this.state ? this.state.navBarShrink : '';
		return (
			<Router>
				<div>
					<NavBarComponent navBarShrink={nbs} />
					<HeroComponent />

					<Switch>
						<Route exact path="/" component={HomeComponent} />
						<Route path="/aboutus" component={AboutUs} />
						<Route path="/contact" component={ContactComponent} />
						<Route path="/gallery" component={GalleryComponent} />
						<Route path="/menu" component={MenuComponent} />
						<Route path="/booktable" component={BookComponent} />
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
