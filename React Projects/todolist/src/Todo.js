import React from 'react';

class Todo extends React.Component {
	constructor(props) {
		super();
		this.state = {
			text: props.text,
			isEditing: false
		};
		this.onClickEdit = this.onClickEdit.bind(this);
		this.onSaveEdit = this.onSaveEdit.bind(this);
		this.onTextChanged = this.onTextChanged.bind(this);
		// this.removeItem = this.removeItem.bind(this);
	}

	onClickEdit() {
		this.setState({ isEditing: !this.state.isEditing });
	}

	onSaveEdit() {
		this.setState({
			isEditing: false
		});
	}

	onTextChanged(e) {
		this.setState({ text: e.target.value });
	}

	// removeItem = (index) => {
	//   return this.setState({text: [...this.state.text].splice(index, 0)});
	// }

	remove = () => {
		this.props.remove(this.props.todo.id);
	};

	render() {
		return (
			<li>
				{this.state.isEditing ? '' : <span>{this.state.text}</span>}
				{this.state.isEditing ? (
					<span>
						<input value={this.state.text} type="text" onChange={this.onTextChanged} />
					</span>
				) : (
					''
				)}
				{this.state.isEditing ? '' : <button onClick={this.onClickEdit}>Edit</button>}
				<button onClick={this.onSaveEdit}>Save</button>
				<button onClick={this.remove}>Delete</button>
			</li>
		);
	}
}

export default Todo;
