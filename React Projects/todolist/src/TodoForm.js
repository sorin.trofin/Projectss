import React, { Component } from 'react';

class TodoForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: ''
		};
		this.submitForm = this.submitForm.bind(this);
	}

	submitForm = (e) => {
		e.preventDefault();
		this.props.add(this.state.value);
	};

	handlerChange = (e) => {
		console.log(e.target.value);

		this.setState({
			value: e.target.value
		});
	};

	render() {
		return (
			<form onSubmit={this.submitForm}>
				<input type="text" className="add-new-item" value={this.state.value} onChange={this.handlerChange} />
				<button type="submit">Add</button>
			</form>
		);
	}
}

export default TodoForm;
