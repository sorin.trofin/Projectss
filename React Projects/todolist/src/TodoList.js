import React from 'react';
import Todo from './Todo';

const TodoList = (props) => {
	const newList = props.list.map((todo) => {
		return <Todo todo={todo} key={todo.id} text={todo.text} remove={props.remove} />;
	});
	return <div>{newList}</div>;
};

export default TodoList;
