import React, { Component } from 'react';
import './App.css';
import Title from './Title';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import _ from 'lodash';

class App extends Component {
	state = {
		items: []
	};

	add = (val) => {
		console.log(val);
		const id = _.uniqueId();
		const todo = { text: val, id: id };
		this.state.items.push(todo);
		this.setState({
			items: this.state.items
		});
	};

	handleRemove = (id) => {
		// Filter all todos except the one to be removed
		const newList = this.state.items.filter((todo) => {
			if (todo.id !== id) return todo;
		});
		this.setState({ items: newList });
	};

	render() {
		return (
			<div>
				<div className="header align">
					<Title count={this.state.items.length} />
				</div>

				<div className="container align">
					<TodoForm add={this.add} />
					<TodoList list={this.state.items} remove={this.handleRemove} />
				</div>
			</div>
		);
	}
}

export default App;
