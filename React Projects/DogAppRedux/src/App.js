import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import './App.css';
import DogContainer from './container/DogContainer';
import Home from './components/Home';

const linkStyle = {
	textDecoration: 'none',
	color: 'black',
	display: 'block',
	transition: '.3s background-color'
};

class App extends Component {
	render() {
		return (
			<Router>
				<div className="container">
					<ul className="mainmenu">
						<li>
							<Link style={linkStyle} to="/">
								Home
							</Link>
						</li>
						<li>
							<Link style={linkStyle} to="/view">
								PetHouse
							</Link>
						</li>
					</ul>

					<Switch>
						<Route exact path="/" component={Home} />
						<Route path="/view" component={DogContainer} />
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
