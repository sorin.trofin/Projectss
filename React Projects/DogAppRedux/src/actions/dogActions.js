import * as actionTypes from '../constants/action-types';

import { getDogs, handleAddDog, handleUpdateDog, handleUpdateAdopted, handleRemoveDog } from '../services/api';

export const getDog = () => async (dispatch) => {
	const dogs = await getDogs();

	dispatch({
		type: actionTypes.GET_DOGS,
		payload: {
			dogs
		}
	});
};

export const createDog = (newDog) => async (dispatch) => {
	const dog = await handleAddDog(newDog);

	dispatch({
		type: actionTypes.CREATE_DOG,
		payload: {
			dog
		}
	});
};

export const updateDog = (DogToUpdate, newDog) => async (dispatch) => {
	const updatedDog = await handleUpdateDog(DogToUpdate, newDog);
	dispatch({
		type: actionTypes.UPDATE_DOG,
		payload: {
			updatedDog
		}
	});
};

export const updateAdopted = (dogToUpdate, newAdopted) => async (dispatch) => {
	const updatedAdoptedDog = await handleUpdateAdopted(dogToUpdate, newAdopted);
	dispatch({
		type: actionTypes.UPDATE_ADOPTED,
		payload: {
			updatedAdoptedDog
		}
	});
};

export const deleteDog = (DogToDelete) => async (dispatch) => {
	const dog = await handleRemoveDog(DogToDelete);
	dispatch({
		type: actionTypes.DELETE_DOG,
		payload: {
			dog
		}
	});
};
