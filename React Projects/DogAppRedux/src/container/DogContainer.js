import React, { Component } from 'react';
import DogList from '../components/DogList';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AddDog from '../components/AddDog';
import 'antd/dist/antd.css';
import * as dogFilterTypes from '../constants/filter-types';
import FilterSelector from '../components/FilterSelector';
import * as dogActions from '../actions/dogActions';
import * as filterActions from '../actions/filterActions';

class DogContainer extends Component {
	async componentDidMount() {
		const { actions } = this.props;
		actions.getDog();
	}

	render() {
		const { actions, filter } = this.props;
		const { dogs } = this.props;
		return (
			<div>
				<FilterSelector currentFilter={filter} onSelectFilter={actions.setFilter} />
				<AddDog handleAddDog={actions.createDog} />
				<DogList
					dogs={dogs}
					handleRemoveDog={actions.deleteDog}
					handleUpdateDog={actions.updateDog}
					currentFilter={filter}
					handleUpdateAdopted={actions.updateAdopted}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	dogs: state.dogs,
	filter: state.filter,
	adopted: state.adopted
});

const mapDispatchToProps = (dispatch) => ({
	actions: {
		setFilter: (filter) => dispatch(filterActions.setFilter(filter)),
		getDog: () => dispatch(dogActions.getDog()),
		createDog: (dogName, rasa, adopted) => dispatch(dogActions.createDog({ dogName, rasa, adopted })),
		updateDog: (dogToUpdate, newName) => dispatch(dogActions.updateDog(dogToUpdate, newName)),
		updateAdopted: (dogToUpdate, newAdopted) => dispatch(dogActions.updateDog(dogToUpdate, newAdopted)),
		deleteDog: (DogToDelete) => dispatch(dogActions.deleteDog(DogToDelete))
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DogContainer));
