import React from 'react';

import * as filterTypes from '../constants/filter-types';

const FilterSelector = ({ selectedFilter, onSelectFilter }) => {
	return (
		<div id="dogsfilter">
			<br />
			<label>Filter the dogs:</label>
			<select
				className="selectAdopted"
				value={selectedFilter}
				onChange={(event) => onSelectFilter(event.target.value)}
			>
				<option value={filterTypes.ALL_ADOPTED}>All</option>
				<option value={filterTypes.ADOPTED}>Adopted</option>
				<option value={filterTypes.NOT_ADOPTED}>Not Adopted</option>
			</select>
		</div>
	);
};

export default FilterSelector;
