import React from 'react';
import DogItem from './DogItem';
import * as dogFilterTypes from '../constants/filter-types';

const FilterTypes = {
	[dogFilterTypes.ALL_ADOPTED]: () => true,
	[dogFilterTypes.ADOPTED]: (dogs) => dogs.adopted === 'Adopted',
	[dogFilterTypes.NOT_ADOPTED]: (dogs) => dogs.adopted === 'Not Adopted'
};

const DogList = ({ dogs, handleRemoveDog, handleUpdateDog, handleUpdateAdopted, currentFilter }) => {
	return (
		<div className="dog-list">
			{dogs
				.filter(FilterTypes[currentFilter])
				.map((dog) => (
					<DogItem
						key={dog.id}
						dog={dog}
						handleRemoveDog={(dog) => handleRemoveDog(dog)}
						handleUpdateDog={(dogToUpdate, newName) => handleUpdateDog(dogToUpdate, newName)}
						handleUpdateAdopted={(dogToUpdate, newAdopted) => handleUpdateAdopted(dogToUpdate, newAdopted)}
					/>
				))}
			<div className="clearfix" />
		</div>
	);
};

export default DogList;
