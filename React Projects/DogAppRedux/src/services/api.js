import uuidv1 from 'uuid/v1';
export const getDogs = async () => {
	const dogsResponse = await fetch('/dogs', {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'GET'
	});
	const dogs = await dogsResponse.json();
	return dogs;
};

export const handleAddDog = async ({ dogName, rasa, adopted }) => {
	const dogs = {
		id: uuidv1(),
		fullname: dogName,
		rasa: rasa,
		adopted: adopted
	};
	const response = await fetch(`/dogs`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(dogs),
		method: 'POST'
	});
	const addedDog = await response.json();
	return addedDog;
};

export const handleUpdateDog = async (dogToUpdate, newName) => {
	const updatedDog = { ...dogToUpdate, fullname: newName };
	const response = await fetch(`/dogs/${dogToUpdate.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(updatedDog),
		method: 'PUT'
	});
	return response.json();
};

export const handleUpdateAdopted = async (dogToUpdate, newAdopted) => {
	const updatedDog = { ...dogToUpdate, adopted: newAdopted };
	const response = await fetch(`/dogs/${dogToUpdate.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(updatedDog),
		method: 'PUT'
	});
	return response.json();
};

export const handleRemoveDog = async (dogToDelete) => {
	const response = await fetch(`/dogs/${dogToDelete.id}`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		method: 'DELETE'
	});
	return response.json();
};
