/**
 * filter actions
 */
export const SET_FILTER = 'SET_FILTER';

/**
 * product actions
 */

export const GET_DOGS = 'GET_DOGS';
export const CREATE_DOG = 'CREATE_DOG';
export const UPDATE_DOG = 'UPDATE_DOG';
export const DELETE_DOG = 'DELETE_DOG';
export const UPDATE_ADOPTED = 'UPDATE_ADOPTED';
