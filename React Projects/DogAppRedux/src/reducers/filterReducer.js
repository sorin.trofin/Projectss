import * as actionTypes from '../constants/action-types';
import * as filterTypes from '../constants/filter-types';

const initialState = filterTypes.ALL_ADOPTED;

const filterReducer = (state = initialState, action) => {
	const { payload } = action;

	switch (action.type) {
		case actionTypes.SET_FILTER:
			return payload.filter;

		default:
			return state;
	}
};

export default filterReducer;
