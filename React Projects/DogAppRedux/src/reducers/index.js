import { combineReducers } from 'redux';

import filterReducer from './filterReducer';
import dogReducer from './dogReducer';

const rootReducer = {
	filter: filterReducer,
	dogs: dogReducer,
	adopted: dogReducer
};

export default combineReducers(rootReducer);
