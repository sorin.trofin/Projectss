import * as actionTypes from '../constants/action-types';

const initialState = [];

const dogReducer = (state = initialState, action) => {
	const { payload } = action;

	switch (action.type) {
		case actionTypes.GET_DOGS:
			return payload.dogs;
		case actionTypes.CREATE_DOG:
			return [ ...state, payload.dog ];

		case actionTypes.UPDATE_DOG: {
			const newDogs = state.map((dog) => {
				if (dog.id === payload.updatedDog.id) {
					return payload.updatedDog;
				}
				return dog;
			});
			return newDogs;
		}
		case actionTypes.UPDATE_ADOPTED: {
			const newAdoptedDogs = state.map((dog) => {
				if (dog.id === payload.updatedAdoptedDog.id) {
					return payload.updatedAdoptedDog;
				}
				return dog;
			});
			return newAdoptedDogs;
		}
		
		case actionTypes.DELETE_DOG: {
			const newState = Object.assign([], state);
			const indexOfDogToDelete = state.findIndex((dog) => {
				return dog.id === payload.dog.id;
			});
			newState.splice(indexOfDogToDelete, 1);
			return newState;
		}
		default:
			return state;
	}
};

export default dogReducer;
